package org.expasy.mzjava.glycomics.io.mol.glycoct;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.glycomics.mol.*;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Davide Alocci
 * @author julien
 * @version sqrt -1.
 */
@ServiceProvider(service = GlycoCtResolver.class)
public class DefaultGlycoCtResolver implements GlycoCtResolver {


    private final Map<String, String> monosaccharideGlycoCtMap = new HashMap<>();
    private final Map<String, String> substituentGlycoCtMap = new HashMap<>();
    private final Map<String, Anomericity> anomericityGlycoCtMap = new HashMap<>();
    private final Map<String, Composition> linkCompGlycoCtMap = new HashMap<>();
    public static final String PROPERTY_GLYCOCT_MONOSACCHARIDES = "org.expasy.mzjava.io.reader.glycoct.monosaccharideGlycoCtData";
    public static final String PROPERTY_GLYCOCT_SUBSTITUENTS = "org.expasy.mzjava.io.reader.glycoct.substituentGlycoCtData";
    private static final String ACTUAL_VALUE = "Actual Value: ";


    public DefaultGlycoCtResolver() {

        final String fileGlycoctMonosaccharides = "monosaccharideGlycoCtData.json";
        final String fileGlycoctSubstituents = "substituentGlycoCtData.json";

        GlycoCtJsonParser glycoCtJsonParser = new GlycoCtJsonParser();

        monosaccharideGlycoCtMap.putAll(glycoCtJsonParser.parseMonosaccharide(PROPERTY_GLYCOCT_MONOSACCHARIDES,fileGlycoctMonosaccharides));
        substituentGlycoCtMap.putAll(glycoCtJsonParser.parseSubstituent(PROPERTY_GLYCOCT_SUBSTITUENTS,fileGlycoctSubstituents));

        anomericityGlycoCtMap.put("a", Anomericity.alpha);
        anomericityGlycoCtMap.put("b", Anomericity.beta);
        anomericityGlycoCtMap.put("o", Anomericity.open);

        linkCompGlycoCtMap.put("o", Composition.parseComposition("H-1"));
        linkCompGlycoCtMap.put("h", Composition.parseComposition("H-1"));
        linkCompGlycoCtMap.put("d", Composition.parseComposition("O-1H-1"));
        linkCompGlycoCtMap.put("r", Composition.parseComposition("H-1"));
        linkCompGlycoCtMap.put("s", Composition.parseComposition("H-1"));
    }


    /**
     * Search for the monosaccharide that resolves the GlycoCT code in input.
     * <p>The method throw an IllegalStateException if there is no monosaccharide that resolves the GlycoCT Code</p>
     *
     * @param glycoCtCode GlycoCT code to resolve.
     * @return A monosaccharide object with the specified GlycoCT Code.
     */
    @Override
    public Monosaccharide getMonosaccharide(String glycoCtCode) {

        Preconditions.checkNotNull(glycoCtCode);


        String monosaccharideName = monosaccharideGlycoCtMap.get(glycoCtCode);
        if (monosaccharideName == null) {
            throw new IllegalStateException("Cannot resolve the Monosaccharide from " + glycoCtCode + ". If you want to add it, you can specify a new GlycoCT Lookup file for Monosaccharide in System Property "+PROPERTY_GLYCOCT_MONOSACCHARIDES+".  " +
                    ACTUAL_VALUE + System.getProperty(PROPERTY_GLYCOCT_MONOSACCHARIDES, "monosaccharideGlycoCtData.json"));
        }

        MonosaccharideLookup lookup = Lookup.getDefault().lookup(MonosaccharideLookup.class);
        return lookup.getNew(monosaccharideName);
    }

    /**
     * Search for the substituent that resolves the GlycoCT code in input.
     * <p>The method throw an IllegalStateException if there is no substituent that resolves the GlycoCT Code</p>
     *
     * @param glycoCtCode GlycoCT Code to resolve.
     * @return A substituent object with the specified GlycoCT Code.
     */
    @Override
    public Substituent getSubstituent(String glycoCtCode) {

        Preconditions.checkNotNull(glycoCtCode);


        String substituentName = substituentGlycoCtMap.get(glycoCtCode);
        if (substituentName == null) {
            throw new IllegalStateException("Cannot resolve the Substituent from " + glycoCtCode + ". If you want to add it, you can specify a new GlycoCT Lookup file for Substituent in System Property "+PROPERTY_GLYCOCT_SUBSTITUENTS+".  " +
                    ACTUAL_VALUE + System.getProperty(PROPERTY_GLYCOCT_SUBSTITUENTS, "substituentGlycoCtData.json"));
        }

        SubstituentLookup lookup = Lookup.getDefault().lookup(SubstituentLookup.class);
        return lookup.getNew(substituentName);
    }

    /**
     * Returns the appropriate Anomericity instance for the given glycoct code.
     *
     * @param glycoCtCode the GlycoCT code to resolve.
     */
    @Override
    public Optional<Anomericity> getAnomericity(String glycoCtCode) {

        checkNotNull(glycoCtCode);

        if ("x".equals(glycoCtCode)) return Optional.absent();

        Anomericity anomericity = anomericityGlycoCtMap.get(glycoCtCode);

        if (anomericity == null) {
            throw new IllegalStateException("Cannot resolve the Anomericity from " + glycoCtCode);
        } else {
            return Optional.of(anomericity);
        }
    }

    /**
     * Returns the appropriate LinkageComposition instance for the given GlycoCT code.
     *
     * @param glycoCtCode the GlycoCT code to resolve.
     */
    @Override
    public Optional<Composition> getLinkageComposition(String glycoCtCode) {

        checkNotNull(glycoCtCode);
        if ("x".equals(glycoCtCode)) return Optional.absent();

        Composition linkageComposition = linkCompGlycoCtMap.get(glycoCtCode);

        if (linkageComposition == null) {
            throw new IllegalStateException("Cannot resolve the linkage composition from " + glycoCtCode);
        } else {
            return Optional.of(linkageComposition);
        }
    }


    /**
     * @author Davide Alocci
     * @version sqrt -1.
     */
    static class GlycoCtJsonParser {

        /**
         * Parser for the GlycoCT Lookup file for Monosaccharide
         *
         * @return A map that has the GlycoCT code as a Key and the name of the monosaccharide as a Value.
         */
        private Map<String, String> parseMonosaccharide(String systemProperty, String defaultFileName) {

            return parse(systemProperty, defaultFileName);
        }

        /**
         * Parser for the GlycoCT Lookup file for Substituent
         *
         * @return A map that has the GlycoCT code as a Key and the name of the substituent as a Value.
         */
        public Map<String, String> parseSubstituent(String systemProperty, String defaultFileName) {
            return parse(systemProperty, defaultFileName);
        }

        private Map<String, String> parse(final String systemProperty, final String defaultFileName) {

            String property = System.getProperty(systemProperty, defaultFileName);

            File glycoCTfile = new File(property);
            try (InputStream stream = glycoCTfile.exists() ? new FileInputStream(glycoCTfile) : getClass().getResourceAsStream(property);
                 Reader in = new InputStreamReader(stream, "UTF-8")) {

                return parse(in);
            } catch (UnsupportedEncodingException e) {

                throw new IllegalStateException("The encoding is not supported.", e);
            } catch (FileNotFoundException e) {

                throw new IllegalStateException("GlycoCT Lookup file not found. Please specify a existing file in System Property " + systemProperty + ".  " +
                        ACTUAL_VALUE + property, e);
            } catch (IOException e) {

                throw new IllegalStateException(e);
            }
        }


        private Map<String, String> parse(Reader in) {

            final Map<String, String> lookupMap = new HashMap<>();
            ObjectMapper mapper = new ObjectMapper();

            Preconditions.checkNotNull(in);

            final JsonNode jsonArray;
            try {
                jsonArray = mapper.readTree(in);
            } catch (IOException e) {
                throw new IllegalStateException("Monosaccharide Lookup file error", e);
            }

            String mName = "name";
            String mCTFormat = "glycoCTformat";
            for (final JsonNode jsonNode : jsonArray) {
                JsonNode nodeName = jsonNode.path(mName);
                JsonNode nodeGlycoCtformat = jsonNode.path(mCTFormat);

                checkJsonNode(nodeName, mName);
                checkJsonNode(nodeGlycoCtformat, mCTFormat);
                lookupMap.put(nodeGlycoCtformat.getTextValue(), nodeName.getTextValue());
            }
            return lookupMap;
        }

        private void checkJsonNode(JsonNode node, String nodePath) {
            if (node.isMissingNode())
                throw new IllegalStateException("Mapping not possible. Each record in the GlycoCT Lookup files need to have " + nodePath + ".");
            if ("".equals(node.getTextValue()))
                throw new IllegalStateException("Node without " + nodePath + " are not allowed in the GlycoCT Lookup files Lookup file.");
        }
    }
}