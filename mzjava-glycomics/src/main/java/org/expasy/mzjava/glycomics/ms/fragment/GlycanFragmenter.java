package org.expasy.mzjava.glycomics.ms.fragment;

import com.google.common.base.Preconditions;
import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.AnnotatedPeak;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.glycomics.mol.*;
import org.expasy.mzjava.glycomics.ms.spectrum.GlycanFragAnnotation;
import org.expasy.mzjava.glycomics.ms.spectrum.GlycanSpectrum;

import java.io.Serializable;
import java.util.*;


/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class GlycanFragmenter {

    private final List<GlycanPeakGenerator<GlycanFragAnnotation>> peakGeneratorList = new ArrayList<GlycanPeakGenerator<GlycanFragAnnotation>>();
    private final PeakList.Precision precision;
    private final int maxGlycosidicCuts;
    private final int maxCrossringCuts;

    private final PeakComparator peakComparator = new PeakComparator();

    /**
     * Constructs a glycan fragmenter that generates peaks using the supplied peak generators.
     * @param peakGeneratorList the list of peak generators to use for generating peaks
     * @param precision the precision of the generated GlycanSpectrum
     * @param maxCrossringCuts maximum of crossring cuts
     * @param maxGlycosidicCuts maximum of glycosidic cuts
     */
    public GlycanFragmenter(List<GlycanPeakGenerator<GlycanFragAnnotation>> peakGeneratorList,PeakList.Precision precision, int maxGlycosidicCuts, int maxCrossringCuts) {

        Preconditions.checkNotNull(precision);
        Preconditions.checkNotNull(peakGeneratorList);

        this.precision = precision;
        this.peakGeneratorList.addAll(peakGeneratorList);
        this.maxCrossringCuts = maxCrossringCuts;
        this.maxGlycosidicCuts = maxGlycosidicCuts;
    }

    /**
     * Construct a glycan fragmenter that generates peaks for the supplied ion types.
     * It uses only the default GlycanFragmentPeakGenerator as a peakgenerator.
     * Both glycosidic and crossring fragment are generated depending on the ion type.
     * The intensity of the fragment is set to 1 by default.
     * @param ionTypes the ion types to generate peaks for
     * @param precision the precision of the generated GlycanSpectrum
     * @param intact allowed the generation of the intact fragment
     * @param internal allowed the generation of internal fragments
     * @param maxCrossringCuts maximum of crossring cuts
     * @param maxGlycosidicCuts maximum of glycosidic cuts
     */
    public GlycanFragmenter(Set<IonType> ionTypes, boolean intact, boolean internal, PeakList.Precision precision, int maxGlycosidicCuts, int maxCrossringCuts) {

        Preconditions.checkNotNull(precision);
        Preconditions.checkNotNull(ionTypes);

        this.precision = precision;
        this.maxCrossringCuts = maxCrossringCuts;
        this.maxGlycosidicCuts = maxGlycosidicCuts;
        Set<FragmentType> fragmentTypes = new HashSet<>();

        for(IonType ionType : ionTypes) {
            fragmentTypes.add(ionType.getFragmentType());
        }
        if(intact){
            fragmentTypes.add(FragmentType.INTACT);
        }
        if(internal){
            fragmentTypes.add(FragmentType.INTERNAL);
        }
        peakGeneratorList.add(new GlycanFragmentPeakGenerator(fragmentTypes, ionTypes,1));
    }



    /**
     * Generate a GlycanSpectrum from the fragment in input.
     * It uses all the peak generator in the list to create peaks for each fragment.
     * This method will use all the possible charge from 0 to the charge of the precursor.
     * It creates all the possible fragment (crossring and glycosidic) for the glycan precursor.
     * @param glycan the glycan precursor.
     * @param precursorCharge the charge of the precursor.
     * @return a GlycanSpectrum
     */
    public GlycanSpectrum fragment(Glycan glycan, int precursorCharge) {

        Preconditions.checkNotNull(glycan);

        AllCrossRingCutType allCrossRingCutType = new AllCrossRingCutType();

        int[] charges = new int[precursorCharge];
        for (int i = 0; i < charges.length; i++) {

            charges[i] = i + 1;
        }

        return fragment(glycan, precursorCharge, charges, allCrossRingCutType);
    }


    /**
     * Generate a GlycanSpectrum from the fragment in input.
     * It uses all the peak generator in the list to create peaks for each fragment.
     * This method will use all the possible charge from 0 to the charge of the precursor.
     * It creates all the possible fragment (crossring and glycosidic) for the glycan precursor.
     * The user can define the crossring type of cleavage to take into account using the interface CrossRingCutType.
     * @param glycan the glycan precursor.
     * @param precursorCharge the charge of the precursor.
     * @param crossRingCutType the type of crossring fragments to generate.
     * @return a GlycanSpectrum.
     */
    public GlycanSpectrum fragment(Glycan glycan, int precursorCharge, CrossRingCutType crossRingCutType) {

        int[] charges = new int[precursorCharge];
        for (int i = 0; i < charges.length; i++) {

            charges[i] = i + 1;
        }

        return fragment(glycan, precursorCharge, charges, crossRingCutType);
    }

    /**
     * Generate a GlycanSpectrum from the fragment in input.
     * It uses all the peak generator in the list to create peaks for each fragment.
     * The charges in the fragmentCharges array are use to calculate the m/z of the peaks.
     * It creates all the possible fragment (crossring and glycosidic) for the glycan precursor.
     * The user can define the crossring type of cleavage to take into account using the interface CrossRingCutType.
     * @param glycan the glycan precursor.
     * @param precursorCharge the charge of the precursor.
     * @param fragmentCharges an array of charge.
     * @param crossRingCutType the type of crossring fragments to generate.
     * @return GlycanSpectrum.
     */
    public GlycanSpectrum fragment(Glycan glycan, int precursorCharge, int[] fragmentCharges, CrossRingCutType crossRingCutType) {

        Set<FragmentType> fragmentTypes = new HashSet<FragmentType>();
        for(GlycanPeakGenerator<GlycanFragAnnotation> peakGenerator : peakGeneratorList) {
            fragmentTypes.addAll(peakGenerator.getFragmentTypes());
        }

        List<GlycanFragment> fragmentList = new ArrayList<GlycanFragment>();
        CutDescriptorGenerator generator = new CutDescriptorGenerator();
        List<CutDescriptor> descriptors = generator.generateCutDescriptor(glycan, maxGlycosidicCuts, maxCrossringCuts, crossRingCutType);

        for(CutDescriptor descriptor : descriptors){
            fragmentList.addAll(glycan.getFragment(descriptor));
        }

        return fragment(glycan, precursorCharge, fragmentList, fragmentCharges);
    }

    /**
     * Generate a GlycanSpectrum from the fragment in input.
     * It uses all the peak generator in the list to create peaks for each fragment.
     * The charges in the fragmentCharges array are use to calculate the m/z of the peaks.
     * @param glycan the glycan precursor.
     * @param precursorCharge the charge of the precursor.
     * @param fragmentList the list of fragment
     * @param fragmentCharges an array of charge.
     * @return a GlycanSpectrum.
     */

    public GlycanSpectrum fragment(Glycan glycan, int precursorCharge, List<GlycanFragment> fragmentList, int[] fragmentCharges) {

        List<AnnotatedPeak<GlycanFragAnnotation>> peaks = new ArrayList<AnnotatedPeak<GlycanFragAnnotation>>();

        GlycanSpectrum spectrum = new GlycanSpectrum(glycan, precursorCharge, precision);

        for (GlycanFragment fragment : fragmentList) {

            for (GlycanPeakGenerator<GlycanFragAnnotation> peakGenerator : peakGeneratorList) {

                peakGenerator.generatePeaks(glycan, fragment, fragmentCharges, peaks);
            }
        }

        //Sort peaks (taken from peptidespectrum class)
        Collections.sort(peaks, peakComparator);
        double lastMz = 0;
        for (AnnotatedPeak<GlycanFragAnnotation> peak : peaks) {

            //(taken from peptidespectrum class)
            //Doing this check because sometimes peaks that have exactly the same m/z
            //have the m/z stored in a double that is slightly different
            //to avoid having more than one peaks for these we check for the delta.
            double mz = peak.getMz();
            if(mz - lastMz < 0.000000000001){
                mz = lastMz;
            }
            spectrum.add(mz, peak.getIntensity(), peak.getAnnotations());
            lastMz = mz;
        }

        Peak precursor = spectrum.getPrecursor();
        precursor.setMzAndCharge(glycan.calculateMz(precursorCharge), precursorCharge);
        precursor.setIntensity(1);
        spectrum.trimToSize();
        return spectrum;
    }


    private static class PeakComparator implements Comparator<AnnotatedPeak>, Serializable {

        @Override
        public int compare(AnnotatedPeak p1, AnnotatedPeak p2) {

            return Double.compare(p1.getMz(), p2.getMz());
        }
    }


}
