package org.expasy.mzjava.glycomics.mol;

import java.util.List;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */

public interface MonosaccharideLookup {

    /**
     * Search for a monosaccharide with the input name the list of known monosaccharides.
     * <p>The method throw an IllegalStateException if there is no monosaccharide with the input name in the list.</p>
     * @param name the name of the monosaccharide
     * @return A monosaccharide object with the specified name.
     */
    Monosaccharide getNew(String name);

    /**
     * Return a list of the monosaccharides specified in Monosaccharide Lookup file.
     * @return A list of the known monosaccharides
     */
    List<Monosaccharide> values();
}
