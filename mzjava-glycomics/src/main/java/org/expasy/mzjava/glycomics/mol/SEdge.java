/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.glycomics.mol;

import java.util.*;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 *
 * @param <N> the type of the child node
 * @param <L> the linkage type
 */
public class SEdge<N extends GlycanNode, L extends Linkage> {

    private final List<Monosaccharide> parents;
    private final N child;
    private final L link;

    /**
     * Constructor
     *
     * @param parents the collection of parent nodes
     * @param child   the child node
     * @param link    the link from the parents to the child
     */
    SEdge(List<Monosaccharide> parents, N child, L link) {

        checkNotNull(parents);
        checkNotNull(child);
        checkNotNull(link);
        checkArgument(!parents.isEmpty());

        for (Monosaccharide parent : parents) {

            checkNotNull(parent);
        }

        this.parents = new ArrayList<Monosaccharide>(parents);
        this.child = child;
        this.link = link;
    }

    /**
     * Constructor
     *
     * @param parent the parent node
     * @param child   the child node
     * @param link    the link from the parents to the child
     */
    SEdge(Monosaccharide parent, N child, L link) {

        checkNotNull(parent);
        checkNotNull(child);
        checkNotNull(link);

        this.parents = new ArrayList<Monosaccharide>(1);
        this.parents.add(parent);
        this.child = child;
        this.link = link;
    }

    public Monosaccharide getParent() {

        return parents.get(0);
    }

    public List<Monosaccharide> getParents() {

        return Collections.unmodifiableList(parents);
    }

    public N getChild() {

        return child;
    }

    public L getLink() {

        return link;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SEdge)) return false;

        SEdge that = (SEdge) o;

        if (!child.equals(that.child)) return false;
        if (!link.equals(that.link)) return false;
        if (parents.size() != that.parents.size()) return false;
        if (parents.size() == 1){
            return parents.get(0).equals(that.parents.get(0));
        }
        Set<Monosaccharide> parentsSet = new HashSet<Monosaccharide>();
        parentsSet.addAll(parents);
        parentsSet.addAll(that.parents);
        return parentsSet.size() == parents.size();
    }

    @Override
    public int hashCode() {
        int result = parents.hashCode();
        result = 31 * result + child.hashCode();
        result = 31 * result + link.hashCode();
        return result;
    }

    public boolean isHyperEdge() {

        return parents.size() > 1;
    }

    @Override
    public String toString() {

        return "SEdge{" +
                "parents=" + parents +
                ", child=" + child +
                ", link=" + link +
                '}';
    }
}
