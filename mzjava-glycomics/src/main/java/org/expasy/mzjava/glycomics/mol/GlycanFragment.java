/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.glycomics.mol;

import com.google.common.base.Preconditions;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.mol.PeriodicTable;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.utils.Counter;
import org.openide.util.Lookup;

import java.util.*;

/**
 * @author Oliver Horlacher
 * @author Davide Alocci
 * @version sqrt -1
 */
public class GlycanFragment extends SaccharideGraph {


    private final FragmentType fragmentType;
    private final Composition endComposition;
    private final Map<CleavedMonosaccharide,IonType> monosaccharideIonTypeMap;
    private final Map<SEdge,IonType> edgeIonTypeMap;


    protected GlycanFragment(Monosaccharide root, FragmentType fragmentType, List<GlycanNode> nodes, List<SEdge> edges, IdentityEdgeMultimap<Monosaccharide, Monosaccharide, GlycosidicEdge> monosaccharideEdgeMultimap, IdentityEdgeMultimap<Monosaccharide, Substituent, SubstituentEdge> substituentEdgeMultimap, Composition endComposition, Map<CleavedMonosaccharide,IonType> monosaccharideIonTypeMap, Map<SEdge,IonType> edgeIonTypeMap) {
        super(root, nodes, edges, monosaccharideEdgeMultimap, substituentEdgeMultimap);
        Preconditions.checkNotNull(endComposition);
        Preconditions.checkNotNull(fragmentType);
        Preconditions.checkNotNull(monosaccharideIonTypeMap);
        Preconditions.checkNotNull(edgeIonTypeMap);
        this.monosaccharideIonTypeMap = monosaccharideIonTypeMap;
        this.edgeIonTypeMap = edgeIonTypeMap;
        this.endComposition = endComposition;
        this.fragmentType = fragmentType;


    }

    protected GlycanFragment(Monosaccharide root, FragmentType fragmentType, Composition endComposition){
        super(root);
        Preconditions.checkNotNull(endComposition);
        Preconditions.checkNotNull(fragmentType);
        this.monosaccharideIonTypeMap = new HashMap<>();
        this.edgeIonTypeMap = new HashMap<>();
        this.endComposition = endComposition;
        this.fragmentType = fragmentType;
    }



    /**
     * Return the mass of the this fragment.
     * @return the mass of the fragment.
     */
    public double calculateMolecularMass(){

        GlycanMassCalculator glycanMassCalculator = Lookup.getDefault().lookup(GlycanMassCalculator.class);
        double mass = calculateTotalSaccharideMass() + endComposition.getMolecularMass();

        List<Composition> glycosidicCompositionList = new ArrayList<>();
        for(Map.Entry<SEdge,IonType> entry : edgeIonTypeMap.entrySet()){
            glycosidicCompositionList.add(glycanMassCalculator.getGlycosidicDeltaComposition(entry.getValue()));
        }

        Composition glycosidicSumComposition = new Composition(glycosidicCompositionList.toArray(new Composition[glycosidicCompositionList.size()]));

        mass += glycosidicSumComposition.getMolecularMass();

        for(Map.Entry<CleavedMonosaccharide,IonType> entry : monosaccharideIonTypeMap.entrySet()){
            if(IonType.x.equals(entry.getValue())){
                mass = mass - glycanMassCalculator.getCrossRingLossMass(entry.getValue(), getAnomericCarbon(entry.getKey().getMonosaccharide()), entry.getKey().getMonosaccharide(), entry.getKey().getCutIndexes());
            } else if (IonType.a.equals(entry.getValue())){
                mass = mass - glycanMassCalculator.getCrossRingLossMass(entry.getValue(), getAnomericCarbon(entry.getKey().getMonosaccharide()) , entry.getKey().getMonosaccharide(), entry.getKey().getCutIndexes());
            }
        }

        if(Math.abs(glycosidicSumComposition.getCharge()) + monosaccharideIonTypeMap.size() > 1){
            mass = mass + (PeriodicTable.H_MASS - PeriodicTable.ELECTRON_MASS)*(Math.abs(glycosidicSumComposition.getCharge()) + monosaccharideIonTypeMap.size() -1);
        }

        return mass ;
    }


    private int getAnomericCarbon(Monosaccharide monosaccharide){
        //Todo: remove this function and create an object with cut direction and ion.
        for (SEdge edge : edges){
            if(edge.getChild() == monosaccharide && edge instanceof GlycosidicEdge){
                GlycosidicEdge glycosidicEdge = (GlycosidicEdge) edge;
                return glycosidicEdge.getLink().getAnomericCarbon().get();
            }
        }

        for (Map.Entry<SEdge, IonType> entry : edgeIonTypeMap.entrySet()) {
            if(entry.getKey().getChild() == monosaccharide && entry.getKey() instanceof GlycosidicEdge){
                GlycosidicEdge glycosidicEdge = (GlycosidicEdge) entry.getKey();
                return glycosidicEdge.getLink().getAnomericCarbon().get();

            }
        }
        return monosaccharide.getFirstCarbonRing();
    }

    /**
     * Return a set of cleaved nodes in the fragment
     * @return a set
     */
    public Set<CleavedMonosaccharide> getCleavedNodes(){
        return Collections.unmodifiableSet(monosaccharideIonTypeMap.keySet());
    }

    /**
     * Return a set of cleaved edge in the fragment
     * @return a set
     */
    public Set<SEdge> getCleavedEdges(){
        return Collections.unmodifiableSet(edgeIonTypeMap.keySet());
    }

    /**
     * Return the fragment type.
     * @return the fragment type.
     */
    public FragmentType getFragmentType() {
        return fragmentType;
    }

    /**
     * Return the composition of the end of the glycan.
     * @return the composition of the end of the glycan.
     */
    public Composition getEndComposition(){
        return endComposition;
    }


    public IonType getIonType(SEdge edge){

        Preconditions.checkNotNull(edge);
        if(edgeIonTypeMap.containsKey(edge)){
            return edgeIonTypeMap.get(edge);
        } else {
            throw new IllegalStateException("The edge specified is not fragmented in this fragment.");
        }

    }

    public IonType getIonType(CleavedMonosaccharide cleavedMonosaccharide){

        Preconditions.checkNotNull(cleavedMonosaccharide);
        if(monosaccharideIonTypeMap.containsKey(cleavedMonosaccharide)){
            return monosaccharideIonTypeMap.get(cleavedMonosaccharide);
        } else {
            throw new IllegalStateException("The monosaccharide specified is not fragmented in this fragment.");
        }
    }

    public Set<IonType> getIonTypes(){
        return Collections.unmodifiableSet(new HashSet<>(edgeIonTypeMap.values()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GlycanFragment)) return false;
        if (!super.equals(o)) return false;

        GlycanFragment fragment = (GlycanFragment) o;

        if (!edgeIonTypeMap.equals(fragment.edgeIonTypeMap)) return false;
        if (!endComposition.equals(fragment.endComposition)) return false;
        if (fragmentType != fragment.fragmentType) return false;
        if (!monosaccharideIonTypeMap.equals(fragment.monosaccharideIonTypeMap)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + fragmentType.hashCode();
        result = 31 * result + endComposition.hashCode();
        result = 31 * result + monosaccharideIonTypeMap.hashCode();
        result = 31 * result + edgeIonTypeMap.hashCode();
        return result;
    }

    private void addCleavedNode(Monosaccharide monosaccharide, CutIndexes cutIndexes, IonType ionType){
        Preconditions.checkNotNull(monosaccharide);
        Preconditions.checkNotNull(cutIndexes);
        monosaccharideIonTypeMap.put(new CleavedMonosaccharide(monosaccharide, cutIndexes),ionType);
    }

    private void addCleavedSEdge(SEdge edge, IonType ionType){
        Preconditions.checkNotNull(edge);
        edgeIonTypeMap.put(edge,ionType);
    }



    /**
     * A builder to build Glycosidic Fragments.  This builder is not reusable.
     */
    public static class Builder extends AbstractBuilder<GlycanFragment>{


        String errorMessage = "A cleaved monosaccharide must be a A or X IonType.";
        /**
         * Set the root of the Glycan Fragments. Can only be called once.
         *
         * @param root the monosaccharide that is to be the root
         * @param fragmentType the fragment type.
         * @return the monosaccharide that becomes root.
         */
        public Monosaccharide setRoot(Monosaccharide root, FragmentType fragmentType){

            Composition.Builder compositionBuilder = new Composition.Builder();
            return setRoot(root,fragmentType,compositionBuilder.build());
        }


        /**
         * Set the cleaved root of the Glycan Fragments. Can only be called once.
         *
         * @param root the monosaccharide that is to be the root
         * @param fragmentType the fragment type.
         * @param cutIndexes the cutIndex of the root.
         * @return the monosaccharide that becomes root.
         */
        public Monosaccharide setCleavedRoot(Monosaccharide root, FragmentType fragmentType, CutIndexes cutIndexes){

            Composition.Builder compositionBuilder = new Composition.Builder();
            return setCleavedRoot(root, fragmentType, compositionBuilder.build(), cutIndexes);
        }


        /**
         * Set the cleaved root of the Glycan Fragments. Can only be called once.
         *
         * @param root the monosaccharide that is to be the root
         * @param fragmentType the fragment type.
         * @param endComposition  the composition of the end of the fragment.
         * @param cutIndexes  the cutIndex of the root.
         * @return the monosaccharide that becomes root.
         */
        public Monosaccharide setCleavedRoot(Monosaccharide root, FragmentType fragmentType, Composition endComposition, CutIndexes cutIndexes){

            Preconditions.checkNotNull(root);
            Preconditions.checkNotNull(fragmentType);
            Preconditions.checkNotNull(endComposition);
            Preconditions.checkNotNull(cutIndexes);

            if (buildSate != BuildSate.NEW) {
                throw new IllegalStateException("Root has already been set and cannot be set again");
            }

            buildSate = BuildSate.BUILDING;
            graph = new GlycanFragment(root, fragmentType, endComposition);

            if(FragmentType.REVERSE.equals(fragmentType)){
                graph.addCleavedNode(root,cutIndexes,IonType.x);
            }

            if(fragmentType.equals(FragmentType.FORWARD) || fragmentType.equals(FragmentType.INTERNAL)){
                graph.addCleavedNode(root,cutIndexes,IonType.a);
            }
            return root;
        }

        /**
         * Set the root of the Glycan Fragments. Can only be called once.
         *
         * @param root the monosaccharide that is to be the root
         * @param fragmentType the fragment type.
         * @param endComposition  the composition of the end of the fragment.
         * @return the monosaccharide that becomes root.
         */
        public Monosaccharide setRoot(Monosaccharide root, FragmentType fragmentType, Composition endComposition){

            Preconditions.checkNotNull(root);
            Preconditions.checkNotNull(fragmentType);
            Preconditions.checkNotNull(endComposition);

            if (buildSate != BuildSate.NEW) {
                throw new IllegalStateException("Root has already been set and cannot be set again");
            }

            buildSate = BuildSate.BUILDING;
            graph = new GlycanFragment(root, fragmentType, endComposition);

            return root;
        }


        /**
         * Add a Monosaccharide to the Glycan.  The <code>cleavedMonosaccharide</code> is attached to the <code>pattern</code> via the
         * <code>linkage</code>. If the parent has not been added to this glycan an IllegalStateException is thrown.
         * @param cleavedMonosaccharide the cleavedMonosaccharide to add
         * @param parent Monosaccharide to which <code>cleavedMonosaccharide</code> is to be added
         * @param linkage the linkage
         * @return the passed in cleavedMonosaccharide
         */
        public Monosaccharide addCleavedMonosaccharide(Monosaccharide cleavedMonosaccharide, Monosaccharide parent, GlycosidicLinkage linkage, CutIndexes cutIndexes, IonType ionType) {

            Preconditions.checkNotNull(cleavedMonosaccharide);
            Preconditions.checkNotNull(parent);
            Preconditions.checkNotNull(linkage);
            Preconditions.checkNotNull(cutIndexes);
            Preconditions.checkNotNull(ionType);

            if(!IonType.a.equals(ionType) && !IonType.x.equals(ionType)){
                throw  new IllegalArgumentException(errorMessage);
            }

            checkIsBuilding();

            graph.addCleavedNode(cleavedMonosaccharide,cutIndexes,ionType);

            graph.add(cleavedMonosaccharide, parent, linkage);

            return cleavedMonosaccharide;
        }

        /**
         * Add <code>cleavedMonosaccharide</code> and <code>substituent</code> to the Glycan being built. The <code>cleavedMonosaccharide</code>
         * is attached to the <code>parent</code> using the <code>glycoLinkage</code> and the <code>substituent</code> is attached
         * to the <code>cleavedMonosaccharide</code> using the <code>substituentLinkage</code>.
         *
         * <p>This method adds two nodes and two edges to the Glycan being built.
         *
         * <pre>
         *     substituent <-- substituentLinkage -- cleavedMonosaccharide <-- glycoLinkage <-- parent
         * </pre>
         *
         * <p>If the parent has not been added to this glycan an IllegalStateException is thrown.
         *
         * @param cleavedMonosaccharide the Monosaccharide to add
         * @param substituent the cleaved monosaccharide substituent
         * @param parent the parent to which the cleavedMonosaccharide is to be attached
         * @param glycoLinkage the linkage between <code>parent</code> and <code>cleavedMonosaccharide</code>
         * @param substituentLinkage the linkage between <code>cleavedMonosaccharide</code> and <code>substituent</code>
         * @return the cleavedMonosaccharide
         */
        public Monosaccharide addCleavedMonosaccharide(Monosaccharide cleavedMonosaccharide, Substituent substituent, Monosaccharide parent, GlycosidicLinkage glycoLinkage, SubstituentLinkage substituentLinkage, CutIndexes cutIndexes, IonType ionType) {

            Preconditions.checkNotNull(cleavedMonosaccharide);
            Preconditions.checkNotNull(parent);
            Preconditions.checkNotNull(substituent);
            Preconditions.checkNotNull(glycoLinkage);
            Preconditions.checkNotNull(substituentLinkage);
            Preconditions.checkNotNull(cutIndexes);
            Preconditions.checkNotNull(ionType);

            if(!IonType.a.equals(ionType) && !IonType.x.equals(ionType)){
                throw  new IllegalArgumentException(errorMessage);
            }

            checkIsBuilding();


            graph.addCleavedNode(cleavedMonosaccharide,cutIndexes,ionType);

            graph.add(cleavedMonosaccharide, parent, glycoLinkage);

            graph.add(substituent, cleavedMonosaccharide, substituentLinkage);

            return cleavedMonosaccharide;
        }


        /**
         * Add an hyper edge from the list of parents to the cleavedMonosaccharide.
         *
         * @param cleavedMonosaccharide the cleavedMonosaccharide to add
         * @param parents the list of parents
         * @param linkage the linkage between the parents and the cleavedMonosaccharide
         * @return the cleavedMonosaccharide
         */
        public Monosaccharide addCleavedMonosaccharide(Monosaccharide cleavedMonosaccharide, List<Monosaccharide> parents, GlycosidicLinkage linkage, CutIndexes cutIndexes, IonType ionType) {

            checkIsBuilding();

            Preconditions.checkNotNull(cleavedMonosaccharide);
            Preconditions.checkNotNull(parents);
            Preconditions.checkNotNull(linkage);
            Preconditions.checkNotNull(cutIndexes);
            Preconditions.checkNotNull(ionType);

            if(!IonType.a.equals(ionType) && !IonType.x.equals(ionType)){
                throw  new IllegalArgumentException(errorMessage);
            }

            graph.addCleavedNode(cleavedMonosaccharide,cutIndexes, ionType);

            graph.add(cleavedMonosaccharide, parents, linkage);

            return cleavedMonosaccharide;
        }

        /**
         * Add a cleaved edge to the fragment.
         * @param attachPoint the monosaccharide where the edge is attach. It could be the parent or the child.
         * @param edge the cleaved edge
         * @return the cleaved edge
         */

        public SEdge addCleavedEdge(Monosaccharide attachPoint, SEdge edge, IonType ionType){

            Preconditions.checkNotNull(ionType);
            Preconditions.checkNotNull(attachPoint);
            Preconditions.checkNotNull(edge);

            if(IonType.a.equals(ionType) || IonType.x.equals(ionType)){
                throw  new IllegalArgumentException("A cleaved edge must be a Y,Z,B or C IonType.");
            }

            if(checkMonosaccharidesPresence(Collections.singletonList(attachPoint))){
                graph.addCleavedSEdge(edge, ionType);
            } else {
                throw new IllegalArgumentException("The attach point is not present in the fragment.");
            }
            return edge;
        }


        /**
         * Build the Glycosidic Fragments Fragment.
         *
         * @return the new Glycosidic Fragments Fragment
         */

        public GlycanFragment build () {

            checkIsBuilding();

            GlycanFragment tmp = graph;
            Counter counter = new Counter();
            tmp.populatePrePostOrder(tmp.root, counter);
            graph = null;

            buildSate = BuildSate.DONE;
            return tmp;
        }
    }
}

