/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.glycomics.mol;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.utils.Counter;
import org.expasy.mzjava.utils.MixedRadixNtupleGenerator;
import org.openide.util.Lookup;

import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Data structure to represent a Glycan.
 * <p/>
 * The glycan is represented as a rooted directed acyclic graph.
 * <p/>
 * To represent uncertainty in linkage Glycans can have hyper edges that connect 1..n parent nodes to 1 child node.
 * <p/>
 * Glycans are immutable, Glycans are constructed using the builder pattern:
 * <pre>
 *
 *     Glycan.Builder builder = new Glycan.Builder()
 *     Monosaccharide glc = builder.setRoot(MonosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "A");
 *     Monosaccharide gal = builder.add(MonosaccharideLookup.getNew("Gal"), glc, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));
 *     Glycan glycan = builder.build();
 * </pre>
 *
 * @author Oliver Horlacher
 * @author Davide Alocci
 * @version sqrt -1
 */
public class Glycan extends SaccharideGraph {

    private final Optional<Anomericity> reducingEndAnomericity;
    private final String databaseIdentifier;
    private final Composition endComposition;

    private final IdentityEdgeMultimap<Monosaccharide, CutIndexes, Set<GlycanNode>> monosaccharideCleavedMap = new IdentityEdgeMultimap<>(1);

    protected Glycan(Monosaccharide root, Optional<Anomericity> reducingEndAnomericity, String databaseIdentifier, Composition endComposition) {

        super(root);
        checkNotNull(reducingEndAnomericity);
        checkNotNull(databaseIdentifier);
        checkNotNull(endComposition);

        this.endComposition = endComposition;
        this.reducingEndAnomericity = reducingEndAnomericity;
        this.databaseIdentifier = databaseIdentifier;
    }

    /**
     * Return the composition of the end of the glycan.
     * @return the composition of the end of the glycan.
     */
    public Composition getEndComposition(){
        return endComposition;
    }

    /**
     * Return the molecular mass of this glycan.
     * @return the molecular mass of this glycan.
     */

    public double calculateMass() {
        return super.calculateTotalSaccharideMass() + endComposition.getMolecularMass();
    }

    /**
     * Return the mass/charge of this glycan.
     * @param charge the specific charge for the calculation.
     * @return the mass/charge of this glycan.
     */
    public double calculateMz(int charge) {
        Preconditions.checkArgument(charge != 0, "Cannot calculate m/z, charge was 0");
        GlycanMassCalculator glycanMassCalculator = Lookup.getDefault().lookup(GlycanMassCalculator.class);
        return glycanMassCalculator.calculateMz(calculateMass(), charge, FragmentType.INTACT);
    }

    /**
     * Return the database identifier associated with this glycan.
     *
     * @return the database identifier associated with this glycan
     */
    public String getDatabaseIdentifier() {

        return this.databaseIdentifier;
    }

    public Optional<Anomericity> getReducingEndAnomericity() {

        return reducingEndAnomericity;
    }

    public List<GlycanFragment> getFragment(CutDescriptor cutDescriptor){

        //Todo: check if the extendedcutDesc is compatible with this glycan.
        List<GlycanFragment> fragments  = new ArrayList<>();
        if(!checkMissingEdges(cutDescriptor.getCleavedNodes())){
            return fragments;
        }

        computeCleavedNodeFromCuts(cutDescriptor.getCleavedEdges(), cutDescriptor.getCleavedNodes(), cutDescriptor, false, false, this.root, fragments);

        for(SEdge edge : cutDescriptor.getCleavedEdges()){
            Set<SEdge> filterEdgeSet = new HashSet<>(cutDescriptor.getCleavedEdges());
            filterEdgeSet.remove(edge);
            computeCleavedNodeFromCuts(filterEdgeSet, cutDescriptor.getCleavedNodes(), cutDescriptor, true, true, (Monosaccharide) edge.getChild(), fragments);
        }

        for(Monosaccharide node : cutDescriptor.getCleavedNodes()){
            computeCleavedNodeFromCuts(cutDescriptor.getCleavedEdges(), cutDescriptor.getCleavedNodes(), cutDescriptor, true, false, node, fragments);
        }
        return fragments;
    }


    private boolean checkMissingEdges(Set<Monosaccharide> monosaccharides){

        for(Monosaccharide node : monosaccharides){
            if(!this.canFragmentCrossRing(node)) {
               return false;
            }
        }
        return true;
    }


    private int getParentLinkedCarbon(Monosaccharide monosaccharide){
        GlycanNode parent = getParent(monosaccharide);
        int parentLinkCarbon;
        if(monosaccharide != this.root){
            parentLinkCarbon = getGlycoLinkage(parent,monosaccharide).get().getAnomericCarbon().get();
        } else {
            parentLinkCarbon = monosaccharide.getFirstCarbonRing();
        }
        return parentLinkCarbon;
    }

    private Set<GlycanNode> getCleavedNodeSet(Monosaccharide monosaccharide, CutIndexes cutIndexes, boolean reverseFragment){

        if(monosaccharideCleavedMap.get(monosaccharide,cutIndexes) == null ){
            addMonosaccharideCleavedMap(monosaccharide,cutIndexes);
        }

        Set<GlycanNode> childrenSet;

        if(reverseFragment){
            childrenSet = monosaccharideCleavedMap.get(monosaccharide,cutIndexes);

        } else {
            childrenSet = this.getChildren(monosaccharide);
            childrenSet.removeAll(monosaccharideCleavedMap.get(monosaccharide, cutIndexes));
        }
        return childrenSet;
    }

    private void addMonosaccharideCleavedMap(Monosaccharide monosaccharide, CutIndexes cutIndexes){

        Set<GlycanNode> cleavedNode =  Sets.newSetFromMap(new IdentityHashMap<GlycanNode, Boolean>());
        for(GlycanNode child : this.getChildren(monosaccharide)){
            if(monosaccharide.checkLinkedCarbon(cutIndexes,CutDirection.CLOCK_WISE,getLinkedcarbon(monosaccharide,child))){
                cleavedNode.add(child);
            }
        }

        if(monosaccharide.checkLinkedCarbon(cutIndexes,CutDirection.CLOCK_WISE,getParentLinkedCarbon(monosaccharide))){
            Set<GlycanNode> complementaryCleavedNode = this.getChildren(monosaccharide);
            complementaryCleavedNode.removeAll(cleavedNode);
            monosaccharideCleavedMap.put(monosaccharide, cutIndexes, complementaryCleavedNode);
        } else {
            monosaccharideCleavedMap.put(monosaccharide, cutIndexes, cleavedNode);
        }
    }


    private void computeCleavedNodeFromCuts(Set<SEdge> cleavedEdgeSet, Set<Monosaccharide> cleavedMonosaccharideSet, CutDescriptor cutDescriptor, boolean isRootCut, boolean startWithEdge, Monosaccharide fragmentRoot, List<GlycanFragment> fragments){

        List<GlycanNode> cleavedNode = new ArrayList<>();

        for(SEdge edge : cleavedEdgeSet){
            cleavedNode.add(edge.getChild());
        }


        if(cleavedMonosaccharideSet.contains(fragmentRoot)) {
            if(startWithEdge){
                cleavedNode.addAll(getCleavedNodeSet(fragmentRoot, cutDescriptor.getCutIndexesForMonosaccharide(fragmentRoot), true));
            } else {
                if(isRootCut){
                    cleavedNode.addAll(getCleavedNodeSet(fragmentRoot, cutDescriptor.getCutIndexesForMonosaccharide(fragmentRoot), false));
                } else {
                    cleavedNode.addAll(getCleavedNodeSet(fragmentRoot, cutDescriptor.getCutIndexesForMonosaccharide(fragmentRoot), true));
                }
            }
        }


        for(Monosaccharide monosaccharide : cleavedMonosaccharideSet){
            if(monosaccharide != fragmentRoot) {
                cleavedNode.addAll(getCleavedNodeSet(monosaccharide, cutDescriptor.getCutIndexesForMonosaccharide(monosaccharide), true));
            }
        }

        Set<GlycanNode> cleavedNodeSet =  Sets.newIdentityHashSet();
        cleavedNodeSet.addAll(cleavedNode);
        if(cleavedNodeSet.size() == cleavedNode.size() && cleavedNodeSet.size() == filterCutNodesList(fragmentRoot,cleavedNodeSet).size()){
            cutFragment(fragmentRoot,filterCutNodesList(fragmentRoot, cleavedNodeSet),isRootCut,cutDescriptor,fragments,startWithEdge);
        }
    }




    private int getLinkedcarbon(GlycanNode parent, GlycanNode child){
        Preconditions.checkNotNull(parent);
        Preconditions.checkNotNull(child);
         if(child instanceof Monosaccharide){
             return getGlycoLinkage(parent,child).get().getLinkedCarbon().get();
         }

        if(child instanceof Substituent){
            return getSubstituentLinkage(parent,child).get().getLinkedCarbon().get();
        }

        throw new IllegalStateException("Unknown node");
    }



    private Set<GlycanNode> filterCutNodesList(Monosaccharide fragmentRoot, Collection<GlycanNode> cutNodesList){
        Set<GlycanNode> filteredList =  Sets.newSetFromMap(new IdentityHashMap<GlycanNode, Boolean>());
        for(GlycanNode node : cutNodesList){
            if(preOrder.get(node) > preOrder.get(fragmentRoot) &&  postOrder.get(node) <  postOrder.get(fragmentRoot)){
                filteredList.add(node);
            }
        }
        return filteredList;
    }

    private void cutFragment(Monosaccharide fragmentRoot, Set<GlycanNode> cleavedNodeList, boolean isRootCut, CutDescriptor cutDescriptor, List<GlycanFragment> fragments, boolean startWithEdge) {

        Set<SEdge> edgeSetCutDescNodes = Collections.newSetFromMap(new IdentityHashMap<SEdge, Boolean>());

        for (SEdge edge : edges) {
            if (edge.isHyperEdge()){
                throw new IllegalStateException("Cannot fragment Glycan that contains hyper edges");
            }

            GlycanNode child = edge.getChild();

            if (checkNodePresence(child, fragmentRoot, cleavedNodeList)){
                edgeSetCutDescNodes.add(edge);
            }
        }

       generateGlycanFragment(fragmentRoot, edgeSetCutDescNodes, isRootCut, cutDescriptor, fragments, startWithEdge);
    }


    private FragmentType checkCuttedFragments(Monosaccharide fragmentRoot, CutDescriptor cutDescriptor, int actualCuts, boolean isRootCut){

        if(actualCuts == cutDescriptor.getNumberOfCut()){
            if(actualCuts == 0){
                return FragmentType.INTACT;
            }
            if(fragmentRoot == this.root && !isRootCut){
                return FragmentType.REVERSE;
            }
            if(actualCuts == 1){
                return FragmentType.FORWARD;
            }
            return FragmentType.INTERNAL;
        }

        return FragmentType.UNKNOWN;
    }



    private void generateGlycanFragment(Monosaccharide root, Set<SEdge> edgeSet, boolean isRootCut, CutDescriptor cutDescriptor, List<GlycanFragment> fragments, boolean startWithEdge) {

        final List<GlycanNode> cNodes = new ArrayList<>();
        final List<SEdge> cEdges = new ArrayList<>();
        final IdentityEdgeMultimap<Monosaccharide, Monosaccharide, GlycosidicEdge> cMonosaccharideEdgeMultimap = new IdentityEdgeMultimap<>(2);
        final IdentityEdgeMultimap<Monosaccharide, Substituent, SubstituentEdge> cSubstituentEdgeMultimap = new IdentityEdgeMultimap<>(2);


        Set<GlycanNode> identityNodeHashSet = Collections.newSetFromMap(new IdentityHashMap<GlycanNode, Boolean>());
        Set<SEdge> identityEdgeHashSet = Collections.newSetFromMap(new IdentityHashMap<SEdge, Boolean>());
        identityNodeHashSet.add(root);


        for (SEdge edge : edgeSet) {
            if (edge.getParent() != root) {
                identityNodeHashSet.add(edge.getParent());
            }
            identityNodeHashSet.add(edge.getChild());
            if (edge instanceof GlycosidicEdge) {

                GlycosidicEdge glycoEdge = (GlycosidicEdge) edge;
                Monosaccharide parent = glycoEdge.getParent();
                Monosaccharide child = glycoEdge.getChild();
                GlycosidicEdge newEdge = new GlycosidicEdge(parent, child, glycoEdge.getLink());
                cMonosaccharideEdgeMultimap.put(parent, child, newEdge);
                identityEdgeHashSet.add(newEdge);
            } else if (edge instanceof SubstituentEdge) {

                SubstituentEdge modEdge = (SubstituentEdge) edge;
                Monosaccharide parent = modEdge.getParent();
                Substituent child = modEdge.getChild();
                SubstituentEdge newEdge = new SubstituentEdge(parent, child, modEdge.getLink());
                cSubstituentEdgeMultimap.put(parent, child, newEdge);
                identityEdgeHashSet.add(newEdge);
            } else {
                throw new IllegalStateException(edge.getClass() + "is an unknown edge type");
            }
        }

        cNodes.addAll(identityNodeHashSet);
        cEdges.addAll(identityEdgeHashSet);

        int filterCleavedNodesCounter = 0;
        int filterCleavedEdgeCounter = 0;
        for(Monosaccharide monosaccharide : cutDescriptor.getCleavedNodes()){
            if(containsGlycanNode(cNodes,monosaccharide)){
                filterCleavedNodesCounter++;
            }
        }

        for(SEdge edge : cutDescriptor.getCleavedEdges()){
            if(containsGlycanNode(cNodes,edge.getParent()) && !containsGlycanNode(cNodes,edge.getChild())){
                filterCleavedEdgeCounter++;
            }
            if(startWithEdge && containsGlycanNode(cNodes,edge.getChild()) && !containsGlycanNode(cNodes,edge.getParent())){
                filterCleavedEdgeCounter++;
            }
        }

        FragmentType fragmentType = checkCuttedFragments(root, cutDescriptor,filterCleavedEdgeCounter+filterCleavedNodesCounter,isRootCut);


        if(!fragmentType.equals(FragmentType.UNKNOWN)){
            List<Map<SEdge,IonType>> listIonMapEdges = generateIonSetEdge(cutDescriptor,startWithEdge,root);
            for(Map<SEdge,IonType> map : listIonMapEdges){
                if(FragmentType.REVERSE.equals(fragmentType) || FragmentType.INTACT.equals(fragmentType)){
                    fragments.add(new GlycanFragment(root,fragmentType, cNodes, cEdges, cMonosaccharideEdgeMultimap, cSubstituentEdgeMultimap, this.endComposition, generateIonSetMonosaccharide(fragmentType,cutDescriptor, startWithEdge, root), map));
                } else {
                    fragments.add(new GlycanFragment(root,fragmentType, cNodes, cEdges, cMonosaccharideEdgeMultimap, cSubstituentEdgeMultimap, Composition.parseComposition(""), generateIonSetMonosaccharide(fragmentType,cutDescriptor, startWithEdge, root), map));
                }

            }
        }
    }


    private Map<CleavedMonosaccharide,IonType> generateIonSetMonosaccharide(FragmentType fragmentType, CutDescriptor cutDescriptor, boolean startWithEdge, Monosaccharide fragmentRoot){

        Map<CleavedMonosaccharide,IonType> resultMap = new HashMap<>();

        if(FragmentType.REVERSE.equals(fragmentType)){

            for(CleavedMonosaccharide cleavedMonosaccharide : cutDescriptor.getCleavedMonosaccharides()){
                resultMap.put(cleavedMonosaccharide,IonType.x);
            }

        } else if(FragmentType.FORWARD.equals(fragmentType)){

            if(!startWithEdge){
                resultMap.put(new CleavedMonosaccharide(fragmentRoot, cutDescriptor.getCutIndexesForMonosaccharide(fragmentRoot)),IonType.a);
            }

        } else if(FragmentType.INTERNAL.equals(fragmentType)){

            for(CleavedMonosaccharide cleavedMonosaccharide : cutDescriptor.getCleavedMonosaccharides()){
                if(!startWithEdge && cleavedMonosaccharide.getMonosaccharide() == fragmentRoot){
                    resultMap.put(cleavedMonosaccharide,IonType.a);
                } else {
                    resultMap.put(cleavedMonosaccharide,IonType.x);
                }
            }

        }
        return resultMap;
    }

    private List<Map<SEdge,IonType>> generateIonSetEdge(CutDescriptor cutDescriptor, boolean startWithEdge, Monosaccharide fragmentRoot){

        List<Map<SEdge,IonType>>  edgeIonTypeMapList = new ArrayList<>();

        if(cutDescriptor.getCleavedEdges().isEmpty()){
            edgeIonTypeMapList.add(new IdentityHashMap<SEdge, IonType>());
            return edgeIonTypeMapList;
        }

        SEdge startingEdge = null;

        if(startWithEdge){
            for(SEdge edge : cutDescriptor.getCleavedEdges()){
                if(edge.getChild() == fragmentRoot){
                    startingEdge = edge;
                }
            }
        }

        glycosidicIonCombinationGenerator(new ArrayList<>(cutDescriptor.getCleavedEdges()),edgeIonTypeMapList,startingEdge);
        return edgeIonTypeMapList;
    }


    private void glycosidicIonCombinationGenerator(List<SEdge> edgeList, List<Map<SEdge,IonType>> results, SEdge startingEdge){

        List<IonType> reverseIonTypes = new ArrayList<>();
        reverseIonTypes.add(IonType.y);
        reverseIonTypes.add(IonType.z);

        List<IonType> forwardIonTypes = new ArrayList<>();
        forwardIonTypes.add(IonType.b);
        forwardIonTypes.add(IonType.c);

        MixedRadixNtupleGenerator.NtupleContainer ntupleContainer = new MixedRadixNtupleGenerator.NtupleContainer();
        MixedRadixNtupleGenerator combinationGenerator = new MixedRadixNtupleGenerator(ntupleContainer);
        combinationGenerator.generate(edgeList.size(),2);

        for(int[]comb : ntupleContainer.getNtuples()){
            Map<SEdge,IonType> sEdgeIonTypeMap = new IdentityHashMap<>();
            if(comb.length != edgeList.size()){
                throw new IllegalStateException("Error glycosidicIonCombinationGenerator: the lenght of the edge list must be the same of the generated combination.");
            }

            for( int i = 0; i <comb.length; i++){
                if(edgeList.get(i) == startingEdge){
                    sEdgeIonTypeMap.put(edgeList.get(i),forwardIonTypes.get(comb[i]));
                } else {
                    sEdgeIonTypeMap.put(edgeList.get(i),reverseIonTypes.get(comb[i]));
                }
            }
            results.add(sEdgeIonTypeMap);
        }
    }

    private boolean containsGlycanNode(Collection<GlycanNode> nodeList , GlycanNode node){
        for(GlycanNode n : nodeList){
            if(n == node){
                return true;
            }
        }
        return false;
    }

    private boolean checkNodePresence(GlycanNode child, Monosaccharide fragmentRoot, Set<GlycanNode> cuttedNodeList){

        List<GlycanNode> selectCuttednodes = new ArrayList<>();

        if(preOrder.get(child) <= preOrder.get(fragmentRoot) ||  postOrder.get(child) >=  postOrder.get(fragmentRoot)){
            return false;
        }

        for(GlycanNode cuttedNode : cuttedNodeList){
            if( preOrder.get(child) >= preOrder.get(cuttedNode) &&  postOrder.get(child) <=  postOrder.get(cuttedNode)){
                selectCuttednodes.add(cuttedNode);
            }
        }

        return selectCuttednodes.isEmpty();

    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Glycan)) return false;
        if (!super.equals(o)) return false;

        Glycan glycan = (Glycan) o;

        return databaseIdentifier.equals(glycan.databaseIdentifier) &&
                reducingEndAnomericity.equals(glycan.reducingEndAnomericity);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + reducingEndAnomericity.hashCode();
        result = 31 * result + databaseIdentifier.hashCode();
        return result;
    }

    /**
     * A builder to build Glycans.  This builder is not reusable.
     */
    public static class Builder  extends AbstractBuilder<Glycan> {

        /**
         * Set the root of the Glycan. Use this method if the glycan has a free end.
         * Can only be called once.
         *
         * @param monosaccharide the monosaccharide that is to be the root
         * @param endAnomericity the anomericity of the root
         * @param databaseIdentifier the database identifier of the glycan
         * @return the monosaccharide
         */
        public Monosaccharide setRoot(Monosaccharide monosaccharide, Optional<Anomericity> endAnomericity, String databaseIdentifier) {
            Composition.Builder compositionBuilder = new Composition.Builder();
            return setRoot(monosaccharide,endAnomericity,databaseIdentifier,compositionBuilder.build());
        }


            /**
             * Set the root of the Glycan. Can only be called once.
             *
             * @param monosaccharide the monosaccharide that is to be the root
             * @param endAnomericity the anomericity of the root
             * @param databaseIdentifier the database identifier of the glycan
             * @param endComposition the compostion of the end of the glycan.
             * @return the monosaccharide
             */
        public Monosaccharide setRoot(Monosaccharide monosaccharide, Optional<Anomericity> endAnomericity, String databaseIdentifier, Composition endComposition) {

            if (buildSate != BuildSate.NEW) {
                throw new IllegalStateException("Root has already been set and cannot be set again");
            }

            checkNotNull(monosaccharide);
            checkNotNull(endAnomericity);
            checkNotNull(databaseIdentifier);

            buildSate = BuildSate.BUILDING;
            graph = new Glycan(monosaccharide, endAnomericity, databaseIdentifier, endComposition);

            return monosaccharide;
        }


        /**
         * Build the Glycan.
         *
         * @return the new Glycan
         */

        public Glycan build() {

            checkIsBuilding();

            Glycan tmp = graph;
            Counter counter = new Counter();
            tmp.populatePrePostOrder(tmp.root,counter);
            graph = null;

            buildSate = BuildSate.DONE;
            return tmp;
        }

    }
}
