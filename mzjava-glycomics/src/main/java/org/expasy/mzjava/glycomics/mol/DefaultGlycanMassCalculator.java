package org.expasy.mzjava.glycomics.mol;

import com.google.common.base.Preconditions;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.mol.PeriodicTable;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

import java.io.*;
import java.util.*;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */

@ServiceProvider(service = GlycanMassCalculator.class)
public class DefaultGlycanMassCalculator implements GlycanMassCalculator {

    private final Map<IonType, Composition> ionTypeCompositionMassMap;
    private static final String ERROR_MESSAGE = "Not valid IonType, for Glycosidic fragment only b,c,z,y Ion are allowed";

    private final Map<Monosaccharide, Map<CutIndexes,Composition>> clockwiseCrossRingCompMap = new LinkedHashMap<>();

    public DefaultGlycanMassCalculator() {

        ionTypeCompositionMassMap = new EnumMap<>(IonType.class);
        ionTypeCompositionMassMap.put(IonType.b, Composition.parseComposition("O-1H-3(-)"));
        ionTypeCompositionMassMap.put(IonType.c, Composition.parseComposition("H-1(-)"));
        ionTypeCompositionMassMap.put(IonType.y, Composition.parseComposition("H-1(-)"));
        ionTypeCompositionMassMap.put(IonType.z, Composition.parseComposition("O-1H-3(-)"));



        final String defaultMonosaccharideLookupFile = "monosaccharideData.json";
        String property = System.getProperty(DefaultMonosaccharideLookup.PROPERTY_MONOSACCHARIDE_LOOKUP, defaultMonosaccharideLookupFile);

        File lookupFile = new File(property);

        try (InputStream streamFile = lookupFile.exists() ? new FileInputStream(lookupFile) : this.getClass().getResourceAsStream(property);
             Reader in = new InputStreamReader(streamFile, "UTF-8")) {

            InternalCleavageJsonParser internalCleavageJsonParser = new InternalCleavageJsonParser();
            internalCleavageJsonParser.parse(in, clockwiseCrossRingCompMap);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("The encoding is not supported.", e);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException("Monosaccharide Lookup file not found. Please specify a existing file in System Property " + DefaultMonosaccharideLookup.PROPERTY_MONOSACCHARIDE_LOOKUP + ".  " +
                    "Actual value: " + property, e);
        } catch (IOException e) {
            throw new IllegalStateException("Monosaccharide Lookup file error closing stream. ", e);
        }
    }

    /**
     * Return the value of mass that the glycan have lost during the crossring fragmentation depending on the ion type.
     * It does not take into account the charge of the ion.
     * @param ionType the type of ion taken into account.
     * @param attachCarbon is the number of a carbon that is attached to the remaining part of the ring.
     * @param cleavedMonosaccaride the cleaved monosaccharide in the crossring fragment.
     * @param cutIndexes if is a x or a ion, this paramenter specify the positions of the cut in the monosaccharide ring.
     * @return the value of mass.
     */
    public final double getCrossRingLossMass(IonType ionType, int attachCarbon, Monosaccharide cleavedMonosaccaride, CutIndexes cutIndexes) {

        Preconditions.checkNotNull(ionType);
        Preconditions.checkNotNull(cleavedMonosaccaride);
        Preconditions.checkNotNull(cutIndexes);
        Preconditions.checkArgument(ionType.equals(IonType.a) || ionType.equals(IonType.x),  "Not valid IonType, for Crossring fragment only a,x Ion are allowed");

        CutDirection cutDirection = selectRingPart(attachCarbon,cleavedMonosaccaride,cutIndexes,ionType);

        if(cutDirection.equals(CutDirection.CLOCK_WISE)) {
            Map<CutIndexes, Composition> cleavagesMap = clockwiseCrossRingCompMap.get(cleavedMonosaccaride);
            Preconditions.checkArgument(cleavagesMap.containsKey(cutIndexes), "Can not retrieve the specified ring cleavage for the " + cleavedMonosaccaride.getName() + ". Is it possible to specify a new Monosaccharide Lookup file in System Property \"org.expasy.mzjava.glycomics.mol.monosaccharideLookup\". ");
            return cleavedMonosaccaride.getMolecularMass() - cleavagesMap.get(cutIndexes).getMolecularMass() + PeriodicTable.H_MASS - PeriodicTable.ELECTRON_MASS;
        }

        if(cutDirection.equals(CutDirection.ANTI_CLOCK_WISE)) {
            Map<CutIndexes, Composition> cleavagesMap = clockwiseCrossRingCompMap.get(cleavedMonosaccaride);
            Preconditions.checkArgument(cleavagesMap.containsKey(cutIndexes), "Can not retrieve the specified ring cleavage for the " + cleavedMonosaccaride.getName() + ". Is it possible to specify a new Monosaccharide Lookup file in System Property \"org.expasy.mzjava.glycomics.mol.monosaccharideLookup\". ");
            return cleavagesMap.get(cutIndexes).getMolecularMass() + PeriodicTable.H_MASS - PeriodicTable.ELECTRON_MASS;
        }
        throw new IllegalStateException("Cannot find the cutdirection");
    }

    private CutDirection selectRingPart(int carbonLinkedNumber, Monosaccharide cleavedMonosaccaride, CutIndexes cutIndexes, IonType ionType){

        Preconditions.checkNotNull(cutIndexes);
        CutDirection cutDirection;

        if(cleavedMonosaccaride.checkLinkedCarbon(cutIndexes,CutDirection.CLOCK_WISE,carbonLinkedNumber)){
            cutDirection = CutDirection.CLOCK_WISE;
        } else {
            cutDirection = CutDirection.ANTI_CLOCK_WISE;
        }

        if(IonType.a.equals(ionType)){
            return cutDirection.getComplement();
        }
        return cutDirection;
    }


    private int fixCarbonNumber(Monosaccharide monosaccharideCleaved, int linkedCarbon){

        Preconditions.checkArgument(monosaccharideCleaved.getRing() == RingType.furanose || monosaccharideCleaved.getRing() == RingType.pyranose,"The fragmentation allowed only pyranose or furanose ring.");

        if(monosaccharideCleaved.getRing() ==  RingType.pyranose && linkedCarbon > 5){
            return 5;
        }
        if(monosaccharideCleaved.getRing() ==  RingType.furanose && linkedCarbon > 4){
            return 4;
        }
        return linkedCarbon;
    }


    /**
     * Return the value of mass that the glycan have lost during the glycosidic fragmentation depending on the ion type.
     * It does not take into account the charge of the ion.
     * @param ionType the type of ion taken into account.
     * @return the value of mass.
     */
    public final double getGlycosidicDeltaMass(IonType ionType) {

        Preconditions.checkNotNull(ionType);
        Preconditions.checkArgument(ionType.equals(IonType.y) || ionType.equals(IonType.z) ||ionType.equals(IonType.b) || ionType.equals(IonType.c), ERROR_MESSAGE);
        Preconditions.checkArgument(ionType.getFragmentType() == FragmentType.FORWARD || ionType.getFragmentType() == FragmentType.REVERSE , ERROR_MESSAGE);


        return ionTypeCompositionMassMap.get(ionType).getMolecularMass();
    }

    /**
     * Return the value of mass that the glycan have lost during the glycosidic fragmentation depending on the ion type.
     * @param ionType the type of ion taken into account.
     * @return the value of mass.
     */
    public final Composition getGlycosidicDeltaComposition(IonType ionType) {

        Preconditions.checkNotNull(ionType);
        Preconditions.checkArgument(ionType.equals(IonType.y) || ionType.equals(IonType.z) ||ionType.equals(IonType.b) || ionType.equals(IonType.c), ERROR_MESSAGE);
        Preconditions.checkArgument(ionType.getFragmentType() == FragmentType.FORWARD || ionType.getFragmentType() == FragmentType.REVERSE , ERROR_MESSAGE);
        return ionTypeCompositionMassMap.get(ionType);
    }

    /**
     * Return the value of the mass over charge. Specify FragmentType.INTACT if you are dealing with precursor.
     * Before call this method be sure that the molecularMass it is correct.
     * @param molecularMass the molecular mass.
     * @param charge the charge of which calculate.
     * @param fragmentType the fragment type of interest.
     * @return the value of mass over charge.
     */
    public final double calculateMz(double molecularMass, int charge, FragmentType fragmentType) {

        Preconditions.checkNotNull(fragmentType);
        Preconditions.checkArgument(charge > 0, "Charge must be greater than 0");
        int chargeCount;
        if( FragmentType.INTACT.equals(fragmentType)){
            chargeCount = charge;
        } else {
            chargeCount = charge - 1;
        }
        return (molecularMass - (chargeCount * (PeriodicTable.H_MASS - PeriodicTable.ELECTRON_MASS))) /charge;
    }

    /**
     * Return all possible type of internal cleavage for the monosaccharide of interest
     * Before call this method be sure that the molecularMass it is correct.
     * @param monosaccharide the monosaccharide of interest.
     * @return an array
     */
     public final CutIndexes[] getAllInternalTypeCleavage(Monosaccharide monosaccharide){

        Preconditions.checkNotNull(monosaccharide);

        Set<CutIndexes> keyset = clockwiseCrossRingCompMap.get(monosaccharide).keySet();

        CutIndexes[] cleavages = new CutIndexes[keyset.size()];
        int i = 0;
        for(CutIndexes cleavage : keyset){
            cleavages[i]  = cleavage;
            i++;
        }
        return cleavages;
    }




    /**
     * @author Davide Alocci
     * @version sqrt -1.
     * Parser for the Internal Cleavage in the X and A IonType. It builds the maps for the a and x Ion.
     */
    static class InternalCleavageJsonParser {

        /**
         * Parser for monosaccharide data.
         * @param in Input reader where the parser will take the data.
         * @param compMap a map with all the information from the file.
         * The key of this map is a Monosaccharide, instead the value is an other map of cleavage position and the cleavage compostion.
         */
        public void parse(Reader in, Map<Monosaccharide, Map<CutIndexes,Composition>> compMap) {

            ObjectMapper mapper = new ObjectMapper();
            final JsonNode monosaccharidesArray;

            try {
                monosaccharidesArray = mapper.readTree(in);
            } catch (IOException e) {
                throw new IllegalStateException("Monosaccharide Lookup file error", e);
            }

            String mName = "name";
            String mDeltaMass = "deltaMass";

            MonosaccharideLookup lookup = Lookup.getDefault().lookup(MonosaccharideLookup.class);

            for (final JsonNode monosaccharideNode : monosaccharidesArray) {
                JsonNode monosaccharideName = monosaccharideNode.path(mName);
                checkJsonNode(monosaccharideName, mName);
                JsonNode monosaccharideDeltaMass = monosaccharideNode.path(mDeltaMass);
                checkJsonNode(monosaccharideDeltaMass, mDeltaMass);

                Map<CutIndexes,Composition> clockWiseCleavedCompMap = new HashMap<>();

                for (final JsonNode ringFragment : monosaccharideDeltaMass) {

                    String mComposition = "composition";
                    String mCleavage= "cleavage";
                    JsonNode monosaccharideComp = ringFragment.path(mComposition);
                    checkJsonNode(monosaccharideComp, mComposition);
                    clockWiseCleavedCompMap.put(parseCleavage(ringFragment.path(mCleavage)), Composition.parseComposition(monosaccharideComp.getTextValue()));

                }
                compMap.put(lookup.getNew(monosaccharideName.getTextValue()), clockWiseCleavedCompMap);
            }
        }



        private CutIndexes parseCleavage(JsonNode monosaccharideCleavage){

            Preconditions.checkNotNull(monosaccharideCleavage);
            checkJsonNode(monosaccharideCleavage, "cleavage");

            if(monosaccharideCleavage.size() != 2)
                throw new IllegalStateException("The cleavage site must have two integer: first and last carbon of the fragment.");

            return new CutIndexes(monosaccharideCleavage.get(0).getValueAsInt(),monosaccharideCleavage.get(1).getValueAsInt());
        }

        private void checkJsonNode(JsonNode node, String nodePath){
            Preconditions.checkNotNull(node);
            Preconditions.checkNotNull(nodePath);
            Preconditions.checkArgument(!"".equals(nodePath),"Node path must not be empty");
            if (node.isMissingNode())
                throw new IllegalStateException("Mapping not possible. Each record in the Monosaccharide Lookup file need to have "+nodePath+".");
            if ("".equals(node.getTextValue()))
                throw new IllegalStateException("Monosaccharide without "+nodePath+" are not allowed in the Monosaccharide Lookup file.");
        }
    }
}