package org.expasy.mzjava.glycomics.ms.spectrum;

import com.google.common.base.Preconditions;
import org.expasy.mzjava.core.mol.*;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.glycomics.mol.GlycanFragment;
import org.expasy.mzjava.glycomics.mol.GlycanMassCalculator;
import org.openide.util.Lookup;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class GlycanFragAnnotation implements PeakAnnotation {

    public static final Composition EMPTY_COMPOSITION = new Composition();

    private final Composition isotopeComposition;
    private final int charge;
    private final GlycanFragment fragment;
    private final Mass neutralLoss;



    /**
     * Construct a GlycanFragAnnotation
     *
     * @param charge the charge of the fragment.
     * @param fragment the glycan fragment to be annotated.
     */
    public GlycanFragAnnotation(int charge, GlycanFragment fragment) {

        this(charge, fragment, EMPTY_COMPOSITION, Mass.ZERO);
    }

    public GlycanFragAnnotation(int charge, GlycanFragment fragment, Composition isotopeComposition, Mass neutralLoss) {


        Preconditions.checkNotNull(fragment);
        Preconditions.checkNotNull(neutralLoss);
        Preconditions.checkNotNull(charge);

        this.isotopeComposition = isotopeComposition;
        this.neutralLoss = neutralLoss;
        this.charge = charge;
        this.fragment = fragment;

    }

    /**
     * Copy constructor
     *
     * @param src the GlycanFragAnnotation to copy
     */
    public GlycanFragAnnotation(GlycanFragAnnotation src) {

        Preconditions.checkNotNull(src);
        isotopeComposition = src.isotopeComposition;
        charge = src.charge;
        fragment = src.fragment;
        neutralLoss = src.neutralLoss;
    }

    /**
     * Returns true if this annotation annotates a peak with a neutral loss, false otherwise
     *
     * @return true if this annotation has a neutral loss, false otherwise
     */
    public boolean hasNeutralLoss() {

        return neutralLoss.getMolecularMass() != 0;
    }

    /**
     * Return the mass shift that is associated with this annotation
     *
     * @return the mass shift that is associated with this annotation
     */
    public Mass getNeutralLoss() {

        return neutralLoss;
    }

    /**
     * Return the glycan fragment associated with this annotation
     *
     * @return the glycan fragment associated with this annotation
     */
    public GlycanFragment getFragment() {

        return fragment;
    }

    /**
     * Return the number of isotopes
     *
     * @return the number of isotopes
     */
    public int getIsotopeCount() {

        return isotopeComposition.size();
    }

    /**
     * Return the Composition that contains the isotopes.
     *
     * @return the Composition that contains the isotopes
     */
    public Composition getIsotopeComposition() {

        return isotopeComposition;
    }

    /**
     * Return the charge for this annotation.
     * @return the charge for this annotation.
     */
    public int getCharge() {

        return charge;
    }


    /**
     * Returns the theoretical m/z for this annotation.
     *
     * @return the theoretical m/z for this annotation.
     */
    public double calcTheoreticalMz() {

        double isotopeDelta;
        GlycanMassCalculator glycanMassCalculator = Lookup.getDefault().lookup(GlycanMassCalculator.class);

        if(isotopeComposition.isEmpty()) {
            isotopeDelta = 0;
        } else {
            isotopeDelta = MassCalculator.calcIsotopeDelta(isotopeComposition);
        }

        return glycanMassCalculator.calculateMz(fragment.calculateMolecularMass(), charge, fragment.getFragmentType()) + (isotopeDelta + neutralLoss.getMolecularMass())/charge;

    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GlycanFragAnnotation)) return false;

        GlycanFragAnnotation that = (GlycanFragAnnotation) o;

        if (charge != that.charge) return false;
        if (!fragment.equals(that.fragment)) return false;
        if (!isotopeComposition.equals(that.isotopeComposition)) return false;
        return neutralLoss.equals(that.neutralLoss);
    }

    @Override
    public int hashCode() {
        int result = isotopeComposition.hashCode();
        result = 31 * result + charge;
        result = 31 * result + fragment.hashCode();
        result = 31 * result + neutralLoss.hashCode();
        return result;
    }

    public final GlycanFragAnnotation copy() {
        return new GlycanFragAnnotation(this);
    }




    /**
     * GlycanFragAnnotation Builder
     */
    public static class FragBuilder {

        static final PeriodicTable periodicTable = PeriodicTable.getInstance();

        private final int charge;
        private GlycanFragment fragment;
        private Mass neutralLoss = Mass.ZERO;
        private final Composition.Builder compositionBuilder = new Composition.Builder();


        /**
         * Construct a new FragBuilder
         *
         * @param charge the charge
         * @param fragment the glycan fragment
         */

        public FragBuilder(int charge, GlycanFragment fragment) {
            Preconditions.checkNotNull(fragment);
            this.charge = charge;
            this.fragment = fragment;
        }





        /**
         * Copy constructor
         *
         * @param src the GlycanFragAnnotation from which to copy the values
         */
        public FragBuilder(GlycanFragAnnotation src) {

            this.charge = src.getCharge();
            this.fragment = src.getFragment();
        }

        /**
         * Set the neutral loss
         *
         * @param neutralLoss the neutral loss
         * @return  this builder
         */
        public final FragBuilder setNeutralLoss(Mass neutralLoss) {

            this.neutralLoss = neutralLoss;
            return this;
        }
        /**
         * Add <code>count</code> C13 isotopes
         *
         * @param count the number of C13 isotopes
         * @return this builder
         */
        public final FragBuilder addC13(int count) {

            compositionBuilder.add(periodicTable.getAtom(AtomicSymbol.C, 13), count);
            return this;
        }

        /**
         * Add <code>count</code> H2 isotpes
         *
         * @param count the number of H2 to add
         * @return this builder
         */
        public final FragBuilder addH2(int count) {

            compositionBuilder.add(periodicTable.getAtom(AtomicSymbol.H, 2), count);
            return this;
        }

        /**
         * Add the <code>isotope</code>
         *
         * @param isotope the isotope to add
         * @return this builder
         */
        public final FragBuilder addIsotope(Atom isotope) {

            Preconditions.checkArgument(!isotope.isDefaultIsotope());

            compositionBuilder.add(isotope, 1);
            return this;
        }

        /**
         * Add all the atoms from <code>isotopeComposition</code>
         *
         * @param isotopeComposition the atoms to add
         * @return this builder
         */
        public final FragBuilder addIsotopeComposition(Composition isotopeComposition) {

            compositionBuilder.addAll(isotopeComposition);
            return this;
        }

        /**
         * Build the CrossRingFragAnnotation annotation.
         *
         * @return the new CrossRingFragAnnotation
         */
        public final GlycanFragAnnotation build(){

            Composition fragIsotopeComposition = compositionBuilder.isEmpty() ? GlycanFragAnnotation.EMPTY_COMPOSITION : compositionBuilder.build();
            return new GlycanFragAnnotation(charge, fragment, fragIsotopeComposition, neutralLoss);
        }

    }

}