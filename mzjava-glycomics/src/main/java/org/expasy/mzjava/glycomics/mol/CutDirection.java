package org.expasy.mzjava.glycomics.mol;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */

/**
 Because the CutIndexes have the first index always lower than the second, the cutDirection will select the part
 between the two indexes if it is CLOCK_WISE otherwise it will select the other part of the whole.

 */

public enum CutDirection {

    CLOCK_WISE,
    ANTI_CLOCK_WISE;

    public CutDirection getComplement() {

        switch (this) {

            case CLOCK_WISE:
                return ANTI_CLOCK_WISE;
            case ANTI_CLOCK_WISE:
                return CLOCK_WISE;
            default:
                throw new IllegalStateException("Cannot get complement of " + this);
        }
    }
}
