package org.expasy.mzjava.glycomics.mol;

import com.google.common.base.Preconditions;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.expasy.mzjava.core.mol.Composition;
import org.openide.util.lookup.ServiceProvider;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Davide Alocci
 * @version sqrt -1
 **/

@ServiceProvider(service = SubstituentLookup.class)
public class DefaultSubstituentLookup implements SubstituentLookup{

    public static final String PROPERTY_SUBSTITUENT_LOOKUP =  "org.expasy.mzjava.glycomics.mol.substituentLookup";
    private final Map<String, Substituent> substituentLookupMap = new LinkedHashMap<>();


    public DefaultSubstituentLookup() {

        final String defaultSubstituentLookupFile = "substituentData.json";

        String property = System.getProperty(PROPERTY_SUBSTITUENT_LOOKUP, defaultSubstituentLookupFile);
        File lookupFile = new File(property);

        try (InputStream streamFile = lookupFile.exists() ? new FileInputStream(lookupFile) : this.getClass().getResourceAsStream(property);
             Reader in = new InputStreamReader(streamFile, "UTF-8")) {

            parse(in, substituentLookupMap);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("The encoding is not supported.",e);
        } catch (FileNotFoundException e){
            throw new IllegalStateException("Substituent Lookup file not found. Please specify a existing file in System Property " + PROPERTY_SUBSTITUENT_LOOKUP + ".  " +
                    "Actual value: " + property, e);
        } catch (IOException e) {
            throw new IllegalStateException("Substituent Lookup file error closing stream. ",e);
        }
    }

    /**
     * Search for a substituent with the input name the list of known substituents.
     * <p>The method throw an IllegalStateException if there is no substituent with the input name in the list.</p>
     * @param name the name of the substituent
     * @return A substituent object with the specified name.
     */
    public final Substituent getNew(String name) {

        Preconditions.checkNotNull(name);
        if(substituentLookupMap.get(name) == null){
            throw new IllegalArgumentException("This substituent is not in the list. If you want to add it, you can specify a new Substituent Lookup file in System Property " + PROPERTY_SUBSTITUENT_LOOKUP + ".  " +
                    "Actual value: " + System.getProperty(PROPERTY_SUBSTITUENT_LOOKUP, "substituentData.json"));
        }
        return new Substituent(substituentLookupMap.get(name));
    }

    /**
     * Return a list of the substituents specified in Substituent Lookup file.
     * @return A list of the known substituents
     */
    public final List<Substituent> values() {
        return new ArrayList<Substituent>(substituentLookupMap.values());
    }



    /**
     * Parser for the Substituent Lookup file
     * @return A map that has the name as Key and the Substituent as Value
     */
    protected Map<String, Substituent> parse(Reader in, Map<String, Substituent> substituentLookupMap) {

        final JsonNode substituentsArray;
        Preconditions.checkNotNull(substituentLookupMap);
        Preconditions.checkNotNull(in);

        ObjectMapper mapper = new ObjectMapper();


        try {
            substituentsArray = mapper.readTree(in);
        } catch (IOException e) {

            throw new IllegalStateException("Substituent Lookup file error", e);
        }

        for (final JsonNode substituentNode : substituentsArray) {

            String mName = "name";
            String mComposition = "composition";

            JsonNode substituentName = substituentNode.path(mName);
            JsonNode substituentComp = substituentNode.path(mComposition);

            checkJsonNode(substituentName, mName);
            checkJsonNode(substituentComp, mComposition);

            Substituent substituent = new Substituent(substituentName.getTextValue(), Composition.parseComposition(substituentComp.getTextValue()));
            substituentLookupMap.put(substituent.getName(), substituent);
        }
        return substituentLookupMap;
    }

    private void checkJsonNode(JsonNode node, String nodePath){

        if (node.isMissingNode())
            throw new IllegalStateException("Mapping not possible. Each record in the Substituent Lookup file need to have "+nodePath+".");
        if ("".equals(node.getTextValue()))
            throw new IllegalStateException("Substituent without "+nodePath+" are not allowed in the Substituent Lookup file.");
    }

}
