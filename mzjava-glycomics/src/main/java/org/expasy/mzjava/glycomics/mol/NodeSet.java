/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.glycomics.mol;

import com.google.common.base.Preconditions;

import java.util.*;

/**
 * This class implements the <tt>Set</tt> interface with a IdentityHashMap, using
 * reference-equality in place of object-equality when comparing entries.
 * In other words, in an <tt>NodeSet</tt>, two entries
 * <tt>k1</tt> and <tt>k2</tt> are considered equal if and only if
 * <tt>(k1==k2)</tt>.  (In normal <tt>Set</tt> implementations (like
 * <tt>HashSet</tt>) two entries <tt>k1</tt> and <tt>k2</tt> are considered equal
 * if and only if <tt>(k1==null ? k2==null : k1.equals(k2))</tt>.)
 *
 * <p><b>This class is <i>not</i> a general-purpose <tt>Set</tt>
 * implementation!  While this class implements the <tt>Set</tt> interface, it
 * intentionally violates <tt>Set's</tt> general contract, which mandates the
 * use of the <tt>equals</tt> method when comparing objects.</b>
 *
 * <p>  This class is designed for use by SaccharideGraph so that multiple
 * instances of the 'same' (i.e. two Glc) Monosaccharide can be used as nodes.
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class NodeSet<E> extends AbstractSet<E> implements Set<E> {

    private final Map<E, Boolean> m;  // The backing map
    private final Set<E> s;       // Its keySet

    /**
     * Constructs a new, empty set with the specified expected maximum size.
     * Putting more than the expected number of values into
     * the set may cause the internal data structure to grow, which may be
     * somewhat time-consuming.
     *
     * @param expectedMaxSize the expected maximum size of the set
     */
    public NodeSet(int expectedMaxSize) {

        m = new IdentityHashMap<E, Boolean>(expectedMaxSize);
        s = m.keySet();
    }

    public NodeSet(Collection<E> contents) {

        m = new IdentityHashMap<E, Boolean>(contents.size());
        s = m.keySet();

        addAll(contents);
    }

    @Override
    public void clear() {

        m.clear();
    }

    @Override
    public int size() {

        return m.size();
    }

    @Override
    public boolean isEmpty() {

        return m.isEmpty();
    }

    @Override
    public boolean contains(Object o) {

        return m.containsKey(o);
    }

    @Override
    public boolean remove(Object o) {

        return m.remove(o) != null;
    }

    @Override
    public boolean add(E e) {

        return m.put(e, Boolean.TRUE) == null;
    }

    @Override
    public Iterator<E> iterator() {

        return s.iterator();
    }

    @Override
    public Object[] toArray() {

        return s.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {

        return s.toArray(a);
    }

    @Override
    public String toString() {

        return s.toString();
    }

    @Override
    public int hashCode() {

        return s.hashCode();
    }

    @Override
    public boolean equals(Object o) {

        return o == this || s.equals(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {

        return s.containsAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {

        return s.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {

        return s.retainAll(c);
    }
    // addAll is the only inherited implementation

    /**
     * Create a new NodeSet that contains the <code>nodes</code>
     *
     * @param nodes the nodes to add to the node set
     * @param <N> the type of the node
     * @return a new NodeSet containing <code>nodes</code>
     */
    public static <N> NodeSet<N> of(N... nodes) {

        Preconditions.checkNotNull(nodes);

        NodeSet<N> nodeSet = new NodeSet<N>(nodes.length);
        Collections.addAll(nodeSet, nodes);
        return nodeSet;
    }
}


