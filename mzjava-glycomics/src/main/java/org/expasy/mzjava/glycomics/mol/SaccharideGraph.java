/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.glycomics.mol;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.custom_hash.TObjectIntCustomHashMap;
import gnu.trove.strategy.IdentityHashingStrategy;
import org.expasy.mzjava.utils.Counter;

import java.util.*;

import static com.google.common.base.Preconditions.*;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class SaccharideGraph {

    public enum Traversal {DFS, BFS}

    protected final Monosaccharide root;
    protected final List<GlycanNode> nodes;
    protected final List<SEdge> edges;
    protected final IdentityEdgeMultimap<Monosaccharide, Monosaccharide, GlycosidicEdge> monosaccharideEdgeMultimap;
    protected final IdentityEdgeMultimap<Monosaccharide, Substituent, SubstituentEdge> substituentEdgeMultimap;
    private final IdentityHashingStrategy<GlycanNode> identityHashingStrategy = new IdentityHashingStrategy<>();
    protected final TObjectIntMap<GlycanNode> preOrder = new TObjectIntCustomHashMap<>(identityHashingStrategy);
    protected final TObjectIntMap<GlycanNode> postOrder = new TObjectIntCustomHashMap<>(identityHashingStrategy);
    private boolean hasHyperEdges = false;
    private boolean populatePrePostOrder = false;

    protected SaccharideGraph(Monosaccharide root) {

        checkNotNull(root);

        nodes = new ArrayList<>();
        edges = new ArrayList<>();
        monosaccharideEdgeMultimap = new IdentityEdgeMultimap<>(2);
        substituentEdgeMultimap = new IdentityEdgeMultimap<>(2);

        this.root = root;
        nodes.add(this.root);
    }

    SaccharideGraph(Monosaccharide root, List<GlycanNode> nodes, List<SEdge> edges, IdentityEdgeMultimap<Monosaccharide, Monosaccharide, GlycosidicEdge> monosaccharideEdgeMultimap, IdentityEdgeMultimap<Monosaccharide, Substituent, SubstituentEdge> substituentEdgeMultimap) {

        this.root = root;
        this.nodes = nodes;
        this.edges = edges;
        this.monosaccharideEdgeMultimap = monosaccharideEdgeMultimap;
        this.substituentEdgeMultimap = substituentEdgeMultimap;
    }

    protected int add(Monosaccharide child, List<Monosaccharide> parents, GlycosidicLinkage linkage) {

        checkNotNull(child);
        checkNotNull(linkage);
        checkArgument(!parents.isEmpty());

        if (parents.size() > 1)
            hasHyperEdges = true;

        for (Monosaccharide parent : parents) {

            checkNotNull(parent);
        }

        GlycosidicEdge edge = new GlycosidicEdge(parents, child, linkage);

        int index = appendNode(child);
        edges.add(edge);

        for (Monosaccharide parent : parents) {

            monosaccharideEdgeMultimap.put(parent, child, edge);
        }

        return index;
    }

    protected int add(Monosaccharide child, Monosaccharide parent, GlycosidicLinkage linkage) {

        checkNotNull(child);
        checkNotNull(parent);
        checkNotNull(linkage);

        GlycosidicEdge edge = new GlycosidicEdge(parent, child, linkage);

        int index = appendNode(child);
        edges.add(edge);

        monosaccharideEdgeMultimap.put(parent, child, edge);

        return index;
    }

    protected int add(Substituent child, List<Monosaccharide> parents, SubstituentLinkage linkage) {

        checkNotNull(child);
        checkNotNull(linkage);
        checkArgument(!parents.isEmpty());

        if (parents.size() > 1)
            hasHyperEdges = true;

        for (Monosaccharide parent : parents) {

            checkNotNull(parent);
        }

        SubstituentEdge edge = new SubstituentEdge(parents, child, linkage);

        int index = appendNode(child);
        edges.add(edge);

        for (Monosaccharide parent : parents) {

            substituentEdgeMultimap.put(parent, child, edge);
        }

        return index;
    }

    protected int add(Substituent child, Monosaccharide parent, SubstituentLinkage linkage) {

        checkNotNull(child);
        checkNotNull(parent);
        checkNotNull(linkage);

        SubstituentEdge edge = new SubstituentEdge(parent, child, linkage);

        int index = appendNode(child);
        edges.add(edge);

        substituentEdgeMultimap.put(parent, child, edge);

        return index;
    }

    private int appendNode(GlycanNode node) {

        nodes.add(node);
        return nodes.size() - 1;
    }

    /**
     * Returns the <code>nodeId</code>'th node that was added to this SaccharideGraph.
     *
     * @param nodeId the node id
     * @return the <code>nodeId</code>'th node that was added to this SaccharideGraph.
     */
    public GlycanNode getNode(int nodeId) {

        checkPositionIndex(nodeId, nodes.size());

        return nodes.get(nodeId);
    }

    /**
     * Return the root of this SaccharideGraph
     *
     * @return the root of this SaccharideGraph
     */
    public Monosaccharide getRoot() {

        return root;
    }

    /**
     * Return the linkage between <code>parentNode</code> and <code>childNode</code>
     *
     * @param parentNode the parent node
     * @param childNode  the childNode
     * @return an Optional containing the linkage if one exists between <code>parentNode</code> and <code>childNode</code>,
     * or an empty Optional if no linkage exists.
     */
    public Optional<GlycosidicLinkage> getGlycoLinkage(GlycanNode parentNode, GlycanNode childNode) {

        GlycosidicEdge edge = monosaccharideEdgeMultimap.get(parentNode, childNode);

        return edge != null ? Optional.of(edge.getLink()) : Optional.<GlycosidicLinkage>absent();
    }

    /**
     * Return the linkage between <code>parentNode</code> and <code>childNode</code>
     *
     * @param parentNode the parent node
     * @param childNode  the childNode
     * @return an Optional containing the linkage if one exists between <code>parentNode</code> and <code>childNode</code>,
     * or an empty Optional if no linkage exists.
     */
    public Optional<SubstituentLinkage> getSubstituentLinkage(GlycanNode parentNode, GlycanNode childNode) {

        SubstituentEdge edge = substituentEdgeMultimap.get(parentNode, childNode);

        return edge != null ? Optional.of(edge.getLink()) : Optional.<SubstituentLinkage>absent();
    }



    /**
     * Returns a the children of parent
     *
     * @param parent the parent node
     * @return a sorted set of all the children of parent
     */
    public NodeSet<GlycanNode> getChildren(Monosaccharide parent) {

        Collection<Monosaccharide> saccarideChildren = monosaccharideEdgeMultimap.getChildren(parent);
        Collection<Substituent> substituentChildren = substituentEdgeMultimap.getChildren(parent);

        NodeSet<GlycanNode> nodeSet = new NodeSet<>(saccarideChildren.size() + substituentChildren.size());
        nodeSet.addAll(saccarideChildren);
        nodeSet.addAll(substituentChildren);
        return nodeSet;

    }

    /**
     * Returns all children of parent that are Monosaccharides
     *
     * @param parent the parent node
     * @return all Monosaccharide children of parent
     */
    public NodeSet<Monosaccharide> getMonosaccharideChildren(Monosaccharide parent) {

        return new NodeSet<>(monosaccharideEdgeMultimap.getChildren(parent));
    }

    /**
     * Returns all children of parent that are Substituents
     *
     * @param parent the parent node
     * @return all children of parent that are Substituents
     */
    public NodeSet<Substituent> getSubstituentChildren(Monosaccharide parent) {

        return new NodeSet<>(substituentEdgeMultimap.getChildren(parent));
    }

    /**
     * Return the number of monosaccharides and substituents in this glycan
     *
     * @return the number of monosaccharides and substituents in this glycan
     */
    public int order() {

        return nodes.size();
    }

    /**
     * Returns a new list of all the edges.
     *
     * @return a new list of all the edges
     */
    public List<SEdge> copyEdgeList() {

        return new ArrayList<>(edges);
    }

    /**
     * Returns <code>true</code> if the glycan has hyperedges.
     * @return <code>true</code> if the glycan has hyperedges;
     *         <code>false</code> otherwise
     */
    public boolean hasHyperEdges() {

        return hasHyperEdges;
    }

    /**
     * Returns <code>true</code> if the specified monosaccharide is a leaf in the saccharide.
     * @return <code>true</code> if the specified monosaccharide is a leaf in the saccharide;
     *         <code>false</code> otherwise
     */
    public boolean isLeaf(Monosaccharide monosaccharide){

        Preconditions.checkNotNull(monosaccharide);
        if(!containsNode(monosaccharide)){
            throw new IllegalArgumentException("The specified monosaccharide doesn't belong to this saccahride.");
        }

        return this.getMonosaccharideChildren(monosaccharide).isEmpty();
    }


    /**
     * Calculate the parent of a node. It use pre and post order so could be time consuming.
     * In case that the node is a root it return the root it self.
     * @param child a child in the graph.
     * @return the parent of the child specified.
     */
    public <A extends GlycanNode> GlycanNode getParent(A child){
        Preconditions.checkNotNull(child);
        if (!populatePrePostOrder){
            Counter counter = new Counter();
            populatePrePostOrder(root,counter);
        }
        GlycanNode parent = root;
        for(GlycanNode node : nodes){
            if(preOrder.get(node)< preOrder.get(child) && postOrder.get(node) > postOrder.get(child) && preOrder.get(parent)< preOrder.get(node)){
                parent = node;
            }
        }
        return parent;
    }

    /**
     * Returns the total mass of the Saccharide Graph.
     *
     * @return the total mass of the Saccharide Graph.
     */
    protected double calculateTotalSaccharideMass() {

        double mass = 0.0;
        for (GlycanNode node : nodes) {
            mass += node.getMolecularMass();
        }

        for (SEdge edge : edges) {
            Linkage link = edge.getLink();
            mass += link.getMolecularMass();
        }
        return mass;
    }

    /**
     * This method check if a glycan contains a specific node.
     * It check for memory reference of the node.
     * Don't use it for check if a glycan contains a type of monosaccahride.
     * @param node a monosaccharide
     * @return <code>true</code> if the monosaccharide is in the list of nodes.
     *         <code>false</code> otherwise
     */
    protected boolean containsNode(GlycanNode node){
        Preconditions.checkNotNull(node);
        for(GlycanNode n : nodes){
            if(n == node){
                return true;
            }
        }
        return false;
    }

    /**
     * Return a boolean value that show if the glycan is fragmentable.
     * @return true if the glycan is fragmentable otherwise return false.
     */
    public boolean canFragmentCrossRing(Monosaccharide monosaccharide) {

        if (hasHyperEdges) {
            return false;
        }

        for (SEdge edge : edges) {
            if(edge.getChild() == monosaccharide  && !edge.getLink().hasSetCarbon() ){
                return false;
            }

            if(edge.getParent() == monosaccharide && !edge.getLink().hasSetCarbon()){
                return false;
            }
        }
        return true;
    }

    /**
     * Check if there is no nodes in the glycan.
     * @return boolean value
     */
    public boolean isEmpty() {

        return nodes.isEmpty();
    }


    /**
     * Return the size of the glycan as the number of monosaccharide.
     * @return size of the fragment.
     */
    public int size() {

        return nodes.size();
    }

    /**
     * Populate the pre and post order table
     * @param node node of the saccharide graph.
     * @param counter node counter.
     */
    protected void populatePrePostOrder(GlycanNode node, Counter counter) {

        populatePrePostOrder = true;
        preOrder.put(node, counter.getCount());
        counter.increment();

        for (GlycanNode child : monosaccharideEdgeMultimap.getChildren(node)) {

            populatePrePostOrder(child, counter);
        }
        for (GlycanNode child : substituentEdgeMultimap.getChildren(node)) {

            populatePrePostOrder(child, counter);
        }

        postOrder.put(node, counter.getCount());
        counter.increment();
    }

 /*   public final List<CutDescriptor> generateCutDescriptor(int glycosidicCut, int crossringCut){
        return generateCutDescriptor(glycosidicCut, crossringCut, new AllCrossRingCutType());
    }


    public final List<CutDescriptor> generateCutDescriptor(int glycosidicCut, int crossringCut, CrossRingCutType crossRingCutType){
        List<CutDescriptor> cutDescriptorList = new ArrayList<>();
        Set<Set<SEdge>> edgePowerSet = this.powerSet(generateEdgeList(true,false));
        Set<Set<GlycanNode>> nodePowerSet = this.powerSet(generateNodeList(true,false));

        for(Set<SEdge> edgeSet : edgePowerSet){
            for(Set<GlycanNode> nodeSet : nodePowerSet){
                if(nodeSet.size() <= crossringCut && edgeSet.size() <= glycosidicCut){
                    List<Set<CleavedMonosaccharide>> cutIndexesSetList = generateCleavedMonosaccharideSets(nodeSet,crossRingCutType);
                    if(cutIndexesSetList.isEmpty()){
                        CutDescriptor cutDescriptor = new CutDescriptor();
                        cutDescriptor.addCleavedEdges(edgeSet);
                        cutDescriptorList.add(cutDescriptor);
                    } else {
                        for (Set<CleavedMonosaccharide> cleavedMonosaccharideSet : cutIndexesSetList) {
                            CutDescriptor cutDescriptor = new CutDescriptor(cleavedMonosaccharideSet,edgeSet);
                            cutDescriptorList.add(cutDescriptor);
                        }
                    }
                }
            }
        }
        return cutDescriptorList;
    }



    private <T> Set<Set<T>> powerSet(Set<T> originalSet) {
        Preconditions.checkNotNull(originalSet);
        Set<Set<T>> sets = new HashSet<>();
        if (originalSet.isEmpty()) {
            sets.add(new HashSet<T>());
            return sets;
        }

        List<T> list = new ArrayList<>(originalSet);
        T head = list.get(0);
        Set<T> rest = Sets.newIdentityHashSet();
        rest.addAll(list.subList(1, list.size()));
        for (Set<T> set : powerSet(rest)) {
            Set<T> newSet = Sets.newIdentityHashSet();
            newSet.add(head);
            newSet.addAll(set);
            sets.add(newSet);
            sets.add(set);
        }
        return sets;
    }


    private List<Set<CleavedMonosaccharide>> generateCleavedMonosaccharideSets(Set<GlycanNode> nodeSet, CrossRingCutType crossRingCutType){

        Map<Monosaccharide, List<CutIndexes>> initialMap = new IdentityHashMap<>();
        List<Integer> radixList = new ArrayList<>();
        for(GlycanNode node : nodeSet) {
            Monosaccharide monosaccharide;

            if (node instanceof Substituent) {
                throw new IllegalStateException("No CutDescriptor for Substituent");
            } else {
                monosaccharide = (Monosaccharide) node;
            }

            List<CutIndexes> allInternalTypeCleavege= new ArrayList<>(crossRingCutType.get(monosaccharide));
            radixList.add(allInternalTypeCleavege.size());
            initialMap.put(monosaccharide,allInternalTypeCleavege);
        }

        List<Set<CleavedMonosaccharide>> results = new ArrayList<>();
        cutIndexCombinationGenerator(initialMap, Ints.toArray(radixList), results);

        return results;
    }


    private void cutIndexCombinationGenerator(Map<Monosaccharide, List<CutIndexes>> monosaccharideMap, int[] radixArray, List<Set<CleavedMonosaccharide>> results){

        MixedRadixNtupleGenerator.NtupleContainer ntupleContainer = new MixedRadixNtupleGenerator.NtupleContainer();
        MixedRadixNtupleGenerator combinationGenerator = new MixedRadixNtupleGenerator(ntupleContainer);
        combinationGenerator.generate(radixArray);

        for(int[]comb : ntupleContainer.getNtuples()){
            if(comb.length != monosaccharideMap.keySet().size()){
                throw new IllegalStateException("Error cutIndexCombinationGenerator: the lenght of the monosaccharide map must be the same of the generated combination.");
            }
            Set<CleavedMonosaccharide> cleavedMonosaccharideSet = new HashSet<>();
            for( int i = 0; i <comb.length; i++){
                Monosaccharide actualMonosaccharide = (Monosaccharide) monosaccharideMap.keySet().toArray()[i];
                cleavedMonosaccharideSet.add(new CleavedMonosaccharide(actualMonosaccharide,monosaccharideMap.get(actualMonosaccharide).get(comb[i])));
            }
            results.add(cleavedMonosaccharideSet);
        }
    }



    private Set<GlycanNode> generateNodeList (boolean includeMonosaccharide, boolean includeSubstituent){

        Set<GlycanNode> glycanNodes = Sets.newSetFromMap(new IdentityHashMap<GlycanNode, Boolean>());
        if(includeMonosaccharide && includeSubstituent){
            glycanNodes.addAll(nodes);
            return glycanNodes;
        }

        for(GlycanNode glycanNode : nodes){
            if(glycanNode instanceof Monosaccharide && includeMonosaccharide){
                glycanNodes.add(glycanNode);
            }
            if(glycanNode instanceof Substituent && includeSubstituent){
                glycanNodes.add(glycanNode);
            }
        }
        return glycanNodes;
    }

    private Set<SEdge> generateEdgeList (boolean includeGlycosidicEdge, boolean includeSubstituentEdge){

        Set<SEdge> glycanEdges = Sets.newSetFromMap(new IdentityHashMap<SEdge, Boolean>());
        if(includeGlycosidicEdge && includeSubstituentEdge){
            glycanEdges.addAll(edges);
            return glycanEdges;
        }

        for(SEdge edge : edges){
            if(edge instanceof GlycosidicEdge && includeGlycosidicEdge){
                glycanEdges.add(edge);
            }
            if(edge instanceof SubstituentEdge && includeSubstituentEdge){
                glycanEdges.add(edge);
            }
        }
        return glycanEdges;
    }

*/


    @Override
    public int hashCode() {
        int result = root.hashCode();
        result = 31 * result + nodes.hashCode();
        result = 31 * result + edges.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SaccharideGraph)) return false;

        SaccharideGraph saccharideGraph = (SaccharideGraph) o;

        if (nodes.size() != saccharideGraph.nodes.size()) return false;
        if (!root.equals(saccharideGraph.root)) return false;
        if (!checkMonosaccharidesListEquals(saccharideGraph, root, saccharideGraph.root, this.getChildren(root), saccharideGraph.getChildren(saccharideGraph.getRoot())))
            return false;
        Set<Monosaccharide> layerMonosaccharidesReference = this.monosaccharideEdgeMultimap.getChildren(root);
        Set<Monosaccharide> layerMonosaccharidesOther = saccharideGraph.monosaccharideEdgeMultimap.getChildren(root);
        Set<Monosaccharide> bufferNodeReference = new NodeSet<>(10);
        Set<Monosaccharide> bufferNodeOther = new NodeSet<>(10);

        while (!layerMonosaccharidesReference.isEmpty() && !layerMonosaccharidesOther.isEmpty()) {
            for (Monosaccharide node : layerMonosaccharidesReference) {
                for (Monosaccharide nodeOther : layerMonosaccharidesOther) {
                    if (checkMonosaccharidesListEquals(saccharideGraph, node, nodeOther, this.getChildren(node), saccharideGraph.getChildren(nodeOther))) {
                        layerMonosaccharidesOther.remove(nodeOther);
                        bufferNodeOther.addAll(saccharideGraph.monosaccharideEdgeMultimap.getChildren(nodeOther));
                        bufferNodeReference.addAll(this.monosaccharideEdgeMultimap.getChildren(node));
                    }
                }
                layerMonosaccharidesReference.remove(node);
            }

            if (layerMonosaccharidesReference.isEmpty() && !layerMonosaccharidesOther.isEmpty()) {
                return false;
            }

            if (layerMonosaccharidesReference.isEmpty() && layerMonosaccharidesOther.isEmpty()) {
                layerMonosaccharidesReference.addAll(bufferNodeReference);
                layerMonosaccharidesOther.addAll(bufferNodeOther);
            } else {
                throw new IllegalStateException("Problems in the equality function on saccharidegraph.");
            }
        }
        return true;
    }

    private boolean checkMonosaccharidesListEquals(SaccharideGraph otherGraph, Monosaccharide parentThis, Monosaccharide parentOther, NodeSet<GlycanNode> nodeListThis, NodeSet<GlycanNode> nodeListOther) {

        if (nodeListThis.size() != nodeListOther.size()) return false;
        int counter = 0;

        for (GlycanNode nodeThis : nodeListThis) {
            for (GlycanNode nodeOther : nodeListOther) {
                if (nodeThis.equals(nodeOther) && this.getGlycoLinkage(parentThis, nodeThis).equals(otherGraph.getGlycoLinkage(parentOther, nodeOther))) {
                    counter++;
                    break;
                }
            }
        }

        return counter == nodeListThis.size();
    }

    /**
     * Traverses the saccharide graph starting at the root. The type of traversal is determined
     * by <code>traversal</code>.
     *
     * @param traversal       determines weather the traversal is depth or breadth first
     * @param linkageAcceptor callback that gets called for every edge that is traversed
     */
    public void forEachLinkage(Traversal traversal, LinkageAcceptor linkageAcceptor) {

        final TraversalComparator traversalComparator = new TraversalComparator() {

            @Override
            public int compare(Monosaccharide child1, GlycosidicLinkage linkage1, Monosaccharide child2, GlycosidicLinkage linkage2) {

                Integer carbon1 = linkage1.getLinkedCarbon().or(Integer.MAX_VALUE);
                Integer carbon2 = linkage2.getLinkedCarbon().or(Integer.MAX_VALUE);

                int result = carbon1.compareTo(carbon2);
                return result == 0 ? child1.getName().compareTo(child2.getName()) : result;
            }
        };
        forEachLinkage(traversal, traversalComparator, linkageAcceptor);
    }

    /**
     * Traverses the saccharide graph starting at the root. The type of traversal is determined
     * by <code>traversal</code>. The order in which the children are traversed can be controlled using
     * the <code>traversalComparator</code>.
     *
     * @param traversal           determines weather the traversal is depth or breadth first
     * @param traversalComparator allows control of the order in which a nodes children are traversed
     * @param linkageAcceptor     callback that gets called for every edge that is traversed
     */
    public void forEachLinkage(Traversal traversal, final TraversalComparator traversalComparator, LinkageAcceptor linkageAcceptor) {

        checkNotNull(traversal);
        checkNotNull(linkageAcceptor);

        Comparator<GlycosidicEdge> comparator = new Comparator<GlycosidicEdge>() {
            @Override
            public int compare(GlycosidicEdge e1, GlycosidicEdge e2) {

                return traversalComparator.compare(e1.getChild(), e1.getLink(), e2.getChild(), e2.getLink());
            }
        };

        Deque<Monosaccharide> deque = new ArrayDeque<>();
        deque.add(getRoot());

        switch (traversal) {

            case BFS:

                doBFS(linkageAcceptor, deque, comparator);
                break;
            case DFS:

                doDFS(linkageAcceptor, deque, comparator);
                break;
            default:

                throw new IllegalStateException("Cannot execute " + traversal + " traversal");
        }
    }

    protected void doDFS(LinkageAcceptor linkageAcceptor, Deque<Monosaccharide> deque, Comparator<GlycosidicEdge> traversalComparator) {

        Map<Monosaccharide, GlycosidicEdge> parentMap = new IdentityHashMap<>();
        while (!deque.isEmpty()) {

            Monosaccharide current = deque.removeLast();
            Map<Monosaccharide, GlycosidicEdge> edgeMap = monosaccharideEdgeMultimap.getOutEdges(current);
            final List<GlycosidicEdge> childEdges = new ArrayList<>(edgeMap.values());
            Collections.sort(childEdges, traversalComparator);
            for (GlycosidicEdge edge : childEdges) {

                Monosaccharide child = edge.getChild();
                parentMap.put(child, edge);
                deque.add(child);
            }

            GlycosidicEdge edge = parentMap.get(current);
            if (edge != null) {

                linkageAcceptor.accept(edge.getParent(), current, edge.getLink());
            }
        }
    }

    protected void doBFS(LinkageAcceptor linkageAcceptor, Deque<Monosaccharide> deque, Comparator<GlycosidicEdge> traversalComparator) {

        while (!deque.isEmpty()) {

            Monosaccharide parent = deque.removeFirst();
            Map<Monosaccharide, GlycosidicEdge> edgeMap = monosaccharideEdgeMultimap.getOutEdges(parent);
            final List<GlycosidicEdge> childEdges = new ArrayList<>(edgeMap.values());
            Collections.sort(childEdges, traversalComparator);
            for (GlycosidicEdge edge : childEdges) {

                Monosaccharide child = edge.getChild();
                linkageAcceptor.accept(parent, child, edge.getLink());
                deque.add(child);
            }
        }
    }

    /**
     * A builder to build Glycan. This builder is not reusable.
     */
    protected abstract static class AbstractBuilder<G extends SaccharideGraph> {

        protected G graph;

        protected BuildSate buildSate = BuildSate.NEW;

        protected enum BuildSate {NEW, BUILDING, DONE}


        /**
         * Add a Monosaccharide to the Glycan.  The <code>monosaccharide</code> is attached to the <code>pattern</code> via the
         * <code>linkage</code>. If the parent has not been added to this glycan an IllegalStateException is thrown.
         *
         * @param monosaccharide the monosaccharide to add
         * @param parent         Monosaccharide to which <code>monosaccharide</code> is to be added
         * @param linkage        the linkage
         * @return the passed in monosaccharide
         */
        public Monosaccharide add(Monosaccharide monosaccharide, Monosaccharide parent, GlycosidicLinkage linkage) {

            checkIsBuilding();

            graph.add(monosaccharide, parent, linkage);

            return monosaccharide;
        }

        /**
         * Add <code>monosaccharide</code> and <code>substituent</code> to the Glycan being built. The <code>monosaccharide</code>
         * is attached to the <code>parent</code> using the <code>glycoLinkage</code> and the <code>substituent</code> is attached
         * to the <code>monosaccharide</code> using the <code>substituentLinkage</code>.
         * <p/>
         * <p>This method addes two nodes and two edges to the Glycan being built.
         * <p/>
         * <pre>
         *     substituent <-- substituentLinkage -- monosaccharide <-- glycoLinkage <-- parent
         * </pre>
         * <p/>
         * <p>If the parent has not been added to this glycan an IllegalStateException is thrown.
         *
         * @param monosaccharide     the Monosaccharide to add
         * @param substituent        the monosaccharide's Substituent
         * @param parent             the parent to which the monosaccharide is to be attached
         * @param glycoLinkage       the linkage between <code>parent</code> and <code>monosaccharide</code>
         * @param substituentLinkage the linkage between <code>monosaccharide</code> and <code>substituent</code>
         * @return the monosaccharide
         */
        public Monosaccharide add(Monosaccharide monosaccharide, Substituent substituent, Monosaccharide parent, GlycosidicLinkage glycoLinkage, SubstituentLinkage substituentLinkage) {

            checkIsBuilding();

            graph.add(monosaccharide, parent, glycoLinkage);

            graph.add(substituent, monosaccharide, substituentLinkage);

            return monosaccharide;
        }

        /**
         * Adda hyper edge from the list of parents to the monosaccharide.
         *
         * @param monosaccharide the monosaccharide to add
         * @param parents        the list of parents
         * @param linkage        the linkage between the parents and the monosaccharide
         * @return the monosaccharide
         */
        public Monosaccharide add(Monosaccharide monosaccharide, List<Monosaccharide> parents, GlycosidicLinkage linkage) {

            checkIsBuilding();

            graph.add(monosaccharide, parents, linkage);

            return monosaccharide;
        }

        /**
         * Adda substituent to parent
         *
         * @param substituent teh substituent to add
         * @param parent      the parent
         * @param linkage     the linkage between parent and substituent
         * @return the substituent
         */
        public Substituent add(Substituent substituent, Monosaccharide parent, SubstituentLinkage linkage) {

            checkIsBuilding();

            graph.add(substituent, parent, linkage);

            return substituent;
        }

        /**
         * Add a hyper edge from parents to substituent.
         *
         * @param substituent the substituent to add
         * @param parents     the list of parents to add the substituent to
         * @param linkage     the linkage between the parents and the substituent
         * @return the substituent
         */
        public Substituent add(Substituent substituent, List<Monosaccharide> parents, SubstituentLinkage linkage) {

            checkIsBuilding();

            graph.add(substituent, parents, linkage);

            return substituent;
        }


        /**
         * Return the <code>nodeId</code>'th node that was added
         *
         * @param nodeId the id of the node
         * @return the <code>nodeId</code>'th node that was added
         */
        public GlycanNode getNode(int nodeId) {

            checkIsBuilding();

            return graph.getNode(nodeId);
        }

        /**
         * Return the id of the <code>node</code>
         *
         * @param node the node
         * @return the id of the node
         */
        public int getId(GlycanNode node) {

            checkIsBuilding();

            return graph.nodes.indexOf(node);
        }

        protected void checkIsBuilding() {

            if (buildSate != BuildSate.BUILDING) {

                throw new IllegalStateException("Interactions on Builder that has already been built is not allowed");
            }
        }

        /**
         * Check for the presence of all cleaved monosaccharides inside the graph.
         * @param monosaccharides list of monosaccharides.
         * @return a boolean value.
         */
        
        protected boolean checkMonosaccharidesPresence(List<Monosaccharide> monosaccharides){

            int counter = 0;

            for(Monosaccharide monosaccharide : monosaccharides){
                for(GlycanNode node : graph.nodes){
                    if(node == monosaccharide){
                        counter++;
                    }
                }
            }

            return counter == monosaccharides.size();
        }
    }
}
