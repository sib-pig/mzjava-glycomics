/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.glycomics.mol;

import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A collection that maps an edge to a parent and child node.
 * <p/>
 * This class uses <tt>IdentityHashMap</tt>s internaly so that the nodes are
 * mapped using reference-equality in place of object-equality. This allows
 * two objects that have different identity but are equals to be used as two
 * separate nodes.
 * <p/>
 * <p/>
 * This class is not immutable.
 *
 * @param <P> the type of the parent node
 * @param <C> the type of the child node
 * @param <E> the type of the edge
 * @author Oliver Horlacher
 * @version sqrt -1
 */
class IdentityEdgeMultimap<P, C, E> {

    private final Map<P, Map<C, E>> edgeMap = new IdentityHashMap<P, Map<C, E>>();

    private final int initialEdgeCapacity;

    /**
     * Construct a new IdentityEdgeMultimap
     *
     * @param initialEdgeCapacity the initial capacity of the edge map
     */
    public IdentityEdgeMultimap(int initialEdgeCapacity) {

        this.initialEdgeCapacity = initialEdgeCapacity;
    }

    /**
     * Add an the <code>edge</code> from <code>parent</code> to <code>child</code>
     *
     * @param parent the parent node
     * @param child  the child node
     * @param edge   the edge
     */
    public void put(P parent, C child, E edge) {

        checkNotNull(parent);
        checkNotNull(child);
        checkNotNull(edge);

        Map<C, E> map = edgeMap.get(parent);
        if (map == null) {

            map = new IdentityHashMap<C, E>(initialEdgeCapacity);
            edgeMap.put(parent, map);
        }
        map.put(child, edge);
    }

    /**
     * Return the edge from <code>parent</code> to <code>child</code>, if there is no edge null is returned.
     *
     * @param parent the parent
     * @param child  the child
     * @return the edge from <code>parent</code> to <code>child</code>, if there is no edge null is returned
     */
    public E get(Object parent, Object child) {

        //noinspection SuspiciousMethodCalls
        Map<C, E> map = edgeMap.get(parent);

        if (map == null) return null;

        //noinspection SuspiciousMethodCalls
        return map.get(child);
    }

    /**
     * Return a set containing all the children of <code>node</code>. If <code>node</code> is a leaf
     * an empty set is returned
     *
     * @param node the node
     * @return a set containing all the children of <code>node</code>. If <code>node</code> is a leaf
     * an empty set is returned
     */
    public Set<C> getChildren(Object node) {

        //noinspection SuspiciousMethodCalls
        Map<C, E> map = edgeMap.get(node);
        return map == null ? Collections.<C>emptySet() : map.keySet();
    }

    /**
     * Return an unmodifiable map containing all the edges originating from <code>node</code>.
     * If <code>node</code> is a leaf an empty map is returned
     *
     * @param node the node
     * @return an unmodifiable map containing all the edges originating from <code>node</code>.
     *         If <code>node</code> is a leaf an empty map is returned
     */
    public Map<C, E> getOutEdges(Object node) {

        //noinspection SuspiciousMethodCalls
        Map<C, E> map = edgeMap.get(node);
        return map == null ? Collections.<C, E>emptyMap() : Collections.unmodifiableMap(map);
    }
}
