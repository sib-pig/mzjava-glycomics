package org.expasy.mzjava.glycomics.mol;

import com.google.common.base.Preconditions;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.expasy.mzjava.core.mol.Composition;
import org.openide.util.lookup.ServiceProvider;

import java.io.*;
import java.util.*;

/**
 *
 * @author Davide Alocci
 * @version sqrt -1
 */

@ServiceProvider(service = MonosaccharideLookup.class)
public class DefaultMonosaccharideLookup implements MonosaccharideLookup{

    public static final String PROPERTY_MONOSACCHARIDE_LOOKUP =  "org.expasy.mzjava.glycomics.mol.monosaccharideLookup";
    private final Map<String, Monosaccharide> monosaccharideLookupMap = new LinkedHashMap<>();

    public DefaultMonosaccharideLookup() {

        final String defaultMonosaccharideLookupFile = "monosaccharideData.json";
        String property = System.getProperty(PROPERTY_MONOSACCHARIDE_LOOKUP, defaultMonosaccharideLookupFile);

        File lookupFile = new File(property);
        try (InputStream streamFile = lookupFile.exists() ? new FileInputStream(lookupFile) : this.getClass().getResourceAsStream(property);
             Reader in = new InputStreamReader(streamFile, "UTF-8")) {

            parse(in, monosaccharideLookupMap);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("The encoding is not supported.", e);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException("Monosaccharide Lookup file not found. Please specify a existing file in System Property " + PROPERTY_MONOSACCHARIDE_LOOKUP + ".  " +
                    "Actual value: " + property, e);
        } catch (IOException e) {
            throw new IllegalStateException("Monosaccharide Lookup file error closing stream. ", e);
        }
    }


    /**
     * Search for a monosaccharide with the input name the list of known monosaccharides.
     * <p>The method throw an IllegalStateException if there is no monosaccharide with the input name in the list.</p>
     * @param name the name of the monosaccharide
     * @return A monosaccharide object with the specified name.
     */
    public final Monosaccharide getNew(String name) {

        Preconditions.checkNotNull(name);
        Monosaccharide monosaccharide = monosaccharideLookupMap.get(name);

        if(monosaccharide == null){
            throw new IllegalArgumentException(name + " is not in the list. If you want to add it, you can specify a new Monosaccharide Lookup file in System Property " + PROPERTY_MONOSACCHARIDE_LOOKUP + ".  " +
                    "Actual value: " + System.getProperty(PROPERTY_MONOSACCHARIDE_LOOKUP, "monosaccharideData.json"));
        }

        return new Monosaccharide(monosaccharide);
    }

    /**
     * Return a list of the monosaccharides specified in Monosaccharide Lookup file.
     * @return A list of the known monosaccharides
     */
    public final List<Monosaccharide> values() {

        return new ArrayList<>(monosaccharideLookupMap.values());
    }


    /**
     * Parser for the Monosaccharide Lookup file
     * @return A map that has the name as Key and the Monosaccharide as Value
     */
    protected Map<String, Monosaccharide> parse(Reader in, Map<String, Monosaccharide> monosaccharideLookupMap) {

        ObjectMapper mapper = new ObjectMapper();
        final JsonNode monosaccharidesArray;

        Preconditions.checkNotNull(in);

        try {
            monosaccharidesArray = mapper.readTree(in);
        } catch (IOException e) {
            throw new IllegalStateException("Monosaccharide Lookup file error", e);
        }

        for (final JsonNode monosaccharideNode : monosaccharidesArray) {

            String mName = "name";
            String mComposition = "composition";
            String mSuperclass = "superclass";
            String mClass = "class";
            String mRing = "ring";
            String mFirstRingCarbon = "ringstart";
            String mLastRingCarbon = "ringend";


            JsonNode monosaccharideName = monosaccharideNode.path(mName);
            JsonNode monosaccharideComp = monosaccharideNode.path(mComposition);
            JsonNode monosaccharideSupClass = monosaccharideNode.path(mSuperclass);
            JsonNode monosaccharideClass = monosaccharideNode.path(mClass);
            JsonNode monosaccharideRing= monosaccharideNode.path(mRing);
            JsonNode monosaccharideFirstRingCarbon = monosaccharideNode.path(mFirstRingCarbon);
            JsonNode monosaccharideLastRingCarbon= monosaccharideNode.path(mLastRingCarbon);

            checkJsonNode(monosaccharideName, mName);
            checkJsonNode(monosaccharideComp, mComposition);
            checkJsonNode(monosaccharideSupClass, mClass);
            checkJsonNode(monosaccharideClass, mSuperclass);
            checkJsonNode(monosaccharideRing, mRing);


            try {
                MonosaccharideSuperclass.valueOf(monosaccharideSupClass.getTextValue());
            } catch (IllegalArgumentException  e) {
                throw new IllegalStateException("In the Monosaccharide lookup file there are unknown superclasses. This is the acutal list of known superclasses :"+ Arrays.asList(MonosaccharideSuperclass.values()), e);
            }

            try {
                MonosaccharideClass.valueOf(monosaccharideClass.getTextValue());
            } catch (IllegalArgumentException  e) {
                throw new IllegalStateException("In the Monosaccharide lookup file there are unknown classes. This is the acutal list of known classes :"+ Arrays.asList(MonosaccharideClass.values()), e);
            }

            Monosaccharide monosaccharide = new Monosaccharide(monosaccharideName.getTextValue(), Composition.parseComposition(monosaccharideComp.getTextValue()), MonosaccharideSuperclass.valueOf(monosaccharideSupClass.getTextValue()),MonosaccharideClass.valueOf(monosaccharideClass.getTextValue()), RingType.getRingType(monosaccharideRing.getTextValue().charAt(0)),monosaccharideFirstRingCarbon.getIntValue(), monosaccharideLastRingCarbon.getIntValue());
            monosaccharideLookupMap.put(monosaccharide.getName(), monosaccharide);
        }
        return monosaccharideLookupMap;
    }


    private void checkJsonNode(JsonNode node, String nodePath){

        if (node.isMissingNode()) {
            throw new IllegalStateException("Mapping not possible. Each record in the Monosaccharide Lookup file need to have " + nodePath + ".");
        }
        if ("".equals(node.getTextValue())){
            throw new IllegalStateException("Monosaccharide without "+nodePath+" are not allowed in the Monosaccharide Lookup file.");
        }

        if("ring".equals(nodePath)){
            Preconditions.checkArgument("p".equals(node.getTextValue()) || "f".equals(node.getTextValue()) || "o".equals(node.getTextValue()), "The ring value must be f for furanose or p for pyranose");
        }
    }

}
