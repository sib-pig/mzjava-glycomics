/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.glycomics.io.ms.gwp;

import com.generationjava.io.xml.PrettyPrinterXmlWriter;
import com.generationjava.io.xml.SimpleXmlWriter;
import com.generationjava.io.xml.XmlWriter;
import org.expasy.mzjava.core.io.ms.spectrum.PeakListPrecisionFormat;
import org.expasy.mzjava.core.ms.peaklist.PeakList;

import java.io.IOException;
import java.io.Writer;
import java.text.NumberFormat;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A writer to write peak lists in the glycoworkbench gwp format
 *
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class GWPWriter {

    private final XmlWriter xmlWriter;
    private final NumberFormat mzFormat;
    private final NumberFormat intensityFormat;

    public GWPWriter(Writer writer, PeakList.Precision precision) throws IOException {

        xmlWriter = new PrettyPrinterXmlWriter(new SimpleXmlWriter(writer));

        xmlWriter.writeXmlVersion();
        xmlWriter.writeEntity("GlycanWorkspace");

        xmlWriter.writeEntity("Configuration");
        xmlWriter.endEntity();

        mzFormat = PeakListPrecisionFormat.getMzFormat(precision);
        intensityFormat = PeakListPrecisionFormat.getIntensityFormat(precision);
    }

    /**
     * Write the spectrum
     *
     * @param spectrum the spectrum to write
     * @param gwStructure the glycoworkbench formatted structure
     * @throws IOException
     */
    public void write(String name, PeakList spectrum, String gwStructure, String notes, int msLevel) throws IOException {

        checkNotNull(spectrum);
        checkNotNull(gwStructure);
        checkNotNull(notes);

        xmlWriter.writeEntity("Scan");
        xmlWriter.writeAttribute("name", name);
        xmlWriter.writeAttribute("precursor_mz", spectrum.getPrecursor().getMz());
        xmlWriter.writeAttribute("is_msms", msLevel == 2);

        xmlWriter.writeEntity("Structures");
        xmlWriter.writeEntity("Glycan");
        xmlWriter.writeAttribute("structure", gwStructure);
        xmlWriter.endEntity();
        xmlWriter.endEntity();

        xmlWriter.writeEmptyEntity("Fragments");


        xmlWriter.writeEntity("PeakList");
        for(int i = 0; i < spectrum.size(); i++) {

            xmlWriter.writeEntity("Peak");
            xmlWriter.writeAttribute("mz_ratio", mzFormat.format(spectrum.getMz(i)));
            xmlWriter.writeAttribute("intensity", intensityFormat.format(spectrum.getIntensity(i)));
            xmlWriter.endEntity();
        }
        xmlWriter.endEntity();

        xmlWriter.writeEntity("Notes");
        if(notes.length() > 0) {
            xmlWriter.writeText(notes);
        }
        xmlWriter.endEntity();

        xmlWriter.endEntity();
    }

    public void close() throws IOException {

        xmlWriter.endEntity();
        xmlWriter.close();
    }
}
