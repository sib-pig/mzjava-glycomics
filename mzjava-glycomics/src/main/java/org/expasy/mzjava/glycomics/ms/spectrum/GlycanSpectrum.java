package org.expasy.mzjava.glycomics.ms.spectrum;

import com.google.common.base.Preconditions;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessor;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.IdentityPeakProcessor;
import org.expasy.mzjava.core.ms.spectrum.Spectrum;
import org.expasy.mzjava.glycomics.mol.Glycan;


/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class GlycanSpectrum extends Spectrum<GlycanFragAnnotation> {

    private final Glycan glycan;
    private final int charge;

    /**
     * Constructor for GlycanSpectrum. The mass spectrum level is already set at two.
     * @param glycan the glycan for creating the spectrum.
     * @param charge the charge of the peaks in the spectrum.
     */
    public GlycanSpectrum(Glycan glycan, int charge) {

        this(glycan, charge, 0, Precision.DOUBLE);
    }

    /**
     * Constructor for GlycanSpectrum.
     * @param src the spectrum that belong to the glycan
     * @param glycan the glycan used for creating the specturm.
     * @param charge the charge of the peaks in the spectrum.
     */
    public GlycanSpectrum(Spectrum<GlycanFragAnnotation> src, Glycan glycan, int charge) {

        super(src, new IdentityPeakProcessor<GlycanFragAnnotation>());
        this.glycan = glycan;
        this.charge = charge;
    }

    /**
     * Constructor for GlycanSpectrum. The mass spectrum level is already set at two.
     * @param glycan the glycan for creating the spectrum.
     * @param charge the charge of the peaks in the spectrum.
     * @param precision the precision of the peaklist.
     */
    public GlycanSpectrum(Glycan glycan, int charge, Precision precision) {

        this(glycan, charge, 0, precision);
    }

    /**
     * Constructor for GlycanSpectrum. The mass spectrum level is already set at two.
     * @param glycan the glycan for creating the spectrum.
     * @param charge the charge of the peaks in the spectrum.
     * @param initialCapacity the initial capacity of the peaklist.
     * @param precision the precision of the peaklist.
     */
    public GlycanSpectrum(Glycan glycan, int charge, int initialCapacity, Precision precision) {

        super(initialCapacity, precision);

        Preconditions.checkNotNull(glycan);
        Preconditions.checkArgument(charge != 0);

        this.glycan = glycan;
        this.charge = charge;
        setMsLevel(2);
    }

    /**
     * Constructor for GlycanSpectrum. The mass spectrum level is already set at two.
     * @param glycan the glycan for creating the spectrum.
     * @param charge the charge of the peaks in the spectrum.
     * @param initialCapacity the initial capacity of the peaklist.
     * @param constantIntensity the fixed intensity of each peak.
     * @param precision the precision of the peaklist, in this case you can choose only DOUBLE_CONSTANT or FLOAT_CONSTANT.
     */
    public GlycanSpectrum(Glycan glycan, int charge, int initialCapacity, double constantIntensity, Precision precision) {

        super(initialCapacity, constantIntensity, precision);

        Preconditions.checkNotNull(glycan);
        this.glycan = glycan;
        this.charge = charge;
        setMsLevel(2);
    }

    /**
     * Copy constructor
     * @param src the GlycanSpectrum to copy.
     */
    protected GlycanSpectrum(GlycanSpectrum src, PeakProcessor<GlycanFragAnnotation, GlycanFragAnnotation> peakProcessor) {

        super(src, peakProcessor);

        glycan = src.glycan;
        charge = src.charge;
    }

    /**
     * Copy constructor
     * @param src the GlycanSpectrum to copy.
     */
    protected GlycanSpectrum(GlycanSpectrum src, PeakProcessorChain<GlycanFragAnnotation> peakProcessorChain) {

        super(src, peakProcessorChain);

        glycan = src.glycan;
        charge = src.charge;
    }



    /**
     * Returns the glycan used for creating the spectrum.
     * @return the glycan
     */

    public Glycan getGlycan() {
        return glycan;
    }

    /**
     * Returns the charge of the peaks in the spectrum.
     * @return the charge.
     */
    public int getCharge() {
        return charge;
    }

    @Override
    public GlycanSpectrum copy(PeakProcessor<GlycanFragAnnotation, GlycanFragAnnotation> peakProcessor) {
        return new GlycanSpectrum(this, peakProcessor);
    }

    @Override
    public GlycanSpectrum copy(PeakProcessorChain<GlycanFragAnnotation> peakProcessorChain) {
        return new GlycanSpectrum(this, peakProcessorChain);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GlycanSpectrum)) return false;
        if (!super.equals(o)) return false;

        GlycanSpectrum that = (GlycanSpectrum) o;

        if (charge != that.charge) return false;
        return glycan.equals(that.glycan);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + glycan.hashCode();
        result = 31 * result + charge;
        return result;
    }
}

