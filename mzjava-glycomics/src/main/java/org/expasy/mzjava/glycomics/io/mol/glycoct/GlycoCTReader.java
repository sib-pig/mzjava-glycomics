/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.glycomics.io.mol.glycoct;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.glycomics.mol.*;
import org.openide.util.Lookup;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: jmarieth
 * Date: 10/29/13
 * Time: 4:32 PM
 */
public class GlycoCTReader {

    private static final String RES = "RES";
    private static final String LIN = "LIN";
    private static final char BASETYPE = 'b';
    private static final char SUBSTITUENT = 's';
    private static final char COLON = ':';
    private static final char PLUS = '+';
    private static final char MINUS = '-';
    private static final char OPENING_BRACKET = '(';
    private static final char CLOSING_BRACKET = ')';
    private static final char UNKNOWN_CHARACTER = 'X';
    private static final int UNKNOWN_INTEGER = -1;

    private final GlycoCtResolver ctResolver;

    public GlycoCTReader() {

        ctResolver = Lookup.getDefault().lookup(GlycoCtResolver.class);
        Preconditions.checkNotNull(ctResolver, "No GlycoCTResolver implementation class found: org.openide.util.Lookup has not properly worked (hint: check that Annotation Compiler Processing is enable)");
    }


    public Glycan read(String glycoct, String databaseIdentifier) {

        List<String> glycoctArray = glycoctToList(glycoct);

        return glycoctListsToSugar(databaseIdentifier, readRES(glycoctArray), readLIN(glycoctArray));
    }

    protected Glycan glycoctListsToSugar(String databaseIdentifier, List<String> residuesList, List<String> linkagesList) {

        Map<Integer, String> residues = glycoctListToMap(residuesList);
        Map<Integer, String> linkages = glycoctListToMap(linkagesList);

        Glycan.Builder builder = new Glycan.Builder();
        //Make root node. We know this will be node 0
        if(residues.get(1).contains(":aldi")){
            builder.setRoot(getMonosaccharide(residues.get(1)), Optional.<Anomericity>absent(), databaseIdentifier, Composition.parseComposition("H2"));
        } else {
            builder.setRoot(getMonosaccharide(residues.get(1)), Optional.<Anomericity>absent(), databaseIdentifier);
        }



        //root is already set with id = 0 (map key = 1)
        for (int i = 1; i < linkages.size() + 1; i++) {

            String linkage = linkages.get(i);
            String residueString = residues.get(getAnomericResidueId(linkage));
            char residueType = getGlycoctResidueType(residueString);

            Integer parentGlycoctKey = getLinkedResidueId(linkage);
            Integer parentNodeId = parentGlycoctKey - 1;

            if (residueType == BASETYPE) {

                Integer anomericCarbon = getAnomericCarbon(linkage);
                Integer linkedCarbon = getLinkedCarbon(linkage);

                Optional<Anomericity> anomericity = ctResolver.getAnomericity(getGlycoctResidueAnomer(residueString).toString());

                Optional<Composition> anomericComposition = ctResolver.getLinkageComposition(getAnomericCompsition(linkage).toString());
                Optional<Composition> linkedComposition = ctResolver.getLinkageComposition(getLinkedCompsition(linkage).toString());

                //todo problem with multiple linkage on substituent ( glycomedb -> 19826)
                builder.add(getMonosaccharide(residueString), (Monosaccharide)builder.getNode(parentNodeId),
                        new GlycosidicLinkage(anomericity,
                                getOptionalInteger(anomericCarbon),
                                getOptionalInteger(linkedCarbon),
                                anomericComposition,
                                linkedComposition
                        ));
            }
            if (residueType == SUBSTITUENT) {

                builder.add(getSubstituent(residueString), (Monosaccharide)builder.getNode(parentNodeId),
                        new SubstituentLinkage(getOptionalInteger(getLinkedCarbon(linkage)), ctResolver.getLinkageComposition(getLinkedCompsition(linkage).toString())));
            }

        }

        return builder.build();
    }

    protected Map<Integer, String> glycoctListToMap(List<String> glycoctList) {

        Preconditions.checkNotNull(glycoctList);

        Map<Integer, String> residues = new HashMap<Integer, String>();

        for (String glycoct : glycoctList) {

            residues.put(getGlycoctIndex(glycoct), glycoct);
        }
        return residues;
    }

    protected Substituent getSubstituent(String glycoctResidueLine) {

        Preconditions.checkNotNull(glycoctResidueLine);
        String residue = glycoctResidueLine.substring(glycoctResidueLine.indexOf(COLON) + 1);
        return ctResolver.getSubstituent(residue);
    }

    protected Monosaccharide getMonosaccharide(String glycoctResidueLine) {

        Preconditions.checkNotNull(glycoctResidueLine);

        Integer index = glycoctResidueLine.indexOf(MINUS) + 1;

        String residueString = glycoctResidueLine.substring(index);

        //most glycoct representation of the monosaccharide contains the anomericity
        if (ctResolver.getMonosaccharide(residueString) == null) {

            residueString = glycoctResidueLine.substring(index + 1);
        }

        return ctResolver.getMonosaccharide(residueString);
    }

    protected Integer getNumericValue(String glycoctLine, int offset) {

        StringBuilder idx = new StringBuilder();
        int i = offset;

        Preconditions.checkNotNull(glycoctLine);

        if (glycoctLine.charAt(i) == MINUS) {
            idx.append(MINUS);
            i++;
        }

        while (Character.isDigit(glycoctLine.charAt(i)) && i < glycoctLine.length()) {
            idx.append(glycoctLine.charAt(i));
            i++;
        }


        return Integer.valueOf(idx.toString());
    }

    protected Optional<Integer> getOptionalInteger(Integer value) {

        if (!value.equals(UNKNOWN_INTEGER)) {
            return Optional.of(value);
        } else return Optional.absent();
    }

    protected Optional<Character> getOptionalCharacter(Character value) {

        if (Character.toUpperCase(value) != UNKNOWN_CHARACTER) {
            return Optional.of(value);
        } else return Optional.absent();
    }


    //Glycoct index is available for every entry in any glycoct section
    protected Integer getGlycoctIndex(String glycoctLine) {

        return getNumericValue(glycoctLine, 0);
    }


    protected Integer getAnomericResidueId(String glycoctLinkageLine) {

        return getNumericValue(glycoctLinkageLine, glycoctLinkageLine.indexOf(CLOSING_BRACKET) + 1);
    }

    protected Integer getAnomericCarbon(String glycoctLinkageLine) {

        return getNumericValue(glycoctLinkageLine, glycoctLinkageLine.indexOf(PLUS) + 1);
    }

    protected Character getAnomericCompsition(String glycoctLinkageLine) {

        return glycoctLinkageLine.charAt(glycoctLinkageLine.length() - 1);
    }


    protected Integer getLinkedResidueId(String glycoctLinkageLine) {

        return getNumericValue(glycoctLinkageLine, glycoctLinkageLine.indexOf(COLON) + 1);
    }

    protected Integer getLinkedCarbon(String glycoctLinkageLine) {

        return getNumericValue(glycoctLinkageLine, glycoctLinkageLine.indexOf(OPENING_BRACKET) + 1);
    }

    protected Character getLinkedCompsition(String glycoctLinkageLine) {

        return glycoctLinkageLine.charAt(glycoctLinkageLine.indexOf(OPENING_BRACKET) - 1);
    }

    protected Map<Integer, String> findLinkages(Map<Integer, String> allLinkages, int anomericResidueId) {

        Preconditions.checkNotNull(allLinkages);
        Preconditions.checkNotNull(anomericResidueId);

        Map<Integer, String> linkages = new HashMap<Integer, String>();

        int i = 1;
        //there shoulb be only 1 linkage except if double bound or cyclic
        while (allLinkages.get(i) != null) {
            if (getAnomericResidueId(allLinkages.get(i)) == anomericResidueId) {
                linkages.put(i, allLinkages.get(i));
            }
            i++;
        }

        return linkages;
    }

    protected char getGlycoctResidueType(String glycoctLine) {

        Preconditions.checkNotNull(glycoctLine);

        Integer index = getGlycoctIndex(glycoctLine);

        return glycoctLine.charAt(glycoctLine.indexOf(index.toString()) + index.toString().length());
    }

    protected Character getGlycoctResidueAnomer(String glycoctLine) {

        Character anomer = null;
        Preconditions.checkNotNull(glycoctLine);

        if (getGlycoctResidueType(glycoctLine) == BASETYPE) {
            anomer = glycoctLine.charAt(glycoctLine.indexOf(':') + 1);
        }
        return anomer;
    }


    protected List<String> glycoctToList(String glycoct) {

        return new ArrayList<String>(Arrays.asList(glycoct.split("\\r?\\n")));
    }

    protected List<String> readRES(List<String> glycoctList) {

        List<String> residues = new ArrayList<String>();
        int startIndex = getStartIndex(glycoctList, RES);
        int stopIndex = getStopIndex(glycoctList, LIN);

        for (int i = startIndex; i < stopIndex + 1; i++) {
            residues.add(glycoctList.get(i));
        }

        return residues;
    }


    protected List<String> readLIN(List<String> glycoctList) {

        List<String> linkages = new ArrayList<String>();
        int startIndex = getStartIndex(glycoctList, LIN);

        //TODO : define stop index for glycoct with UND, REP or ALT
        int stopIndex = glycoctList.size() - 1;

        for (int i = startIndex; i < stopIndex + 1; i++) {
            linkages.add(glycoctList.get(i));
        }

        return linkages;
    }

    protected int getStartIndex(List<String> al, String startTag) {

        int i = 0;
        while (i < al.size() && !al.get(i).equals(startTag)) {
            i++;
        }
        return i + 1;
    }

    protected int getStopIndex(List<String> al, String stopTag) {

        int i = 0;

        while (i < al.size() && !(al.get(i).equals(stopTag))) {
            i++;
        }


        if (i == (al.size() - 1)) { // stopTag NOT found
            return i;
        } else { // stopTag found
            return i - 1;
        }
    }
}
