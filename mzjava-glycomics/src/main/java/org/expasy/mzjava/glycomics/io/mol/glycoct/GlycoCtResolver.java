package org.expasy.mzjava.glycomics.io.mol.glycoct;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.glycomics.mol.Anomericity;
import org.expasy.mzjava.glycomics.mol.Monosaccharide;
import org.expasy.mzjava.glycomics.mol.Substituent;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public interface GlycoCtResolver {
    Monosaccharide getMonosaccharide(String glycoCtCode);

    Substituent getSubstituent(String glycoCtCode);

    Optional<Anomericity> getAnomericity(String glycoCtCode);

    Optional<Composition> getLinkageComposition(String glycoCtCode);
}
