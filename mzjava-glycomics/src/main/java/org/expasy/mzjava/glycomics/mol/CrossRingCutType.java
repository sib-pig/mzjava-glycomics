package org.expasy.mzjava.glycomics.mol;

import org.expasy.mzjava.glycomics.mol.CutIndexes;
import org.expasy.mzjava.glycomics.mol.Monosaccharide;

import java.util.Set;

/**
 * The CrossRingCutType interface allows you to get specific Crossring cleavage sites.
 * @author Davide Alocci
 * @version sqrt -1.
 */
public interface CrossRingCutType {

    /**
     * Return an array of Crossring indexes. The Crossring indexes specify the two sites where the Crossring of a monosaccharide is cleaved.
     * @param monosaccharide the monosaccharide to be fragment.
     * @return an array of Crossring indexex
     */
    Set<CutIndexes> get(Monosaccharide monosaccharide);
}
