package org.expasy.mzjava.glycomics.ms.fragment;

import com.google.common.base.Preconditions;
import org.expasy.mzjava.core.ms.spectrum.AnnotatedPeak;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.glycomics.mol.Glycan;
import org.expasy.mzjava.glycomics.mol.GlycanFragment;
import org.expasy.mzjava.glycomics.mol.GlycanMassCalculator;
import org.expasy.mzjava.glycomics.ms.spectrum.GlycanFragAnnotation;
import org.openide.util.Lookup;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class GlycanFragmentPeakGenerator implements GlycanPeakGenerator<GlycanFragAnnotation> {

    private final Set<FragmentType> fragmentTypes;
    private final Set<IonType> ionTypes;
    private final int peakIntensity;

    public GlycanFragmentPeakGenerator(Set<FragmentType> fragmentTypes, Set<IonType> ionTypes, int peakIntensity) {

        Preconditions.checkNotNull(fragmentTypes);
        Preconditions.checkArgument(!fragmentTypes.isEmpty(), "There is no FragmentType. Please specify a set of IonType");

        this.peakIntensity = peakIntensity;
        this.fragmentTypes = fragmentTypes;
        this.ionTypes = ionTypes;
    }

    /**
     * Generate peaks that are observed for the fragment at the supplied charge states.
     * The generated peaks are added to the peaks list.
     * @param fragment the glycan fragment taken into account.
     * @param charges an array of charges used for generate different peaks with different charges.
     * @param annotatedPeaks the peaklist where you want to add the new generated peaks.
     * @return the annotatedPeaks with the new generated peaks
     */
    public List<AnnotatedPeak<GlycanFragAnnotation>> generatePeaks(Glycan precursor,GlycanFragment fragment, int[] charges, List<AnnotatedPeak<GlycanFragAnnotation>> annotatedPeaks) {

        Preconditions.checkNotNull(fragment);
        Preconditions.checkNotNull(precursor);
        Preconditions.checkNotNull(charges);

        GlycanMassCalculator glycanMassCalculator = Lookup.getDefault().lookup(GlycanMassCalculator.class);

        if(fragment.isEmpty()){
            throw new IllegalArgumentException("Fragment has to have a size that is > 0");
        }

        List<AnnotatedPeak<GlycanFragAnnotation>> containerAnnotatedPeaks;

        if(annotatedPeaks != null){
            containerAnnotatedPeaks = annotatedPeaks;
        } else {
            containerAnnotatedPeaks = new ArrayList<AnnotatedPeak<GlycanFragAnnotation>>();
        }

        if(charges.length < 1){
            return containerAnnotatedPeaks;
        }

        if(fragmentTypes.contains(fragment.getFragmentType()) && ionTypes.containsAll(fragment.getIonTypes())){
            for (int charge : charges) {
                AnnotatedPeak<GlycanFragAnnotation> peak = new AnnotatedPeak<GlycanFragAnnotation>(
                        glycanMassCalculator.calculateMz(fragment.calculateMolecularMass(), charge, fragment.getFragmentType()),
                        peakIntensity, charge, new GlycanFragAnnotation(charge, fragment));
                containerAnnotatedPeaks.add(peak);
            }
        }
        return containerAnnotatedPeaks;
    }

    /**
     * Return the set of fragment type used in the peak generator.
     * @return the fragment type used in the peak generator.
     */
    public Set<FragmentType> getFragmentTypes() {
        return fragmentTypes;
    }

}
