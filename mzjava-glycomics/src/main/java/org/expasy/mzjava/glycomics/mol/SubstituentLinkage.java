/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.glycomics.mol;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import org.expasy.mzjava.core.mol.Composition;

import java.util.Objects;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class SubstituentLinkage implements Linkage {

    public static final SubstituentLinkage UNKNOWN = new SubstituentLinkage(Optional.<Integer>absent(), Optional.<Composition>absent());

    private final Optional<Integer> linkedCarbon;
    private final Optional<Composition> linkedComposition;

    public SubstituentLinkage(Integer linkedCarbon, Composition linkedComposition) {

        this(Optional.fromNullable(linkedCarbon), Optional.fromNullable(linkedComposition));
    }

    public SubstituentLinkage(Optional<Integer> linkedCarbon, Optional<Composition> linkedComposition) {

        Preconditions.checkNotNull(linkedCarbon);
        Preconditions.checkNotNull(linkedComposition);

        this.linkedCarbon = linkedCarbon;
        this.linkedComposition = linkedComposition;
    }

    /**
     * Check if the linkage has the anomeric carbon already set.
     * @return a boolean value that is true if the carbon is set otherwise return false.
     */
    public boolean hasSetCarbon(){
        if(linkedCarbon.isPresent()){
            return true;
        }
        return false;
    }

    public Optional<Integer> getLinkedCarbon() {

        return linkedCarbon;
    }

    public Optional<Composition> getLinkedComposition() {

        return linkedComposition;
    }

    @Override
    public int hashCode() {

        return Objects.hash(linkedCarbon, linkedComposition);
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final SubstituentLinkage other = (SubstituentLinkage) obj;
        return Objects.equals(this.linkedCarbon, other.linkedCarbon)
                && Objects.equals(this.linkedComposition, other.linkedComposition);
    }

    @Override
    public String toString() {

        String linkedCarb = linkedCarbon.isPresent() ? linkedCarbon.get().toString() : "-1";
        String linkedComp = linkedComposition.isPresent() ? linkedComposition.get().toString() : "unknown";
        return "SubstituentLinkage{" +
                "linkedCarbon=" + linkedCarb + "," +
                "linkedComposition=" + linkedComp +
                '}';
    }

    /**
     * Return the molecular mass of the composition change due to the linkage.
     * @return the mass.
     */
    @Override
    public double getMolecularMass() {
        Preconditions.checkArgument(linkedComposition.isPresent(),"Can not calculate the linkage mass if there is no linked composition");
        return linkedComposition.get().getMolecularMass();
    }
}

