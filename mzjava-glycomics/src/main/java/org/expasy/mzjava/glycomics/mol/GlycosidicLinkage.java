/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.glycomics.mol;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import org.expasy.mzjava.core.mol.Composition;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class GlycosidicLinkage implements Linkage {

    public static final GlycosidicLinkage UNKNOWN = new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent());

    private final Optional<Anomericity> anomericity;
    private final Optional<Integer> anomericCarbon;
    private final Optional<Integer> linkedCarbon;
    private final Optional<Composition> anomericComposition;
    private final Optional<Composition> linkedComposition;


    public GlycosidicLinkage(Anomericity anomericity, Integer anomericCarbon, Integer linkedCarbon, Composition anomericComposition, Composition linkedComposition ) {

        this(Optional.fromNullable(anomericity), Optional.fromNullable(anomericCarbon), Optional.fromNullable(linkedCarbon), Optional.fromNullable(anomericComposition), Optional.fromNullable(linkedComposition));
    }

    public GlycosidicLinkage(Optional<Anomericity> anomericity, Optional<Integer> anomericCarbon, Optional<Integer> linkedCarbon, Optional<Composition> anomericComposition, Optional<Composition> linkedComposition) {

        checkNotNull(anomericity);
        checkNotNull(anomericCarbon);
        checkNotNull(linkedCarbon);
        checkNotNull(anomericComposition);
        checkNotNull(linkedComposition);

        if (anomericCarbon.isPresent())
            checkArgument(anomericCarbon.get() > 0);
        if(linkedCarbon.isPresent())
            checkArgument(linkedCarbon.get() > 0);

        this.anomericity = anomericity;
        this.anomericCarbon = anomericCarbon;
        this.linkedCarbon = linkedCarbon;
        this.anomericComposition = anomericComposition;
        this.linkedComposition = linkedComposition;

    }

    /**
     * Check if the linkage has the anomeric and the linked carbons already set.
     * @return a boolean value that is true if the two carbons are set otherwise return false.
     */
    public boolean hasSetCarbon(){
        if(!anomericCarbon.isPresent()){
            return false;
        }
        if(!linkedCarbon.isPresent()){
            return false;
        }
        return true;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GlycosidicLinkage linkage = (GlycosidicLinkage) o;

        return anomericCarbon.equals(linkage.anomericCarbon) &&
                anomericity.equals(linkage.anomericity) &&
                linkedCarbon.equals(linkage.linkedCarbon) &&
                anomericComposition.equals(linkage.anomericComposition) &&
                linkedComposition.equals(linkage.linkedComposition);

    }

    @Override
    public int hashCode() {

        int result = anomericity.hashCode();
        result = 31 * result + anomericCarbon.hashCode();
        result = 31 * result + linkedCarbon.hashCode();
        result = 31 * result + anomericComposition.hashCode();
        result = 31 * result + linkedComposition.hashCode();

        return result;
    }

    public Optional<Anomericity> getAnomericity() {

        return anomericity;
    }

    public Optional<Integer> getAnomericCarbon() {

        return anomericCarbon;
    }

    public Optional<Integer> getLinkedCarbon() {

        return linkedCarbon;
    }

    public Optional<Composition> getAnomericComposition() {

        return anomericComposition;
    }

    public Optional<Composition> getLinkedComposition() {

        return linkedComposition;
    }

    @Override
    public String toString() {

        String unknown = "unknown";

        return "GlycosidicLinkage{" +
                "anomericity=" + (anomericity.isPresent() ? anomericity.get().toString() : unknown) +
                ", anomericCarbon=" + (anomericCarbon.isPresent() ? anomericCarbon.get().toString() : "-1") +
                ", linkedCarbon=" + (linkedCarbon.isPresent() ? linkedCarbon.get().toString() : "-1") +
                ", anomericComposition=" + (anomericComposition.isPresent() ? anomericComposition.get().toString() : unknown) +
                ", linkedComposition=" + (linkedComposition.isPresent() ? linkedComposition.get().toString() : unknown)+
                '}';
    }

    /**
     * Return the molecular mass of hte composition change due to the linkage.
     * @return the mass.
     */

    @Override
    public double getMolecularMass() {
        Preconditions.checkArgument(linkedComposition.isPresent(), "Can not calculate the linkage mass if there is no linked composition");
        Preconditions.checkArgument(anomericComposition.isPresent(), "Can not calculate the linkage mass if there is no anomeric composition");
        return linkedComposition.get().getMolecularMass() + anomericComposition.get().getMolecularMass();
    }
}
