package org.expasy.mzjava.glycomics.mol;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public enum MonosaccharideSuperclass {

    AcidicAmineSugar,
    AcidicSugar,
    Heptose,
    Hexose,
    Octose,
    Nonose,
    Hexosamine,
    Pentose
}
