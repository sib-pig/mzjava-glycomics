/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.glycomics.mol;

import com.google.common.base.Preconditions;
import org.expasy.mzjava.core.mol.Composition;

/**
 * @author Oliver Horlacher
 * @author Davide Alocci
 * @version sqrt -1
 */
public class Monosaccharide extends GlycanNode {

    private final MonosaccharideSuperclass monosaccharideSuperclass;
    private final MonosaccharideClass monosaccharideClass;
    private final int firstCarbonRing;
    private final int lastCarbonRing;
    private final RingType ring;

    public Monosaccharide(String name, Composition composition, MonosaccharideSuperclass monosaccharideSuperclass, MonosaccharideClass monosaccharideClass, RingType ring, int firstCarbonRing,int lastCarbonRing) {

        super(name, composition);
        Preconditions.checkNotNull(monosaccharideClass);
        Preconditions.checkNotNull(monosaccharideSuperclass);
        Preconditions.checkNotNull(ring);
        this.monosaccharideClass = monosaccharideClass;
        this.monosaccharideSuperclass = monosaccharideSuperclass;
        this.ring = ring;
        this.firstCarbonRing = firstCarbonRing;
        this.lastCarbonRing = lastCarbonRing;
    }

    public Monosaccharide(Monosaccharide src){

        super(src);
        this.ring = src.ring;
        this.monosaccharideClass = src.getMonosaccharideClass();
        this.monosaccharideSuperclass = src.getMonosaccharideSuperclass();
        this.firstCarbonRing = src.firstCarbonRing;
        this.lastCarbonRing = src.lastCarbonRing;
    }

    /**
     * Return the class of the Monosaccharide
     * @return the class of the Monosaccharide
     */
    public MonosaccharideClass getMonosaccharideClass() {
        return monosaccharideClass;
    }

    /**
     * Return the superclass of the Monosaccharide
     * @return the superclass of the Monosaccharide
     */
    public MonosaccharideSuperclass getMonosaccharideSuperclass(){
        return monosaccharideSuperclass;
    }

    /**
     * Return the ring type of the Monosaccharide
     * @return f if it is a furanose and p if it is a pyranose
     */
    public RingType getRing() {
        return ring;
    }

    /**
     * Return the number of the first carbon in the ring of the Monosaccharide.
     * @return the numeber of the carbon
     */
    public int getFirstCarbonRing() {
        return firstCarbonRing;
    }

    /**
     * Return the number of the last carbon in the ring of the Monosaccharide.
     * @return the numeber of the carbon
     */
    public int getLastCarbonRing() {
        return lastCarbonRing;
    }


    /**
     * Check if a carbon of the ring is in the fragment specified by the cutIndexes and the cutDirection.
     * @param cutIndexes  cut to be applied on the ring.
     * @param cutDirection part of the ring to take into account.
     * @param carbonNumber the number of the carbon in the ring.
     * @return <code>true</code> if the carbon is a part of the fragment;
     *         <code>false</code> otherwise
     */
    public boolean checkLinkedCarbon (CutIndexes cutIndexes,CutDirection cutDirection, int carbonNumber){

        boolean presence;
        if (carbonNumber > lastCarbonRing){
            carbonNumber = lastCarbonRing;
        }

        carbonNumber = carbonNumber - (firstCarbonRing - 1);

        presence = cutIndexes.getFirstIndex() < carbonNumber && carbonNumber <= cutIndexes.getSecondIndex();

        if(CutDirection.CLOCK_WISE.equals(cutDirection)){
            return presence;
        } else {
            return !presence;
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Monosaccharide)) return false;

        Monosaccharide that = (Monosaccharide) o;
        if (!name.equals(that.name)) return false;
        if (!composition.equals(that.composition)) return false;
        if (firstCarbonRing != that.firstCarbonRing) return false;
        if (lastCarbonRing != that.lastCarbonRing) return false;
        if (ring != that.ring) return false;
        if (monosaccharideClass != that.monosaccharideClass) return false;
        if (monosaccharideSuperclass != that.monosaccharideSuperclass) return false;

        return true;
    }

    @Override
    public int hashCode() {

        int result = name.hashCode();
        result = 31 * result + monosaccharideSuperclass.hashCode();
        result = 31 * result + monosaccharideClass.hashCode();
        result = 31 * result + composition.hashCode();
        result = 31 * result + firstCarbonRing;
        result = 31 * result + lastCarbonRing;
        result = 31 * result + ring.hashCode();
        return result;
    }

    @Override
    public String toString() {

        return "Monosaccharide{" +
                "name='" + name + '\'' +
                ", composition=" + composition +
                ", monosaccharideSuperclass=" + monosaccharideSuperclass +
                ", monosaccharideClass=" + monosaccharideClass +
                ", ring=" + ring +
                ", firstRingCarbon=" + firstCarbonRing +
                ", lastRingCarbon=" + lastCarbonRing +
                '}';
    }
}