package org.expasy.mzjava.glycomics.mol;

import com.google.common.base.Preconditions;
import org.openide.util.Lookup;

import java.util.*;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class AllCrossRingCutType implements CrossRingCutType {

    private final Map<Monosaccharide,Set<CutIndexes>>  typeCleavageMap = new HashMap<Monosaccharide,Set<CutIndexes>>();

    /**
     * Return an array of all possible Crossiring cleavage indexes for the specific input.
     * @param monosaccharide the monosaccharide to be fragment.
     * @return an array of all possible Crossring cleavage indexes for the specific input.
     */
    @Override
    public Set<CutIndexes> get(Monosaccharide monosaccharide) {

        Preconditions.checkNotNull(monosaccharide);
        if(typeCleavageMap.containsKey(monosaccharide)){
            return typeCleavageMap.get(monosaccharide);
        }

        return retriveCleavageFromMassCalculator(monosaccharide);

    }

    private Set<CutIndexes> retriveCleavageFromMassCalculator(Monosaccharide monosaccharide){

        Set<CutIndexes> allCutIndexes = new HashSet<CutIndexes>();
        GlycanMassCalculator glycanMassCalculator = Lookup.getDefault().lookup(GlycanMassCalculator.class);
        allCutIndexes.addAll(Arrays.asList(glycanMassCalculator.getAllInternalTypeCleavage(monosaccharide)));
        typeCleavageMap.put(monosaccharide, allCutIndexes);
        return allCutIndexes;
    }

}
