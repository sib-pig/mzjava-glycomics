package org.expasy.mzjava.glycomics.mol;

import java.util.List;

/**
 * @author Davide Alocci
 * @version sqrt -1
 **/

public interface SubstituentLookup {

    /**
     * Search for a substituent with the input name the list of known substituents.
     * <p>The method throw an IllegalStateException if there is no substituent with the input name in the list.</p>
     * @param name the name of the substituent
     * @return A substituent object with the specified name.
     */
    Substituent getNew(String name);

    /**
     * Return a list of the substituents specified in Substituent Lookup file.
     * @return A list of the known substituents
     */
    List<Substituent> values();
}
