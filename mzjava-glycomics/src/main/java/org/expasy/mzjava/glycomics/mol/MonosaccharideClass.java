package org.expasy.mzjava.glycomics.mol;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public enum MonosaccharideClass {

    dPen,
    Pen,
    dHex,
    ddHex,
    HexN,
    Bac,
    Hex,
    HexA,
    uHexA,
    MeHex,
    dHep,
    Hep,
    HexNAc,
    BacNAc,
    Kdo,
    Kdn,
    Ko,
    Neu,
    NeuAc,
    NeuGc,
    NeuAcLac,
    HexNA,
    GalNA,
    GlcNA,
    IdoNA,
    ManNA

}
