package org.expasy.mzjava.glycomics.mol;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;
import org.expasy.mzjava.utils.MixedRadixNtupleGenerator;

import java.util.*;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */

public class CutDescriptorGenerator {


    public final List<CutDescriptor> generateCutDescriptor(Glycan glycan, int glycosidicCut, int crossringCut){
        return generateCutDescriptor(glycan, glycosidicCut, crossringCut, new AllCrossRingCutType());
    }


    public final List<CutDescriptor> generateCutDescriptor(Glycan glycan, int glycosidicCut, int crossringCut, CrossRingCutType crossRingCutType){

        Preconditions.checkArgument(!glycan.isEmpty(),"Glycan size must be greater than zero.");
        Preconditions.checkNotNull(glycan);
        Preconditions.checkNotNull(crossRingCutType);

        List<CutDescriptor> cutDescriptorList = new ArrayList<>();

        Predicate<Set<SEdge>> edgeMaxSize = new MaxSizePredicate<>(glycosidicCut);
        Predicate<Set<GlycanNode>> nodeMaxSize = new MaxSizePredicate<>(crossringCut);

        Set<Set<SEdge>> edgePowerSet =  Sets.filter(this.powerSet(generateEdgeList(glycan)), edgeMaxSize);
        Set<Set<GlycanNode>> nodePowerSet = Sets.filter(this.powerSet(generateMonosaccharideList(glycan)),nodeMaxSize);

        for(Set<SEdge> edgeSet : edgePowerSet){
            for(Set<GlycanNode> nodeSet : nodePowerSet){
                pairAcceptor(edgeSet, nodeSet, crossRingCutType, cutDescriptorList);
            }
        }
        return cutDescriptorList;
    }



    private void pairAcceptor(Set<SEdge> edgeSet, Set<GlycanNode> nodeSet, CrossRingCutType crossRingCutType, List<CutDescriptor> cutDescriptorList){

        List<Set<CleavedMonosaccharide>> cutIndexesSetList = generateCleavedMonosaccharideSets(nodeSet,crossRingCutType);
        if(cutIndexesSetList.isEmpty()){
            CutDescriptor cutDescriptor = new CutDescriptor();
            cutDescriptor.addCleavedEdges(edgeSet);
            cutDescriptorList.add(cutDescriptor);
        } else {
            for (Set<CleavedMonosaccharide> cleavedMonosaccharideSet : cutIndexesSetList) {
                CutDescriptor cutDescriptor = new CutDescriptor(cleavedMonosaccharideSet,edgeSet);
                cutDescriptorList.add(cutDescriptor);
            }
        }
    }


    private <T> Set<Set<T>> powerSet(Set<T> originalSet) {

        Preconditions.checkNotNull(originalSet);
        Set<Set<T>> sets = new HashSet<>();
        if (originalSet.isEmpty()) {
            sets.add(new HashSet<T>());
            return sets;
        }

        List<T> list = new ArrayList<>(originalSet);
        T head = list.get(0);
        Set<T> rest = Sets.newIdentityHashSet();
        rest.addAll(list.subList(1, list.size()));
        for (Set<T> set : powerSet(rest)) {
            Set<T> newSet = Sets.newIdentityHashSet();
            newSet.add(head);
            newSet.addAll(set);
            sets.add(newSet);
            sets.add(set);
        }
        return sets;
    }


    private List<Set<CleavedMonosaccharide>> generateCleavedMonosaccharideSets(Set<GlycanNode> nodeSet, CrossRingCutType crossRingCutType){

        Map<Monosaccharide, List<CutIndexes>> initialMap = new IdentityHashMap<>();
        List<Integer> radixList = new ArrayList<>();
        for(GlycanNode node : nodeSet) {
            Monosaccharide monosaccharide;

            if (node instanceof Substituent) {
                throw new IllegalStateException("No CutDescriptor for Substituent");
            } else {
                monosaccharide = (Monosaccharide) node;
            }

            List<CutIndexes> allInternalTypeCleavege= new ArrayList<>(crossRingCutType.get(monosaccharide));
            radixList.add(allInternalTypeCleavege.size());
            initialMap.put(monosaccharide,allInternalTypeCleavege);
        }

        List<Set<CleavedMonosaccharide>> results = new ArrayList<>();
        cutIndexCombinationGenerator(initialMap, Ints.toArray(radixList), results);

        return results;
    }


    private void cutIndexCombinationGenerator(Map<Monosaccharide, List<CutIndexes>> monosaccharideMap, int[] radixArray, List<Set<CleavedMonosaccharide>> results){

        MixedRadixNtupleGenerator.NtupleContainer ntupleContainer = new MixedRadixNtupleGenerator.NtupleContainer();
        MixedRadixNtupleGenerator combinationGenerator = new MixedRadixNtupleGenerator(ntupleContainer);
        combinationGenerator.generate(radixArray);

        for(int[]comb : ntupleContainer.getNtuples()){
            if(comb.length != monosaccharideMap.keySet().size()){
                throw new IllegalStateException("Error CutIndexCombinationGenerator: the length of the monosaccharide map must be the same of the generated combination.");
            }
            Set<CleavedMonosaccharide> cleavedMonosaccharideSet = new HashSet<>();
            for( int i = 0; i <comb.length; i++){
                Monosaccharide actualMonosaccharide = (Monosaccharide) monosaccharideMap.keySet().toArray()[i];
                cleavedMonosaccharideSet.add(new CleavedMonosaccharide(actualMonosaccharide,monosaccharideMap.get(actualMonosaccharide).get(comb[i])));
            }
            results.add(cleavedMonosaccharideSet);
        }
    }



    private Set<GlycanNode> generateMonosaccharideList (Glycan glycan){

        final Set<GlycanNode> glycanNodes = Sets.newSetFromMap(new IdentityHashMap<GlycanNode, Boolean>());

        glycanNodes.add(glycan.root);
        glycan.forEachLinkage(SaccharideGraph.Traversal.BFS, new LinkageAcceptor() {
            @Override
            public void accept(Monosaccharide parent, Monosaccharide child, GlycosidicLinkage glycosidicLinkage) {
                glycanNodes.add(child);
            }
        });

        return glycanNodes;
    }

    private Set<SEdge> generateEdgeList (Glycan glycan){

        Set<SEdge> glycanEdges = Sets.newSetFromMap(new IdentityHashMap<SEdge, Boolean>());

        for(SEdge edge : glycan.copyEdgeList()){
            if(edge instanceof GlycosidicEdge){
                glycanEdges.add(edge);
            }
        }
        return glycanEdges;
    }

    private static class MaxSizePredicate<E> implements Predicate<Set<E>> {

        int maxSize;

        public MaxSizePredicate (int maxSize){
            this.maxSize = maxSize;
        }

        @Override
        public boolean apply(Set<E> sEdges) {
            return sEdges.size() <= maxSize;
        }
    }
}
