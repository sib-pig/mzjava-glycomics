
package org.expasy.mzjava.glycomics.mol;

/**
 * This class contains the two carbons where the carbon ring is cut.
 * If you want to specify a glycosidic cut (where the ring remains intact) please set the first and the second carbons at -1.
 * Each object contains the two index that represent where the monosaccharide ring is cut.
 * @author Davide Alocci
 * @version sqrt -1.
 */

public class CutIndexes {

    private final int firstIndex;
    private final int secondIndex;

    public CutIndexes(int firstIndex, int secondIndex){

        if(firstIndex != -1 && secondIndex != -1 ){
            checkCarbons(firstIndex,secondIndex);
        }
        this.firstIndex = firstIndex;
        this.secondIndex = secondIndex;
    }


    /**
     * Sanity check for the input.
     */

    private void checkCarbons(int firstIndex, int secondIndex){

        if(secondIndex <= firstIndex){
            throw new IllegalArgumentException("The first index must be lower than the second index");
        }
        if(firstIndex > 5 || firstIndex < 0){
            throw new IllegalArgumentException("The first index must be between 0 and 6");
        }
        if(secondIndex > 5 || secondIndex < 0){
            throw new IllegalArgumentException("The second index must be between 0 and 6");
        }
    }


    /**
     * Return the first index of the cleavage
     * @return the first index of the cleavage
     */

    public int getFirstIndex() {
        return firstIndex;
    }


    /**
     * Return the second index of the cleavage
     * @return the second index of the cleavage
     */

    public int getSecondIndex() {
        return secondIndex;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CutIndexes)) return false;

        CutIndexes that = (CutIndexes) o;

        return firstIndex == that.firstIndex && secondIndex == that.secondIndex;
    }

    @Override
    public int hashCode() {
        int result = firstIndex;
        result = 31 * result + secondIndex;
        return result;
    }

    @Override
    public String toString() {
        return "CutIndexes{" +
                "firstIndex=" + firstIndex +
                ", secondIndex=" + secondIndex +
                '}';
    }

    /**
     * Check if the cut is a glycosidic cut
     * @return <code>true</code> if it is a glycosidic cut.
     *         <code>false</code> otherwise
     */
    public boolean isGlycosidicCut(){
        if(firstIndex == -1 && secondIndex == -1){
            return true;
        }
        return false;
    }
}

