package org.expasy.mzjava.glycomics.mol;

import com.google.common.base.Preconditions;
import org.openide.util.Lookup;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class CleavedMonosaccharide {

    private final Monosaccharide monosaccharide;
    private final CutIndexes cutIndexes;


    public CleavedMonosaccharide(Monosaccharide monosaccharide, CutIndexes cutIndexes){

        Preconditions.checkNotNull(monosaccharide);
        Preconditions.checkNotNull(cutIndexes);
        if( checkCutIndex(monosaccharide,cutIndexes)){
            this.monosaccharide = monosaccharide;
            this.cutIndexes = cutIndexes;
        } else {
            throw new IllegalArgumentException("CutIndex not found for this monosaccharide.");
        }
    }


    public Monosaccharide getMonosaccharide() {
        return monosaccharide;
    }

    public CutIndexes getCutIndexes() {
        return cutIndexes;
    }

    private boolean checkCutIndex(Monosaccharide monosaccharide, CutIndexes cutIndexes){
        GlycanMassCalculator glycanMassCalculator = Lookup.getDefault().lookup(GlycanMassCalculator.class);
        Set<CutIndexes> cutIndexesSet = new HashSet<CutIndexes>();
        cutIndexesSet.addAll(Arrays.asList(glycanMassCalculator.getAllInternalTypeCleavage(monosaccharide)));
        return cutIndexesSet.contains(cutIndexes);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CleavedMonosaccharide)) return false;

        CleavedMonosaccharide that = (CleavedMonosaccharide) o;

        if (!cutIndexes.equals(that.cutIndexes)) return false;
        if (!(monosaccharide == that.monosaccharide)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = monosaccharide.hashCode();
        result = 31 * result + cutIndexes.hashCode();
        return result;
    }
}
