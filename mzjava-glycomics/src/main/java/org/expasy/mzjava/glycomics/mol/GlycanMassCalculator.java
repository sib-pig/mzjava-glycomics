package org.expasy.mzjava.glycomics.mol;

import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */

public interface GlycanMassCalculator {

    /**
     * Return the value of mass that the glycan have lost during the crossring fragmentation depending on the ion type.
     * @param ionType the type of ion taken into account.
     * @param anomericCarbon the carbon linked to the parent.
     * @param cleavedMonosaccaride the cleaved monosaccharide in the crossring fragment.
     * @param cutIndexes if is a x or a ion, this paramenter specify the positions of the cut in the monosaccharide ring.
     * @return the value of mass.
     */
    double getCrossRingLossMass(IonType ionType, int anomericCarbon, Monosaccharide cleavedMonosaccaride, CutIndexes cutIndexes);


    /**
     * Return the value of mass that the glycan have lost during the glycosidic fragmentation depending on the ion type.
     * @param ionType the type of ion taken into account.
     * @return the value of mass.
     */
    double getGlycosidicDeltaMass(IonType ionType);

    /**
     * Return the composition that the glycan have lost during the glycosidic fragmentation depending on the ion type.
     * @param ionType the type of ion taken into account.
     * @return the composition.
     */
    Composition getGlycosidicDeltaComposition(IonType ionType);

    /**
     * Return the value of the mass over charge. Specify FragmentType.INTACT if you are dealing with precursor.
     * Before call this method be sure that the molecularMass it is correct.
     * @param molecularMass the molecular mass.
     * @param charge the charge of which calculate.
     * @param fragmentType the fragment type of interest.
     * @return the value of mass over charge.
     */
    double calculateMz(double molecularMass, int charge, FragmentType fragmentType);

    /**
     * Return all possible type of internal cleavage for the monosaccharide of interest
     * Before call this method be sure that the molecularMass it is correct.
     * @param monosaccharide the monosaccharide of interest.
     * @return an array
     */
    CutIndexes[] getAllInternalTypeCleavage(Monosaccharide monosaccharide);

}
