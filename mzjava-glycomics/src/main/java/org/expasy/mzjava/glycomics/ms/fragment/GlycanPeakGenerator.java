package org.expasy.mzjava.glycomics.ms.fragment;

import org.expasy.mzjava.core.ms.spectrum.AnnotatedPeak;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.glycomics.mol.Glycan;
import org.expasy.mzjava.glycomics.mol.GlycanFragment;
import org.expasy.mzjava.glycomics.ms.spectrum.GlycanFragAnnotation;

import java.util.List;
import java.util.Set;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public interface GlycanPeakGenerator<A extends PeakAnnotation> {

    /**
     * Generate peaks that are observed for the fragment at the supplied charge states.
     * The generated peaks are to be added to peaks.
     *
     * @param precursor the glycan that this fragment was derived from.
     * @param fragment the fragment  of the glycan.
     * @param charges the charges for which to generate peaks.
     * @return the peaks list that was passed in.
     */

    List<AnnotatedPeak<A>> generatePeaks(Glycan precursor,GlycanFragment fragment, int[] charges, List<AnnotatedPeak<GlycanFragAnnotation>> annotatedPeaks);

    /**
     * Return the set of fragment type used in the peak generator.
     * @return the fragment type used in the peak generator.
     */
    Set<FragmentType> getFragmentTypes();

}
