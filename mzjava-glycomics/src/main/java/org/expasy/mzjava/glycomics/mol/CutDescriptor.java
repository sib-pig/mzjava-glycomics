package org.expasy.mzjava.glycomics.mol;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;

import java.util.*;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class CutDescriptor {

    private final Set<CleavedMonosaccharide> cleavedMonosaccharides = new HashSet<CleavedMonosaccharide>();
    private final Set<SEdge> cleavedEdges = Sets.newIdentityHashSet();

    public CutDescriptor() {
    }

    public CutDescriptor(Set<CleavedMonosaccharide> cleavedMonosaccharidesSet, Set<SEdge> cleavedEdges) {
        Preconditions.checkNotNull(cleavedEdges);
        Preconditions.checkNotNull(cleavedMonosaccharidesSet);
        this.cleavedEdges.addAll(cleavedEdges);
        this.cleavedMonosaccharides.addAll(cleavedMonosaccharidesSet);
    }


    /**
     * Add a cleaved edge to the CutDescriptor.
     *
     * @param edge cleaved edge.
     */
    public void addCleavedEdge(SEdge edge) {
        Preconditions.checkNotNull(edge);
        cleavedEdges.add(edge);
    }

    /**
     * Add a set of cleaved edge to the CutDescriptor.
     *
     * @param edges cleaved edge.
     */
    public void addCleavedEdges(Set<SEdge> edges) {
        Preconditions.checkNotNull(edges);
        cleavedEdges.addAll(edges);
    }

    /**
     * Add a cleaved monosaccharide edge to the CutDescriptor.
     *
     * @param monosaccharide cleaved monosaccharide.
     * @param cutIndexes     type of cut for the monosaccharide.
     */
    public void addCleavedMonosaccharide(Monosaccharide monosaccharide, CutIndexes cutIndexes) {
        Preconditions.checkNotNull(monosaccharide);
        Preconditions.checkNotNull(cutIndexes);
        cleavedMonosaccharides.add(new CleavedMonosaccharide(monosaccharide, cutIndexes));
    }

    /**
     * Add a collection of cleaved monosaccharides edge to the CutDescriptor.
     * It use an IdentityHashMap because the software use the references to check the equaly.
     *
     * @param cleavedMonosaccharides a set of CleavedMonosaccharides.
     */

    public void addCleavedMonosaccharides(Set<CleavedMonosaccharide> cleavedMonosaccharides) {
        Preconditions.checkNotNull(cleavedMonosaccharides);
        this.cleavedMonosaccharides.addAll(cleavedMonosaccharides);
    }

    /**
     * Check if the CutDescriptor contains the specified monosaccharide.
     * Return <code>true</code> if the monosaccharide is already present in the CutDescriptor.
     *
     * @param monosaccharide a monosaccharide
     * @return <code>true</code> if the monosaccharide is in the CutDescriptor.
     * <code>false</code> otherwise
     */
    public boolean contains(Monosaccharide monosaccharide) {
        Preconditions.checkNotNull(monosaccharide);
        for (CleavedMonosaccharide cleavedMonosaccharide : cleavedMonosaccharides) {
            if (cleavedMonosaccharide.getMonosaccharide() == monosaccharide) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if the CutDescriptor contains the specified edge.
     * Return <code>true</code> if the edge is already present in the CutDescriptor.
     *
     * @param edge a edge
     * @return <code>true</code> if the edge is in the CutDescriptor.
     * <code>false</code> otherwise
     */
    public boolean contains(SEdge edge) {
        Preconditions.checkNotNull(edge);
        return cleavedEdges.contains(edge);
    }

    /**
     * Return the CutIndex related with the specified monosaccharide.
     *
     * @param monosaccharide a monosaccharide.
     * @return CutIndex related with the specified monosaccharide.
     */
    public CutIndexes getCutIndexesForMonosaccharide(Monosaccharide monosaccharide) {
        Preconditions.checkNotNull(monosaccharide);
        for (CleavedMonosaccharide cleavedMonosaccharide : cleavedMonosaccharides) {
            if (cleavedMonosaccharide.getMonosaccharide() == monosaccharide) {
                return cleavedMonosaccharide.getCutIndexes();
            }
        }
        throw new IllegalArgumentException("Monosaccharide not found for this CutIndex");
    }

    /**
     * Return <code>true</code> if the CutDescriptor is empty.
     *
     * @return <code>true</code> if the CutDescriptor is empty.
     * <code>false</code> otherwise
     */
    public boolean isEmpty() {
        if (cleavedMonosaccharides.isEmpty() && cleavedEdges.isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * Return the actual number of cuts in the CutDescriptor.
     *
     * @return a integer
     */
    public int getNumberOfCut() {
        return cleavedEdges.size() + cleavedMonosaccharides.size();
    }


    /**
     * Return a set of all the cleaved monosaccharides in the CutDescriptor.
     *
     * @return a set of monosaccharides.
     */
    public Set<Monosaccharide> getCleavedNodes() {

        Set<Monosaccharide> resultSet = Sets.newIdentityHashSet();
        for (CleavedMonosaccharide cleavedMonosaccharide : cleavedMonosaccharides) {
            resultSet.add(cleavedMonosaccharide.getMonosaccharide());
        }
        return resultSet;
    }

    /**
     * Return a set of all the cleaved monosaccharides in the CutDescriptor with their CutIndex.
     *
     * @return a set of CleavedMonosaccharides.
     */
    public Set<CleavedMonosaccharide> getCleavedMonosaccharides() {
        return Collections.unmodifiableSet(cleavedMonosaccharides);
    }


    /**
     * Return a set of all the cleaved edges in the CutDescriptor.
     *
     * @return a set of edges.
     */
    public Set<SEdge> getCleavedEdges() {

        return Collections.unmodifiableSet(cleavedEdges);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CutDescriptor)) return false;

        CutDescriptor that = (CutDescriptor) o;

        if (!cleavedEdges.equals(that.cleavedEdges)) return false;
        return cleavedMonosaccharides.equals(that.cleavedMonosaccharides);

    }


    @Override
    public int hashCode() {
        int result = cleavedMonosaccharides.hashCode();
        result = 31 * result + cleavedEdges.hashCode();
        return result;
    }
}
