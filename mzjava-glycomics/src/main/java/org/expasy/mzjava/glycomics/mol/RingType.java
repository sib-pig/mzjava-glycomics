package org.expasy.mzjava.glycomics.mol;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */

public enum RingType {
    furanose,
    pyranose,
    open;


    public static RingType getRingType(char c){

        switch (c) {
            case 'p' : return RingType.pyranose;
            case 'f' : return RingType.furanose;
            case 'o' : return RingType.open;
            default: break;
        }

        throw new IllegalArgumentException("Cannot resolve ring type for input: "+c);
    }
}
