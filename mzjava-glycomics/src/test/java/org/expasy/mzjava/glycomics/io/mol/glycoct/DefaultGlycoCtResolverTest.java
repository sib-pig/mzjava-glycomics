package org.expasy.mzjava.glycomics.io.mol.glycoct;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.glycomics.mol.Anomericity;
import org.expasy.mzjava.glycomics.mol.MonosaccharideLookup;
import org.expasy.mzjava.glycomics.mol.SubstituentLookup;
import org.junit.After;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.openide.util.Lookup;

import java.io.File;
import java.io.FileWriter;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * @author julien
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class DefaultGlycoCtResolverTest {

    private final MonosaccharideLookup monosaccharideLookup = Lookup.getDefault().lookup(MonosaccharideLookup.class);

    @Test
    public void testGetMonosaccharide() throws Exception {

        GlycoCtResolver resolver =  Lookup.getDefault().lookup(GlycoCtResolver.class);

        assertThat(resolver.getMonosaccharide("dgal-HEX-1:5"), is(monosaccharideLookup.getNew("Gal")));
        assertThat(resolver.getMonosaccharide("lgal-HEX-1:5|6:d"), is(monosaccharideLookup.getNew("Fuc")));
        assertThat(resolver.getMonosaccharide("HEX-1:5|0:d"), is(monosaccharideLookup.getNew("DeoxyHex")));
        assertThat(resolver.getMonosaccharide("HEX-1:5"), is(monosaccharideLookup.getNew("Hex")));
        assertThat(resolver.getMonosaccharide("HEX-1:5|6:a"), is(monosaccharideLookup.getNew("HexA")));
        assertThat(resolver.getMonosaccharide("dgro-dgal-NON-2:6|1:a|2:keto|3:d"), is(monosaccharideLookup.getNew("Kdn")));
        assertNotEquals(resolver.getMonosaccharide("HEX-1:5"), monosaccharideLookup.getNew("DeoxyHex"));
        assertFalse(resolver.getMonosaccharide("dgal-HEX-1:5").equals(monosaccharideLookup.getNew("Fuc")));
    }

    @Test(expected = IllegalStateException.class)
    public void testInvalidGetMonosaccharide() throws Exception {

        GlycoCtResolver resolver = Lookup.getDefault().lookup(GlycoCtResolver.class);
        resolver.getMonosaccharide("A");
    }

    @Test
    public void testGetSubstituent() throws Exception {
        GlycoCtResolver resolver = Lookup.getDefault().lookup(GlycoCtResolver.class);
        SubstituentLookup lookup = Lookup.getDefault().lookup(SubstituentLookup.class);

        assertThat(resolver.getSubstituent("sulfate"), is(lookup.getNew("Sulfate")));
        assertThat(resolver.getSubstituent("(r)-pyruvate"), is(lookup.getNew("RPyruvate")));
        assertThat(resolver.getSubstituent("n-dimethyl"), is(lookup.getNew("NDimethyl")));
        assertThat(resolver.getSubstituent("iodo"), is(lookup.getNew("Iodo")));
    }

    @Test(expected = IllegalStateException.class)
    public void testInvalidGetSubstituent() throws Exception {

        GlycoCtResolver resolver = Lookup.getDefault().lookup(GlycoCtResolver.class);
        resolver.getSubstituent("A");
    }

    @Test
    public void testGetAnomericity() throws Exception {
        GlycoCtResolver resolver = Lookup.getDefault().lookup(GlycoCtResolver.class);
        assertEquals(resolver.getAnomericity("a"), Optional.of(Anomericity.alpha));
        assertEquals(resolver.getAnomericity("b"), Optional.of(Anomericity.beta));
        assertEquals(resolver.getAnomericity("o"), Optional.of(Anomericity.open));
    }

    @Test(expected = IllegalStateException.class)
    public void testInvalidAnomericity() throws Exception {
        GlycoCtResolver resolver = Lookup.getDefault().lookup(GlycoCtResolver.class);
        resolver.getAnomericity("A");
    }

    @Test
    public void testGetLinkageComposition() throws Exception {
        GlycoCtResolver resolver = Lookup.getDefault().lookup(GlycoCtResolver.class);
        assertEquals(resolver.getLinkageComposition("o"), Optional.of(Composition.parseComposition("H-1")));
        assertEquals(resolver.getLinkageComposition("h"), Optional.of(Composition.parseComposition("H-1")));
        assertEquals(resolver.getLinkageComposition("d"), Optional.of(Composition.parseComposition("(OH)-1")));
        assertEquals(resolver.getLinkageComposition("r"), Optional.of(Composition.parseComposition("H-1")));
        assertEquals(resolver.getLinkageComposition("s"), Optional.of(Composition.parseComposition("H-1")));
    }

    @Test(expected = IllegalStateException.class)
    public void testInvalidGetLinkageComposition() throws Exception {
        GlycoCtResolver resolver = Lookup.getDefault().lookup(GlycoCtResolver.class);
        resolver.getLinkageComposition("A");
    }


    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void testSetPropertyFile() throws Exception {

        File temporaryfile = folder.newFile("testMonosaccharideGlycoCt.json");

        FileWriter fileWriter = new FileWriter(temporaryfile);
        fileWriter.write("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Man\",\n" +
                "\t\t\"glycoCTformat\": \"alt-TEST-1:5\"\n" +
                "\t}]");

        fileWriter.close();
        System.setProperty(DefaultGlycoCtResolver.PROPERTY_GLYCOCT_MONOSACCHARIDES,temporaryfile.getPath());
        GlycoCtResolver resolver = new DefaultGlycoCtResolver();
        assertThat(resolver.getMonosaccharide("alt-TEST-1:5"), is(monosaccharideLookup.getNew("Man")));
        System.clearProperty(DefaultGlycoCtResolver.PROPERTY_GLYCOCT_MONOSACCHARIDES);
        folder.delete();
    }

    @Test(expected = IllegalStateException.class)
    public void testSetPropertyFileException() throws Exception {

        File temporaryfile = folder.newFile("testMonosaccharideGlycoCt.json");

        FileWriter fileWriter = new FileWriter(temporaryfile);
        fileWriter.write("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Man\",\n" +
                "\t\t\"glycoCTformat\": \"alt-TEST-1:5\"\n" +
                "\t}]");

        fileWriter.close();
        System.setProperty(DefaultGlycoCtResolver.PROPERTY_GLYCOCT_MONOSACCHARIDES,temporaryfile.getPath());
        GlycoCtResolver resolver = new DefaultGlycoCtResolver();
        assertThat(resolver.getMonosaccharide("ciao"), is(monosaccharideLookup.getNew("Man")));
        System.clearProperty(DefaultGlycoCtResolver.PROPERTY_GLYCOCT_MONOSACCHARIDES);
        folder.delete();
    }
    @After
    public void closeResource(){
        System.clearProperty(DefaultGlycoCtResolver.PROPERTY_GLYCOCT_MONOSACCHARIDES);
        folder.delete();
    }



    @Test
    public void testSetPropertyFile2() throws Exception {
        System.setProperty(DefaultGlycoCtResolver.PROPERTY_GLYCOCT_MONOSACCHARIDES,getClass().getResource("testGlycoCt.json").getPath());
        GlycoCtResolver resolver = new DefaultGlycoCtResolver();
        Assert.assertEquals(resolver.getMonosaccharide("GlycoCT"), monosaccharideLookup.getNew("Hex"));
        System.clearProperty(DefaultGlycoCtResolver.PROPERTY_GLYCOCT_MONOSACCHARIDES);
    }

}
