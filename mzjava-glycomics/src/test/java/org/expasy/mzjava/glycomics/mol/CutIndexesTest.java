package org.expasy.mzjava.glycomics.mol;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class CutIndexesTest {
    @Test(expected = IllegalArgumentException.class)
    public void wrongOrderCarbon() throws Exception{
        CutIndexes test = new CutIndexes(2,1);
        Assert.assertEquals(test.getSecondIndex(),1);
    }
    @Test(expected = IllegalArgumentException.class)
    public void sameCarbon() throws Exception {
        CutIndexes test = new CutIndexes(1,1);
        Assert.assertEquals(test.getSecondIndex(),1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void wrongCarbonIndex() throws Exception {
        CutIndexes test = new CutIndexes(8,1);
        Assert.assertEquals(test.getSecondIndex(),1);
    }
    @Test(expected = IllegalArgumentException.class)
    public void wrongCarbonIndex2() throws Exception {
        CutIndexes test = new CutIndexes(-9,1);
        Assert.assertEquals(test.getSecondIndex(),1);
    }

    @Test
    public void testGetFirstCarbon() throws Exception {
        CutIndexes test = new CutIndexes(1,5);
        Assert.assertEquals(test.getFirstIndex(),1);
    }

    @Test
    public void testGetSecondCarbon() throws Exception {
        CutIndexes test = new CutIndexes(1,5);
        Assert.assertEquals(test.getSecondIndex(),5);

    }

    @Test
    public void testEquals() throws Exception{

        CutIndexes test1 = new CutIndexes(1,2);
        CutIndexes test2 = new CutIndexes(1,2);
        Assert.assertTrue(test1.equals(test2) && test2.equals(test1));
        Assert.assertTrue(test2.hashCode() == test1.hashCode());
        Assert.assertFalse(test2.equals(new CutIndexes(1,3)));
    }

    @Test
    public void testToString() throws Exception{
        CutIndexes test = new CutIndexes(1,5);
        Assert.assertEquals(test.toString(),"CutIndexes{firstIndex=1, secondIndex=5}");

    }

    @Test
    public void testGlycosidic() throws Exception{
        CutIndexes test = new CutIndexes(-1,-1);
        CutIndexes test2 = new CutIndexes(3,4);
        Assert.assertTrue(test.isGlycosidicCut());
        Assert.assertFalse(test2.isGlycosidicCut());
    }

}
