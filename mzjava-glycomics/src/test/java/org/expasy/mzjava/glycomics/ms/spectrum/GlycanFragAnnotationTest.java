package org.expasy.mzjava.glycomics.ms.spectrum;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.mol.Mass;
import org.expasy.mzjava.core.mol.MassCalculator;
import org.expasy.mzjava.core.mol.NumericMass;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.glycomics.mol.*;
import org.junit.Assert;
import org.junit.Test;
import org.openide.util.Lookup;


/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class GlycanFragAnnotationTest {


    private final MonosaccharideLookup monosaccharideLookup = Lookup.getDefault().lookup(MonosaccharideLookup.class);



   /* @Test
    public void testGetIonType() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(MonosaccharideLookup.getNew("Glc"), Optional.of(Anomericity.alpha), "Sugar1");
        Monosaccharide node1 = builder.add(MonosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(MonosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node3 = builder.add(MonosaccharideLookup.getNew("Gal"), node2, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan glycan = builder.build();

        GlycanFragment.Builder fragmentBuilder = new GlycanFragment.Builder();
        fragmentBuilder.setRoot(node0, FragmentType.REVERSE);
        fragmentBuilder.add(node1, node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        fragmentBuilder.addCleavedMonosaccharide(node2, node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")),new CutIndexes(0,3));
        GlycanFragment fragment = fragmentBuilder.build();

        GlycanFragAnnotation.FragBuilder fragBuilder = new GlycanFragAnnotation.FragBuilder(1,fragment);
        fragBuilder.addMonsaccarideCut(node2,new CutIndexes(-1,-1), Arrays.asList(IonType.y));
        GlycanFragAnnotation glycanFragAnnotation = fragBuilder.build();

        GlycanFragAnnotation.FragBuilder fragBuilder1 = new GlycanFragAnnotation.FragBuilder(1,fragment);
        fragBuilder1.addMonsaccarideCut(node2,new CutIndexes(-1,-1), Arrays.asList(IonType.z));
        fragBuilder1.addIsotopeComposition(Composition.parseComposition("O"));
        GlycanFragAnnotation glycanFragAnnotation2 = fragBuilder1.build();

        Assert.assertTrue(glycanFragAnnotation.getIonType(node2).contains(IonType.y));
        Assert.assertEquals(glycanFragAnnotation.getIonType(node2).size(), 1);
        Assert.assertFalse(glycanFragAnnotation.getIonType(node2).contains(IonType.z));
        Assert.assertEquals(glycanFragAnnotation2.getIonType(node2).size(),1);
        Assert.assertFalse(glycanFragAnnotation2.getIonType(node2).contains(IonType.x));
        Assert.assertTrue(glycanFragAnnotation2.getIonType(node2).contains(IonType.z));

    }  */

    @Test
    public void testHasNeutralLoss() throws Exception {


        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.of(Anomericity.alpha), "Sugar1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node3 = builder.add(monosaccharideLookup.getNew("Gal"), node2, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan glycan = builder.build();

        GlycanFragment.Builder fragmentBuilder = new GlycanFragment.Builder();
        fragmentBuilder.setRoot(node0, FragmentType.REVERSE);
        fragmentBuilder.add(node1, node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        fragmentBuilder.addCleavedMonosaccharide(node2, node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")),new CutIndexes(0,3),IonType.x);
        GlycanFragment fragment = fragmentBuilder.build();

        GlycanFragAnnotation.FragBuilder fragBuilder = new GlycanFragAnnotation.FragBuilder(1,fragment);
        GlycanFragAnnotation glycanFragAnnotation = fragBuilder.build();

        GlycanFragAnnotation.FragBuilder fragBuilder1 = new GlycanFragAnnotation.FragBuilder(1,fragment);
        fragBuilder1.setNeutralLoss(new NumericMass(1));
        fragBuilder1.addIsotopeComposition(Composition.parseComposition("O"));
        GlycanFragAnnotation glycanFragAnnotation2 = fragBuilder1.build();

        Assert.assertFalse(glycanFragAnnotation.hasNeutralLoss());
        Assert.assertTrue(glycanFragAnnotation2.hasNeutralLoss());



    }

    @Test
    public void testGetNeutralLoss() throws Exception {


        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.of(Anomericity.alpha), "Sugar1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node3 = builder.add(monosaccharideLookup.getNew("Gal"), node2, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan glycan = builder.build();

        GlycanFragment.Builder fragmentBuilder = new GlycanFragment.Builder();
        fragmentBuilder.setRoot(node0, FragmentType.REVERSE);
        fragmentBuilder.add(node1, node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        fragmentBuilder.addCleavedMonosaccharide(node2, node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")),new CutIndexes(0,3),IonType.x);
        GlycanFragment fragment = fragmentBuilder.build();

        GlycanFragAnnotation.FragBuilder fragBuilder = new GlycanFragAnnotation.FragBuilder(1,fragment);
        GlycanFragAnnotation glycanFragAnnotation = fragBuilder.build();

        GlycanFragAnnotation.FragBuilder fragBuilder1 = new GlycanFragAnnotation.FragBuilder(1,fragment);
        fragBuilder1.setNeutralLoss(new NumericMass(10.11));
        fragBuilder1.addIsotopeComposition(Composition.parseComposition("O"));
        GlycanFragAnnotation glycanFragAnnotation2 = fragBuilder1.build();

        Assert.assertTrue(glycanFragAnnotation.getNeutralLoss().equals(Mass.ZERO));
        Assert.assertTrue(glycanFragAnnotation2.getNeutralLoss().equals(new NumericMass(10.11)));


    }

    @Test
    public void testGetFragment() throws Exception {


        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.of(Anomericity.alpha), "Sugar1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node3 = builder.add(monosaccharideLookup.getNew("Gal"), node2, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan glycan = builder.build();

        GlycanFragment.Builder fragmentBuilder = new GlycanFragment.Builder();
        fragmentBuilder.setRoot(node0, FragmentType.REVERSE);
        fragmentBuilder.add(node1, node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        fragmentBuilder.addCleavedMonosaccharide(node2, node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")),new CutIndexes(0,3),IonType.x);
        GlycanFragment fragment = fragmentBuilder.build();

        GlycanFragAnnotation.FragBuilder fragBuilder = new GlycanFragAnnotation.FragBuilder(1,fragment);
        GlycanFragAnnotation glycanFragAnnotation = fragBuilder.build();

        Assert.assertTrue(fragment.equals(glycanFragAnnotation.getFragment()));
    }

    @Test
    public void testGetIsotopeCount() throws Exception {


        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.of(Anomericity.alpha), "Sugar1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node3 = builder.add(monosaccharideLookup.getNew("Gal"), node2, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan glycan = builder.build();

        GlycanFragment.Builder fragmentBuilder = new GlycanFragment.Builder();
        fragmentBuilder.setRoot(node0, FragmentType.REVERSE);
        fragmentBuilder.add(node1, node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        fragmentBuilder.addCleavedMonosaccharide(node2, node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")),new CutIndexes(0,3),IonType.x);
        GlycanFragment fragment = fragmentBuilder.build();

        GlycanFragAnnotation.FragBuilder fragBuilder = new GlycanFragAnnotation.FragBuilder(1,fragment);
        GlycanFragAnnotation glycanFragAnnotation = fragBuilder.build();

        GlycanFragAnnotation.FragBuilder fragBuilder1 = new GlycanFragAnnotation.FragBuilder(1,fragment);
        fragBuilder1.setNeutralLoss(new NumericMass(10.11));
        fragBuilder1.addIsotopeComposition(Composition.parseComposition("OH"));
        GlycanFragAnnotation glycanFragAnnotation2 = fragBuilder1.build();

        Assert.assertEquals(0, glycanFragAnnotation.getIsotopeCount());
        Assert.assertEquals(2, glycanFragAnnotation2.getIsotopeCount());
        Assert.assertNotEquals(1, glycanFragAnnotation2.getIsotopeCount());


    }

    @Test
    public void testGetIsotopeComposition() throws Exception {


        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.of(Anomericity.alpha), "Sugar1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node3 = builder.add(monosaccharideLookup.getNew("Gal"), node2, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan glycan = builder.build();

        GlycanFragment.Builder fragmentBuilder = new GlycanFragment.Builder();
        fragmentBuilder.setRoot(node0, FragmentType.REVERSE);
        fragmentBuilder.add(node1, node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        fragmentBuilder.addCleavedMonosaccharide(node2, node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")),new CutIndexes(0,3),IonType.x);
        GlycanFragment fragment = fragmentBuilder.build();

        GlycanFragAnnotation.FragBuilder fragBuilder = new GlycanFragAnnotation.FragBuilder(1,fragment);
        fragBuilder.addIsotopeComposition(Composition.parseComposition("O"));
        GlycanFragAnnotation glycanFragAnnotation = fragBuilder.build();

        GlycanFragAnnotation.FragBuilder fragBuilder1 = new GlycanFragAnnotation.FragBuilder(1,fragment);
        fragBuilder1.setNeutralLoss(new NumericMass(10.11));
        fragBuilder1.addIsotopeComposition(Composition.parseComposition("OH"));
        GlycanFragAnnotation glycanFragAnnotation2 = fragBuilder1.build();

        Assert.assertEquals(Composition.parseComposition("O"), glycanFragAnnotation.getIsotopeComposition());
        Assert.assertEquals(Composition.parseComposition("HO"),glycanFragAnnotation2.getIsotopeComposition());

    }

    @Test
    public void testGetCharge() throws Exception {


        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.of(Anomericity.alpha), "Sugar1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node3 = builder.add(monosaccharideLookup.getNew("Gal"), node2, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan glycan = builder.build();

        GlycanFragment.Builder fragmentBuilder = new GlycanFragment.Builder();
        fragmentBuilder.setRoot(node0, FragmentType.REVERSE);
        fragmentBuilder.add(node1, node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        fragmentBuilder.addCleavedMonosaccharide(node2, node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")),new CutIndexes(0,3),IonType.x);
        GlycanFragment fragment = fragmentBuilder.build();

        GlycanFragAnnotation.FragBuilder fragBuilder = new GlycanFragAnnotation.FragBuilder(1,fragment);
        CutIndexes glycosidicCut = new CutIndexes(-1,-1);
        fragBuilder.addIsotopeComposition(Composition.parseComposition("O"));
        GlycanFragAnnotation glycanFragAnnotation = fragBuilder.build();

        GlycanFragAnnotation.FragBuilder fragBuilder1 = new GlycanFragAnnotation.FragBuilder(2,fragment);
        fragBuilder1.setNeutralLoss(new NumericMass(10.11));
        fragBuilder1.addIsotopeComposition(Composition.parseComposition("OH"));
        GlycanFragAnnotation glycanFragAnnotation2 = fragBuilder1.build();

        Assert.assertEquals(1,glycanFragAnnotation.getCharge());
        Assert.assertNotEquals(2, glycanFragAnnotation.getCharge());
        Assert.assertEquals(2,glycanFragAnnotation2.getCharge());
        Assert.assertNotEquals(1,glycanFragAnnotation2.getCharge());


    }

    @Test
    public void testGetTheoreticalMz() throws Exception {

        GlycanMassCalculator glycanMassCalculator = Lookup.getDefault().lookup(GlycanMassCalculator.class);

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.of(Anomericity.alpha), "Sugar1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node3 = builder.add(monosaccharideLookup.getNew("Gal"), node2, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan glycan = builder.build();

        GlycanFragment.Builder fragmentBuilder = new GlycanFragment.Builder();
        fragmentBuilder.setRoot(node0, FragmentType.REVERSE);
        fragmentBuilder.add(node1, node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        fragmentBuilder.addCleavedMonosaccharide(node2, node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")),new CutIndexes(0,3),IonType.x);
        GlycanFragment fragment = fragmentBuilder.build();

        GlycanFragAnnotation.FragBuilder fragBuilder = new GlycanFragAnnotation.FragBuilder(1,fragment);
        GlycanFragAnnotation glycanFragAnnotation = fragBuilder.build();

        GlycanFragAnnotation.FragBuilder fragBuilder1 = new GlycanFragAnnotation.FragBuilder(2,fragment);
        fragBuilder1.setNeutralLoss(new NumericMass(10.11));
        fragBuilder1.addIsotopeComposition(Composition.parseComposition("O[17]"));
        GlycanFragAnnotation glycanFragAnnotation2 = fragBuilder1.build();

        Assert.assertEquals(glycanMassCalculator.calculateMz(fragment.calculateMolecularMass(), 1, FragmentType.REVERSE),glycanFragAnnotation.calcTheoreticalMz(),0.00001);
        Assert.assertEquals((glycanMassCalculator.calculateMz(fragment.calculateMolecularMass(), 2, FragmentType.REVERSE))+ ((10.11+ MassCalculator.calcIsotopeDelta(Composition.parseComposition("O[17]")))/2) ,glycanFragAnnotation2.calcTheoreticalMz(),0.00001);

    }




    @Test
    public void testEquals() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.of(Anomericity.alpha), "Sugar1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node3 = builder.add(monosaccharideLookup.getNew("Gal"), node2, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan glycan = builder.build();

        GlycanFragment.Builder fragmentBuilder = new GlycanFragment.Builder();
        fragmentBuilder.setRoot(node0, FragmentType.REVERSE);
        fragmentBuilder.add(node1, node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        fragmentBuilder.addCleavedMonosaccharide(node2, node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")),new CutIndexes(0,3),IonType.x);
        GlycanFragment fragment = fragmentBuilder.build();

        GlycanFragAnnotation.FragBuilder fragBuilder = new GlycanFragAnnotation.FragBuilder(1,fragment);
        fragBuilder.addIsotopeComposition(Composition.parseComposition("O"));
        GlycanFragAnnotation glycanFragAnnotation = fragBuilder.build();

        GlycanFragAnnotation.FragBuilder fragBuilder1 = new GlycanFragAnnotation.FragBuilder(2,fragment);
        fragBuilder1.setNeutralLoss(new NumericMass(10.11));
        fragBuilder1.addIsotopeComposition(Composition.parseComposition("O"));
        GlycanFragAnnotation glycanFragAnnotation2 = fragBuilder1.build();

        GlycanFragAnnotation.FragBuilder fragBuilder3 = new GlycanFragAnnotation.FragBuilder(1,fragment);
        fragBuilder3.addIsotopeComposition(Composition.parseComposition("O"));
        GlycanFragAnnotation glycanFragAnnotation3 = fragBuilder.build();


        Assert.assertFalse(glycanFragAnnotation.equals(glycanFragAnnotation2));
        Assert.assertTrue(glycanFragAnnotation.equals(glycanFragAnnotation3));
        Assert.assertFalse(glycanFragAnnotation3.equals(glycanFragAnnotation2));

    }

    @Test
    public void testCopy() throws Exception {


        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.of(Anomericity.alpha), "Sugar1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node3 = builder.add(monosaccharideLookup.getNew("Gal"), node2, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan glycan = builder.build();

        GlycanFragment.Builder fragmentBuilder = new GlycanFragment.Builder();
        fragmentBuilder.setRoot(node0, FragmentType.REVERSE);
        fragmentBuilder.add(node1, node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        fragmentBuilder.addCleavedMonosaccharide(node2, node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")),new CutIndexes(0,3),IonType.x);
        GlycanFragment fragment = fragmentBuilder.build();

        GlycanFragAnnotation.FragBuilder fragBuilder = new GlycanFragAnnotation.FragBuilder(1,fragment);
        fragBuilder.addIsotopeComposition(Composition.parseComposition("O"));
        GlycanFragAnnotation glycanFragAnnotation = fragBuilder.build();

        PeakAnnotation glycanFragAnnotationCopy = glycanFragAnnotation.copy();
        Assert.assertEquals(glycanFragAnnotation,glycanFragAnnotationCopy);

    }


    /*@Test
    public void testToString() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(MonosaccharideLookup.getNew("Glc"), Optional.of(Anomericity.alpha), "Sugar1");
        Monosaccharide node1 = builder.add(MonosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(MonosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node3 = builder.add(MonosaccharideLookup.getNew("Gal"), node2, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan glycan = builder.build();

        GlycanFragment.Builder fragmentBuilder = new GlycanFragment.Builder();
        fragmentBuilder.setRoot(node0, FragmentType.REVERSE);
        fragmentBuilder.add(node1, node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        fragmentBuilder.addCleavedMonosaccharide(node2, node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")),new CutIndexes(0,3),IonType.x);
        GlycanFragment fragment = fragmentBuilder.build();

        GlycanFragAnnotation.FragBuilder fragBuilder = new GlycanFragAnnotation.FragBuilder(1,fragment);
        fragBuilder.addIsotopeComposition(Composition.parseComposition("O"));
        GlycanFragAnnotation glycanFragAnnotation = fragBuilder.build();

        Assert.assertEquals("GlycanFragAnnotation{[ Monosaccharide=Gal,GlycosidicCut,IonType=[z];], isotopeComposition=O, charge=1, neutralLoss=0.0}",glycanFragAnnotation.toString());
    }*/

}
