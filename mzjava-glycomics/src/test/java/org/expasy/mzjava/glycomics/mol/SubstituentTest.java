/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.glycomics.mol;

import org.expasy.mzjava.core.mol.Composition;
import org.junit.Assert;
import org.junit.Test;
import org.openide.util.Lookup;

/**
 * Created with IntelliJ IDEA.
 * User: jmarieth
 * Date: 10/31/13
 * Time: 3:12 PM
 */
public class SubstituentTest {


    private final SubstituentLookup lookup = Lookup.getDefault().lookup(SubstituentLookup.class);

    @Test(expected = NullPointerException.class)
    public void testNullName() throws Exception{
        new Substituent(null,Composition.parseComposition("H"));
    }

    @Test(expected = NullPointerException.class)
    public void testNullComp() throws Exception{
        new Substituent("Ciao",null);
    }

    @Test(expected = NullPointerException.class)
    public void testNullCopy() throws Exception{
        new Substituent(null);
    }


    @Test
    public void testGetName() throws Exception {

        Substituent bromo = lookup.getNew("Bromo");
        Assert.assertEquals(bromo.getName(), "Bromo");
        Substituent nAcetyl = lookup.getNew("NAcetyl");
        Assert.assertEquals(nAcetyl.getName(), "NAcetyl");
        Substituent hydroxymethyl = new Substituent("Hydroxymethyl", Composition.parseComposition("CH2OH"));
        Assert.assertEquals(hydroxymethyl.getName(), "Hydroxymethyl");
        Assert.assertNotEquals(hydroxymethyl.getName(), "Hydroxymethylsdasa");
        Assert.assertNotEquals(nAcetyl.getName(), "sdasda");
    }

    @Test
    public void testGetComposition() throws Exception {

        Substituent bromo = lookup.getNew("Bromo");
        Assert.assertEquals(bromo.getComposition(), Composition.parseComposition("Br"));
        Substituent nAcetyl = lookup.getNew("NAcetyl");
        Assert.assertEquals(nAcetyl.getComposition(),  Composition.parseComposition("NHCOCH3"));
        Substituent hydroxymethyl = new Substituent("hydroxymethyl", Composition.parseComposition("CH2OH"));
        Assert.assertEquals(hydroxymethyl.getComposition(), Composition.parseComposition("CH2OH"));
        Assert.assertNotEquals(hydroxymethyl.getComposition(), Composition.parseComposition("COH"));
        Assert.assertNotEquals(nAcetyl.getComposition(),Composition.parseComposition("H") );
    }

    @Test
    public void testGetMolecularMass() throws Exception {
        Substituent bromo = lookup.getNew("Bromo");
        Substituent nAcetyl = lookup.getNew("NAcetyl");
        Assert.assertEquals(bromo.getMolecularMass(),78.918338,0.0000000001);
        Assert.assertEquals(nAcetyl.getMolecularMass(),58.029288754999996,0.0000000001);
    }


    @Test
    public void testEquals_Symmetric() {
        //Inside Lookup there is a copy constructor so the object are different
        Substituent Iodo = lookup.getNew("Iodo");
        Substituent Iodo2  = lookup.getNew("Iodo");
        Assert.assertTrue(Iodo.equals(Iodo2) && Iodo2.equals(Iodo));
        Assert.assertTrue(Iodo.hashCode() == Iodo2.hashCode());
    }

    @Test
    public void testEquals() throws Exception{
        Substituent bromo = lookup.getNew("Bromo");
        Substituent bromo2 = new Substituent("Bromo", Composition.parseComposition("Br"));
        Substituent nAcetyl = lookup.getNew("NAcetyl");
        Assert.assertEquals(bromo,bromo2);
        Assert.assertNotEquals(bromo,nAcetyl);
    }

    @Test
    public void testToString() throws Exception{
        Substituent bromo = lookup.getNew("Bromo");
        Assert.assertEquals("Substituent{name='Bromo', composition=Br}",bromo.toString());
    }

}
