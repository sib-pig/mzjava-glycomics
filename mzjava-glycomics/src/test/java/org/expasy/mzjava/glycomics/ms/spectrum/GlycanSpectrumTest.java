package org.expasy.mzjava.glycomics.ms.spectrum;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.peaklist.PeakProcessorChain;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.IdentityPeakProcessor;
import org.expasy.mzjava.glycomics.mol.*;
import org.junit.Assert;
import org.junit.Test;
import org.openide.util.Lookup;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class GlycanSpectrumTest {

    private final SubstituentLookup substituentLookup = Lookup.getDefault().lookup(SubstituentLookup.class);
    private final MonosaccharideLookup monosaccharideLookup = Lookup.getDefault().lookup(MonosaccharideLookup.class);


    @Test
    public void testGetGlycan() throws Exception {
        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Fuc"), node0, new GlycosidicLinkage(Anomericity.alpha, 1, 4, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar = builder.build();

        GlycanSpectrum glycanSpectrum = new GlycanSpectrum(sugar,1);
        Assert.assertEquals(sugar, glycanSpectrum.getGlycan());
    }

    @Test
    public void testGetCharge() throws Exception {
        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Fuc"), node0, new GlycosidicLinkage(Anomericity.alpha, 1, 4, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar = builder.build();

        GlycanSpectrum glycanSpectrum = new GlycanSpectrum(sugar,1, PeakList.Precision.FLOAT);
        Assert.assertEquals(1, glycanSpectrum.getCharge());
    }

    @Test
    public void testCopy() throws Exception {
        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Fuc"), node0, new GlycosidicLinkage(Anomericity.alpha, 1, 4, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar = builder.build();
        GlycanSpectrum glycanSpectrum = new GlycanSpectrum(sugar,1, 1, 1.0, PeakList.Precision.DOUBLE_CONSTANT);
        GlycanSpectrum copy = glycanSpectrum.copy(new IdentityPeakProcessor<GlycanFragAnnotation>());

        Assert.assertEquals(glycanSpectrum,copy);
    }

    @Test
    public void testCopy2() throws Exception {
        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Fuc"), node0, new GlycosidicLinkage(Anomericity.alpha, 1, 4, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar = builder.build();
        GlycanSpectrum glycanSpectrum = new GlycanSpectrum(sugar,1, 1, 1.0, PeakList.Precision.DOUBLE_CONSTANT);
        GlycanSpectrum copy = glycanSpectrum.copy(new PeakProcessorChain<GlycanFragAnnotation>(new IdentityPeakProcessor<GlycanFragAnnotation>()));

        Assert.assertEquals(glycanSpectrum,copy);
    }



    @Test
    public void testEquals() throws Exception {
        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Fuc"), node0, new GlycosidicLinkage(Anomericity.alpha, 1, 4, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar = builder.build();
        GlycanSpectrum glycanSpectrum = new GlycanSpectrum(sugar,1, 1, 1.0, PeakList.Precision.DOUBLE_CONSTANT);
        GlycanSpectrum glycanSpectrum2 = new GlycanSpectrum(sugar,1, 1, 1.0, PeakList.Precision.DOUBLE_CONSTANT);
        Assert.assertTrue(glycanSpectrum.equals(glycanSpectrum2));
        Assert.assertTrue(glycanSpectrum2.equals(glycanSpectrum));

        Glycan.Builder builder1 = new Glycan.Builder();
        Monosaccharide node1_0 = builder1.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1_1 = builder1.add(monosaccharideLookup.getNew("Man"), node1_0, new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node1_2 = builder1.add(monosaccharideLookup.getNew("Fuc"), node1_0, new GlycosidicLinkage(Anomericity.alpha, 1, 4, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar1 = builder1.build();

        GlycanSpectrum glycanSpectrum3 = new GlycanSpectrum(sugar1,1, 1, 1.0, PeakList.Precision.DOUBLE_CONSTANT);
        Assert.assertTrue(glycanSpectrum.equals(glycanSpectrum3));
        Assert.assertTrue(glycanSpectrum3.equals(glycanSpectrum));


        Glycan.Builder builder3 = new Glycan.Builder();
        Monosaccharide node3_0 = builder3.setRoot(monosaccharideLookup.getNew("Gal"), Optional.<Anomericity>absent(), "Id_3");
        Substituent sub3_0 = builder3.add(substituentLookup.getNew("NAcetyl"), node3_0, new SubstituentLinkage(2, Composition.parseComposition("O-1H-1")));
        Monosaccharide node3_1 = builder3.add(monosaccharideLookup.getNew("Glc"), node3_0, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.of(Composition.parseComposition("O-1H-1")), Optional.of(Composition.parseComposition("H-1"))));
        Substituent sub3_1 = builder3.add(substituentLookup.getNew("NAcetyl"), node3_1, new SubstituentLinkage(2, Composition.parseComposition("O-1H-1")));
        Monosaccharide node3_2 = builder3.add(monosaccharideLookup.getNew("Gal"), node3_0, new GlycosidicLinkage(Anomericity.beta, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node3_3 = builder3.add(monosaccharideLookup.getNew("Fuc"), node3_2, new GlycosidicLinkage(Anomericity.alpha, 1, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar2 = builder3.build();

        GlycanSpectrum glycanSpectrum4 = new GlycanSpectrum(sugar2,1, 1, 1.0, PeakList.Precision.DOUBLE_CONSTANT);
        Assert.assertFalse(glycanSpectrum4.equals(glycanSpectrum));
        Assert.assertFalse(glycanSpectrum.equals(glycanSpectrum4));
    }

    @Test
    public void testHashCode() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Fuc"), node0, new GlycosidicLinkage(Anomericity.alpha, 1, 4, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar = builder.build();

        Glycan.Builder builder1 = new Glycan.Builder();
        Monosaccharide node1_0 = builder1.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1_1 = builder1.add(monosaccharideLookup.getNew("Man"), node1_0, new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node1_2 = builder1.add(monosaccharideLookup.getNew("Fuc"), node1_0, new GlycosidicLinkage(Anomericity.alpha, 1, 4, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar1 = builder1.build();

        Assert.assertEquals(sugar.hashCode(),sugar1.hashCode());
        Assert.assertEquals(sugar.hashCode(),sugar.hashCode());
    }

}
