/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.glycomics.mol;

import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class IdentityEdgeMultimapTest {

    /**
     * <pre>
     * A-"1-2"-A
     *       /
     *   "3-2"
     *   /
     * A
     * </pre>
     *
     * @throws Exception
     */
    @SuppressWarnings("RedundantStringConstructorCall") //need to use new String so that I get three objects
    @Test
    public void testIdentity() throws Exception {

        String node1 = new String("A");
        String node2 = new String("A");
        String node3 = new String("A");

        Assert.assertNotSame(node1, node2);
        Assert.assertEquals(node1, node2);

        IdentityEdgeMultimap<String, String, String> edgeMultimap = new IdentityEdgeMultimap<String, String, String>(2);
        String value = "1-2";
        edgeMultimap.put(node1, node2, value);
        edgeMultimap.put(node3, node2, "3-2");

        Assert.assertEquals(value, edgeMultimap.get(node1, node2));
        Assert.assertEquals("3-2", edgeMultimap.get(node3, node2));
        Assert.assertEquals(null, edgeMultimap.get(node2, node3));

        Assert.assertEquals(Collections.singleton(node2), edgeMultimap.getChildren(node1));
        Assert.assertEquals(Collections.singletonMap(node2, value), edgeMultimap.getOutEdges(node1));
    }
}
