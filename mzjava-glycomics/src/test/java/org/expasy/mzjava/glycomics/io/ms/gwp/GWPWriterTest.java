/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.glycomics.io.ms.gwp;

import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.junit.Assert;
import org.junit.Test;

import java.io.StringWriter;
import java.util.UUID;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class GWPWriterTest {

    @Test
    public void testEmpty() throws Exception {

        StringWriter out = new StringWriter();
        GWPWriter writer = new GWPWriter(out, PeakList.Precision.FLOAT);

        writer.close();
        out.close();

        Assert.assertEquals("<?xml version=\"1.0\"?>\n" +
                "<GlycanWorkspace>\n" +
                "  <Configuration/>\n" +
                "</GlycanWorkspace>\n", out.toString());
    }

    @Test
    public void testSpectrum() throws Exception {

        MsnSpectrum spectrum = new MsnSpectrum(PeakList.Precision.FLOAT);
        spectrum.setId(UUID.fromString("40cd1a36-0dc0-457d-8ed7-4d4286ce2871"));
        spectrum.getPrecursor().setMzAndCharge(733, 145237, 1);
        spectrum.setMsLevel(2);
        spectrum.add(220.03, 20.4);
        spectrum.add(222.07, 15.6);

        StringWriter out = new StringWriter();
        GWPWriter writer = new GWPWriter(out, PeakList.Precision.FLOAT);
        writer.write("40cd1a36-0dc0-457d-8ed7-4d4286ce2871", spectrum,
                "redEnd--?b1D-GlcNAc,p--4b1D-GlcNAc,p--4b1D-Man,p(--3a1D-Man,p(--2b1D-GlcNAc,p)--4b1D-GlcNAc,p)--6a1D-Man,p--2b1D-GlcNAc,p$MONO,Und,-2H,0,redEnd",
                "SPECTRUM - MS&#13;\n" +
                        "            080612_SF_C0870_Run6.RAW&#13;\n" +
                        "            ITMS - p ESI d w Full ms2 739.65@cid35.00 [190.00-1490.00]&#13;\n" +
                        "            Scan #: 2430-2534&#13;\n" +
                        "            RT: 23.09-24.14&#13;\n" +
                        "            AV: 25&#13;\n" +
                        "            m/z&#13;", 2
        );
        writer.close();
        out.close();

        Assert.assertEquals("<?xml version=\"1.0\"?>\n" +
                        "<GlycanWorkspace>\n" +
                        "  <Configuration/>\n" +
                        "  <Scan name=\"40cd1a36-0dc0-457d-8ed7-4d4286ce2871\" precursor_mz=\"733.0\" is_msms=\"true\">\n" +
                        "    <Structures>\n" +
                        "      <Glycan structure=\"redEnd--?b1D-GlcNAc,p--4b1D-GlcNAc,p--4b1D-Man,p(--3a1D-Man,p(--2b1D-GlcNAc,p)--4b1D-GlcNAc,p)--6a1D-Man,p--2b1D-GlcNAc,p$MONO,Und,-2H,0,redEnd\"/>\n" +
                        "    </Structures>\n" +
                        "    <Fragments/>\n" +
                        "    <PeakList>\n" +
                        "      <Peak mz_ratio=\"220.030\" intensity=\"20.400\"/>\n" +
                        "      <Peak mz_ratio=\"222.070\" intensity=\"15.600\"/>\n" +
                        "    </PeakList>\n" +
                        "    <Notes>SPECTRUM - MS&amp;#13;\n" +
                        "            080612_SF_C0870_Run6.RAW&amp;#13;\n" +
                        "            ITMS - p ESI d w Full ms2 739.65@cid35.00 [190.00-1490.00]&amp;#13;\n" +
                        "            Scan #: 2430-2534&amp;#13;\n" +
                        "            RT: 23.09-24.14&amp;#13;\n" +
                        "            AV: 25&amp;#13;\n" +
                        "            m/z&amp;#13;</Notes>\n" +
                        "  </Scan>\n" +
                        "</GlycanWorkspace>\n",
                out.toString()
        );
    }
}
