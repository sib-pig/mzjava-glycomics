/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.glycomics.mol;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.glycomics.io.mol.glycoct.GlycoCTReader;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.openide.util.Lookup;

import java.util.*;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class GlycanTest {

    private final SubstituentLookup substituentLookup = Lookup.getDefault().lookup(SubstituentLookup.class);
    private final MonosaccharideLookup monosaccharideLookup = Lookup.getDefault().lookup(MonosaccharideLookup.class);


    @Test
    public void testGetDatabaseIdentifier() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();

        String dbId = "Id_1";

        builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), dbId);
        Glycan sugar = builder.build();

        assertThat(sugar.getDatabaseIdentifier(), is(dbId));
    }

    /**
     * <pre>
     * node1
     *      \
     *       node0-|
     *      /
     * node2
     * </pre>
     *
     * @throws Exception
     */
    @Test
    public void testAdd() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        String dbId = "Id_1";

        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), dbId);
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));

        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        assertThat(builder.getId(node2), is(2));

        Glycan glycan = builder.build();

        assertThat(glycan.hasHyperEdges(), is(false));
        assertEquals(3, glycan.size());

        assertEquals(Optional.<Anomericity>absent(), glycan.getReducingEndAnomericity());

        assertThat((Monosaccharide) glycan.getNode(0), is(monosaccharideLookup.getNew("Glc")));
        assertThat((Monosaccharide) glycan.getNode(1), is(monosaccharideLookup.getNew("Man")));
        assertThat((Monosaccharide) glycan.getNode(2), is(monosaccharideLookup.getNew("Gal")));

        assertThat(glycan.getGlycoLinkage(node0, node1).get(), is(new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1"))));
        assertThat(glycan.getGlycoLinkage(node0, node2).get(), is(new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1"))));

        Set<GlycanNode> children = new HashSet<GlycanNode>();
        children.add(node1);
        children.add(node2);
        Set<Monosaccharide> saccarideChildren = new HashSet<Monosaccharide>();
        saccarideChildren.add(node1);
        saccarideChildren.add(node2);

        assertThat(glycan.getChildren(node0), is(children));
        assertThat(glycan.getChildren(node1), is(Collections.<GlycanNode>emptySet()));
        assertThat(glycan.getMonosaccharideChildren(node0), is(saccarideChildren));
        assertThat(glycan.getSubstituentChildren(node0), is(Collections.<Substituent>emptySet()));

        assertThat(glycan.getDatabaseIdentifier(), is(dbId));

    }

    @Test
    public void testAddHyperEdge() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();

        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");

        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));

        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        assertThat(builder.getId(node2), is(2));
        assertThat(node2, is(monosaccharideLookup.getNew("Gal")));

        Monosaccharide node3 = builder.add(monosaccharideLookup.getNew("Gal"), Arrays.asList(node0, node1, node2), GlycosidicLinkage.UNKNOWN);

        Glycan sugar = builder.build();

        assertThat((Monosaccharide)sugar.getNode(0), is(monosaccharideLookup.getNew("Glc")));
        assertThat((Monosaccharide) sugar.getNode(1), is(monosaccharideLookup.getNew("Man")));
        assertThat((Monosaccharide) sugar.getNode(2), is(monosaccharideLookup.getNew("Gal")));
        assertThat((Monosaccharide) sugar.getNode(3), is(monosaccharideLookup.getNew("Gal")));

        assertThat(sugar.getGlycoLinkage(node0, node1).get(), is(new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1"))));
        assertThat(sugar.getGlycoLinkage(node0, node2).get(), is(new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1"))));

        assertThat(sugar.getGlycoLinkage(node0, node3).get(), is(GlycosidicLinkage.UNKNOWN));
        assertThat(sugar.getGlycoLinkage(node1, node3).get(), is(GlycosidicLinkage.UNKNOWN));
        assertThat(sugar.getGlycoLinkage(node2, node3).get(), is(GlycosidicLinkage.UNKNOWN));
        assertThat(sugar.getGlycoLinkage(node3, node0), is(Optional.<GlycosidicLinkage>absent()));
        assertThat(sugar.getGlycoLinkage(node1, node2), is(Optional.<GlycosidicLinkage>absent()));

        NodeSet<GlycanNode> children = NodeSet.<GlycanNode>of(node1, node2, node3);
        assertThat(sugar.getChildren(node0), is(children));
        assertThat(sugar.getChildren(node1), is(Collections.<GlycanNode>singleton(node3)));
        assertThat(sugar.getChildren(node2), is(Collections.<GlycanNode>singleton(node3)));
        assertThat(sugar.getChildren(node3), is(Collections.<GlycanNode>emptySet()));
    }

    /**
     * <pre>
     *       node1
     *            \
     *             node0-|
     *            /
     * node3-node2
     * </pre>
     *
     * @throws Exception
     */
    @Test
    public void testAddSubstituent() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();

        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));

        Substituent node3 = builder.add( substituentLookup.getNew("Sulfate"), node2, SubstituentLinkage.UNKNOWN);

        Glycan glycan = builder.build();

        assertThat(glycan.getGlycoLinkage(node0, node3), is(Optional.<GlycosidicLinkage>absent()));
        assertThat(glycan.getSubstituentLinkage(node2, node3).get(), is(SubstituentLinkage.UNKNOWN));
        assertThat(glycan.getSubstituentLinkage(node0, node1), is(Optional.<SubstituentLinkage>absent()));

        assertThat(glycan.getChildren(node2), is(Collections.<GlycanNode>singleton(node3)));
        assertThat(glycan.getSubstituentChildren(node2), is(Collections.<Substituent>singleton(node3)));
    }

    @Test
    public void testAddSubstituentHyperEdge() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();

        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));

        Substituent node3 = builder.add(substituentLookup.getNew("Sulfate"), Arrays.asList(node1, node2), SubstituentLinkage.UNKNOWN);

        Glycan sugar = builder.build();

        assertThat(sugar.getGlycoLinkage(node0, node3), is(Optional.<GlycosidicLinkage>absent()));
        assertThat(sugar.getSubstituentLinkage(node2, node3).get(), is(SubstituentLinkage.UNKNOWN));
        assertThat(sugar.getSubstituentLinkage(node0, node1), is(Optional.<SubstituentLinkage>absent()));

        assertThat(sugar.getChildren(node1), is(Collections.<GlycanNode>singleton(node3)));
        assertThat(sugar.getChildren(node2), is(Collections.<GlycanNode>singleton(node3)));
    }

    @Test(expected = IllegalStateException.class)
    public void testBuildTwice() {

        Glycan.Builder builder = new Glycan.Builder();
        builder.setRoot(monosaccharideLookup.getNew("Hex"), Optional.<Anomericity>absent(), "Id_1");
        assertNotNull(builder.build());
        builder.build();
    }

    @Test(expected = IllegalStateException.class)
    public void testBuildEmpty() {

        Glycan.Builder builder = new Glycan.Builder();
        builder.build();
    }

    @Test(expected = NullPointerException.class)
    public void testIllegalBuilderArgument1() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        builder.setRoot(null, Optional.of(Anomericity.beta), "Id_1");
    }

    @Test(expected = NullPointerException.class)
    public void testIllegalBuilderArgument2() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        builder.setRoot(monosaccharideLookup.getNew("Gal"), null, "Id_1");
    }

    @Test
    public void testAddMonomerAndSubstituent() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();

        Monosaccharide root = builder.setRoot(monosaccharideLookup.getNew("Man"), Optional.<Anomericity>absent(), "Id_1");

        Monosaccharide galNac = builder.add(monosaccharideLookup.getNew("Gal"),  substituentLookup.getNew("NAcetyl"), root, GlycosidicLinkage.UNKNOWN, SubstituentLinkage.UNKNOWN);

        Glycan sugar = builder.build();
        assertEquals(Collections.<GlycanNode>singleton(galNac), sugar.getChildren(root));
        Set<GlycanNode> galChildren = sugar.getChildren(galNac);
        assertThat(galChildren.size(), is(1));

        GlycanNode nac = galChildren.iterator().next();
        assertThat((Substituent) nac, is(substituentLookup.getNew("NAcetyl")));
    }

    @Test(expected = IllegalStateException.class)
    public void testSetRootTwice() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();

        builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
    }

    @Test
    public void testBuilderGetNode() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();

        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));

        Substituent node3 = builder.add( substituentLookup.getNew("Sulfate"), node2, SubstituentLinkage.UNKNOWN);

        assertSame(node0, builder.getNode(0));
        assertSame(node1, builder.getNode(1));
        assertSame(node2, builder.getNode(2));
        assertSame(node3, builder.getNode(3));
    }

    /**
     * <pre>
     *             Man(1)
     *                   \
     *                    Glc(0)-|
     *                   /
     * Sulfate(3)--Gal(2)
     *
     * </pre>
     * @throws Exception
     */
    @Test
    public void testGetEdgeList() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();

        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));

        Substituent node3 = builder.add( substituentLookup.getNew("Sulfate"), node2, SubstituentLinkage.UNKNOWN);

        Glycan sugar = builder.build();

        List<SEdge> edgeList = sugar.copyEdgeList();

        assertThat(edgeList.size(), is(3));

        SEdge edge0 = edgeList.get(0);

        assertEquals(Collections.singletonList(node0), edge0.getParents());
        assertEquals(node1, edge0.getChild());

        SEdge edge1 = edgeList.get(1);
        assertEquals(Collections.singletonList(node0), edge1.getParents());
        assertEquals(node2, edge1.getChild());

        SEdge edge2 = edgeList.get(2);
        assertEquals(Collections.singletonList(node2), edge2.getParents());
        assertEquals(node3, edge2.getChild());
    }

    /**
     * <pre>
     *             Man(1)
     *             }      \
     *      Hex(4)}        Glc(0)-|
     *             }     /
     * Sulfate(3)--Gal(2)
     *
     * </pre>
     * @throws Exception
     */
    @Test
    public void testGetEdgeListWithHyperEdge() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();

        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));

        Substituent node3 = builder.add( substituentLookup.getNew("Sulfate"), node2, SubstituentLinkage.UNKNOWN);
        Monosaccharide node4 = builder.add(monosaccharideLookup.getNew("Hex"), Arrays.asList(node1, node2), GlycosidicLinkage.UNKNOWN);

        Glycan sugar = builder.build();

        assertThat(sugar.hasHyperEdges(), is(true));

        List<SEdge> edgeList = sugar.copyEdgeList();

        assertThat(edgeList.size(), is(4));

        SEdge edge0 = edgeList.get(0);

        assertEquals(Collections.singletonList(node0), edge0.getParents());
        assertEquals(node1, edge0.getChild());

        SEdge edge1 = edgeList.get(1);
        assertEquals(Collections.singletonList(node0), edge1.getParents());
        assertEquals(node2, edge1.getChild());

        SEdge edge2 = edgeList.get(2);
        assertEquals(Collections.singletonList(node2), edge2.getParents());
        assertEquals(node3, edge2.getChild());

        SEdge edge3 = edgeList.get(3);
        assertEquals(Arrays.asList(node1, node2), edge3.getParents());
        assertEquals(node4, edge3.getChild());
    }


    @Test
    public void testContainNode(){
        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide glc = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "A");
        Monosaccharide gal = builder.add(monosaccharideLookup.getNew("Gal"), glc, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));
        Monosaccharide man = builder.add(monosaccharideLookup.getNew("Man"), glc, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));
        Monosaccharide fuc = builder.add(monosaccharideLookup.getNew("Fuc"), man, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));

        Glycan glycan = builder.build();

        Assert.assertTrue(glycan.containsNode(glycan.getRoot()));
        Assert.assertTrue(glycan.containsNode(glc));
        Assert.assertTrue(glycan.containsNode(gal));
        Assert.assertTrue(glycan.containsNode(man));
        Assert.assertTrue(glycan.containsNode(fuc));
    }
    /**
     * <pre>
     *         Gal(1)
     *              \
     *              Glc(0)-|
     *             /
     * Fuc(3)--Man(2)
     *
     * </pre>
     * @throws Exception
     */
    @Test @Ignore
    public void testFragment() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide glc = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "A");
        Monosaccharide gal = builder.add(monosaccharideLookup.getNew("Gal"), glc, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));
        Monosaccharide man = builder.add(monosaccharideLookup.getNew("Man"), glc, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));
        Monosaccharide fuc = builder.add(monosaccharideLookup.getNew("Fuc"), man, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));

        Glycan glycan = builder.build();

        Set<FragmentType> fragmentTypeSet = new HashSet<FragmentType>();
        fragmentTypeSet.add(FragmentType.FORWARD);
        fragmentTypeSet.add(FragmentType.INTACT);
        fragmentTypeSet.add(FragmentType.REVERSE);

        CutDescriptorGenerator cutDescriptorGenerator = new CutDescriptorGenerator();

        List<CutDescriptor> descriptors = cutDescriptorGenerator.generateCutDescriptor(glycan, 1, 1);
        List<GlycanFragment> fragments = new ArrayList<GlycanFragment>();
        for(CutDescriptor descriptor : descriptors){
            fragments.addAll(glycan.getFragment(descriptor));
        }


    }

    @Test @Ignore
    public void testFragmentMultipleCut() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide glc = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "A");
        Monosaccharide gal = builder.add(monosaccharideLookup.getNew("Gal"), glc, new GlycosidicLinkage(Anomericity.alpha, 1, 6, Composition.parseComposition("OH"), Composition.parseComposition("H")));
        Monosaccharide man = builder.add(monosaccharideLookup.getNew("Man"), glc, new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("OH"), Composition.parseComposition("H")));
        Monosaccharide fuc = builder.add(monosaccharideLookup.getNew("Fuc"), man, new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("OH"), Composition.parseComposition("H")));

        Glycan glycan = builder.build();

        Set<FragmentType> fragmentTypeSet = new HashSet<FragmentType>();
        fragmentTypeSet.add(FragmentType.FORWARD);
        fragmentTypeSet.add(FragmentType.INTACT);
        fragmentTypeSet.add(FragmentType.REVERSE);
        fragmentTypeSet.add(FragmentType.INTERNAL);

        CutDescriptorGenerator cutDescriptorGenerator = new CutDescriptorGenerator();

        List<CutDescriptor> descriptors = cutDescriptorGenerator.generateCutDescriptor(glycan, 1, 1);
        Map<CutDescriptor,List<GlycanFragment>> test = new HashMap<CutDescriptor, List<GlycanFragment>>();
        List<GlycanFragment> fragments = new ArrayList<GlycanFragment>();
        for(CutDescriptor descriptor : descriptors){
            fragments.addAll(glycan.getFragment(descriptor));
            test.put(descriptor,glycan.getFragment(descriptor));
        }

        Assert.assertEquals(10,descriptors.size());
    }


    /**
     * <pre>
     *         Gal(1)
     *              \
     *              Glc(0)-|
     *             /
     * Fuc(3)--Man(2)
     *
     * </pre>
     * This test produce no internal fragment just because there is only one cut.
     * @throws Exception
     */
    @Test @Ignore
    public void testFragmentWithInternal() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide glc = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "A");
        Monosaccharide gal = builder.add(monosaccharideLookup.getNew("Gal"), glc, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));
        Monosaccharide man = builder.add(monosaccharideLookup.getNew("Man"), glc, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));
        Monosaccharide fuc = builder.add(monosaccharideLookup.getNew("Fuc"), man, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));

        Glycan glycan = builder.build();

        Set<FragmentType> fragmentTypeSet = new HashSet<FragmentType>();
        fragmentTypeSet.add(FragmentType.FORWARD);
        fragmentTypeSet.add(FragmentType.INTACT);
        fragmentTypeSet.add(FragmentType.REVERSE);
        fragmentTypeSet.add(FragmentType.INTERNAL);

        CutDescriptorGenerator cutDescriptorGenerator = new CutDescriptorGenerator();

        List<CutDescriptor> descriptors = cutDescriptorGenerator.generateCutDescriptor(glycan, 1, 0);
        List<GlycanFragment> fragments = new ArrayList<GlycanFragment>();
        for(CutDescriptor descriptor : descriptors){
            fragments.addAll(glycan.getFragment(descriptor));
        }

    }

    @Test  @Ignore
    public void testFragmentWithInternalMultipleCut() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide glc = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "A");
        Monosaccharide gal = builder.add(monosaccharideLookup.getNew("Gal"), glc, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));
        Monosaccharide man = builder.add(monosaccharideLookup.getNew("Man"), glc, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));
        Monosaccharide fuc = builder.add(monosaccharideLookup.getNew("Fuc"), man, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));

        Glycan glycan = builder.build();

        Set<FragmentType> fragmentTypeSet = new HashSet<FragmentType>();
        fragmentTypeSet.add(FragmentType.FORWARD);
        fragmentTypeSet.add(FragmentType.INTACT);
        fragmentTypeSet.add(FragmentType.REVERSE);
        fragmentTypeSet.add(FragmentType.INTERNAL);

        CutDescriptorGenerator cutDescriptorGenerator = new CutDescriptorGenerator();

        List<CutDescriptor> descriptors = cutDescriptorGenerator.generateCutDescriptor(glycan, 2, 1);
        List<GlycanFragment> fragments = new ArrayList<GlycanFragment>();
        for(CutDescriptor descriptor : descriptors){
            fragments.addAll(glycan.getFragment(descriptor));
        }
        Assert.assertEquals(9, fragments.size());
    }



    /**
     * <pre>
     *                  Gal(1)
     *                        \
     *                        Glc(0)-|
     *                       /
     * Sulf(4)--Fuc(3)--Man(2)
     *
     * </pre>
     * @throws Exception
     */
    @Test @Ignore
    public void testFragmentWithSubstituent() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide glc = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "A");
        Monosaccharide gal = builder.add(monosaccharideLookup.getNew("Gal"), glc, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));
        Monosaccharide man = builder.add(monosaccharideLookup.getNew("Man"), glc, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));
        Monosaccharide fuc = builder.add(monosaccharideLookup.getNew("Fuc"), man, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));
        builder.add(substituentLookup.getNew("Sulfate"), fuc, new SubstituentLinkage(Optional.<Integer>absent(), Optional.of(Composition.parseComposition("H-1"))));

        Glycan glycan = builder.build();

        Set<FragmentType> fragmentTypeSet = new HashSet<FragmentType>();
        fragmentTypeSet.add(FragmentType.FORWARD);
        fragmentTypeSet.add(FragmentType.INTACT);
        fragmentTypeSet.add(FragmentType.REVERSE);

        CutDescriptorGenerator cutDescriptorGenerator = new CutDescriptorGenerator();

        List<CutDescriptor> descriptors = cutDescriptorGenerator.generateCutDescriptor(glycan, 2, 1);
        List<GlycanFragment> fragments = new ArrayList<GlycanFragment>();
        for(CutDescriptor descriptor : descriptors){
            fragments.addAll(glycan.getFragment(descriptor));
        }

        Assert.assertEquals(9, fragments.size());




       /* todo fix it
       GlycanFragmentOLD precursor = fragments.get(0);
        Assert.assertEquals(glc, precursor.getRoot());
        Assert.assertEquals(glc, precursor.getCleavedMonosaccharide());
        Assert.assertEquals(FragmentType.INTACT, precursor.getFragmentType());

        GlycanFragmentOLD reverse0_1 = fragments.get(1);
        Assert.assertEquals(glc, reverse0_1.getRoot());
        Assert.assertEquals(glc, reverse0_1.getCleavedMonosaccharide());
        Assert.assertEquals(FragmentType.REVERSE, reverse0_1.getFragmentType());

        GlycanFragmentOLD forward0_1 = fragments.get(2);
        Assert.assertEquals(gal, forward0_1.getRoot());
        Assert.assertEquals(gal, forward0_1.getCleavedMonosaccharide());
        Assert.assertEquals(FragmentType.FORWARD, forward0_1.getFragmentType());

        GlycanFragmentOLD reverse2_3 = fragments.get(5);
        Assert.assertEquals(glc, reverse2_3.getRoot());
        Assert.assertEquals(man, reverse2_3.getCleavedMonosaccharide());
        Assert.assertEquals(FragmentType.REVERSE, reverse2_3.getFragmentType());

        GlycanFragmentOLD forward2_3 = fragments.get(6);
        Assert.assertEquals(fuc, forward2_3.getRoot());
        Assert.assertEquals(fuc, forward2_3.getCleavedMonosaccharide());
        Assert.assertEquals(FragmentType.FORWARD, forward2_3.getFragmentType());  */
    }


    @Test @Ignore
    public void test733() throws Exception{

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide glc = builder.setRoot(monosaccharideLookup.getNew("Glc"),Optional.of(Anomericity.alpha), "A");
        builder.add(substituentLookup.getNew("NAcetyl"), glc, new SubstituentLinkage(2, Composition.parseComposition("O-1H-1")));
        Monosaccharide gal = builder.add(monosaccharideLookup.getNew("Gal"), glc, new GlycosidicLinkage(Anomericity.beta, 1, 4, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1") ));
        Monosaccharide gal2 = builder.add(monosaccharideLookup.getNew("Gal"), gal, new GlycosidicLinkage(Anomericity.alpha, 1, 3,  Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1") ));
        Monosaccharide fuc = builder.add(monosaccharideLookup.getNew("Fuc"), gal, new GlycosidicLinkage(Anomericity.alpha, 1, 2 , Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1") ));
        builder.add(substituentLookup.getNew("NAcetyl"), gal2, new SubstituentLinkage(2, Composition.parseComposition("O-1H-1")));
        builder.add(substituentLookup.getNew("Sulfate"), fuc, new SubstituentLinkage(3, Composition.parseComposition("H-1")));

        Glycan glycan = builder.build();

        CutDescriptorGenerator cutDescriptorGenerator = new CutDescriptorGenerator();

        List<CutDescriptor> descriptors = cutDescriptorGenerator.generateCutDescriptor(glycan, 2, 1);
        List<GlycanFragment> fragments = new ArrayList<GlycanFragment>();
        for(CutDescriptor descriptor : descriptors){
            fragments.addAll(glycan.getFragment(descriptor));
        }

        Assert.assertEquals(9, fragments.size());


    }
    /**
     * <pre>
     * Gal(1)--Glc(0)-|
     *     *   *
     *       +
     *       *
     *     Man(2)
     * </pre>
     * @throws Exception
     */
    @Test(expected = IllegalStateException.class)
    public void testFailOnHyperEdge() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide glc = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "A");
        Monosaccharide gal = builder.add(monosaccharideLookup.getNew("Gal"), glc, new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));
        builder.add(monosaccharideLookup.getNew("Man"), Arrays.asList(glc, gal), new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent()));

        Glycan glycan = builder.build();

        Assert.assertEquals(true, glycan.hasHyperEdges());
        glycan.getFragment(new CutDescriptor());
    }

    /**
     * The masses in the test are compute with GlycoWorkBench.
     * The sugars in the test have FreeEnd.
     * @throws Exception
     */
    @Test
    public void testCalculateMolecularMass() throws Exception{

        double delta = 0.0001; //Because GlycoWorkBench round the masses to the fourth decimal place.

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar = builder.build();

        Assert.assertEquals(504.1690,sugar.calculateMass(),delta);

        Glycan.Builder builder1 = new Glycan.Builder();
        Monosaccharide node1_0 = builder1.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_2");
        Monosaccharide node1_1 = builder1.add(monosaccharideLookup.getNew("Hex"), node1_0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node1_2 = builder1.add(monosaccharideLookup.getNew("Gal"), node1_1, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Substituent node1_3 = builder1.add(substituentLookup.getNew("NAcetyl"), node1_2, new SubstituentLinkage(Optional.<Integer>absent(), Optional.of(Composition.parseComposition("O-1H-1"))));
        Glycan sugar1 = builder1.build();

        Assert.assertEquals(545.1956,sugar1.calculateMass(),delta);

        Glycan.Builder builder2 = new Glycan.Builder();
        Monosaccharide node2_0 = builder2.setRoot(monosaccharideLookup.getNew("Gal"), Optional.<Anomericity>absent(), "Id_3");
        Monosaccharide node2_1 = builder2.add(monosaccharideLookup.getNew("Hex"), node2_0, new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Substituent node2_3 = builder2.add(substituentLookup.getNew("NAcetyl"), node2_1, new SubstituentLinkage(Optional.<Integer>absent(), Optional.of(Composition.parseComposition("O-1H-1"))));
        Monosaccharide node2_2 = builder2.add(monosaccharideLookup.getNew("Fuc"), node2_1, new GlycosidicLinkage(Anomericity.alpha, 1, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar2 = builder2.build();

        Assert.assertEquals(529.2007,sugar2.calculateMass(),delta);
    }

    /**
     * The masses in the test are compute with GlycoWorkBench.
     * The sugars in the test have FreeEnd.
     * @throws Exception
     */
    @Test
    public void testCalculateMolecularMassWithGlycoCT() throws Exception{

        GlycoCTReader reader = new GlycoCTReader();
        double delta = 0.0001;
        Glycan glycan = reader.read("RES\n" +
                "1b:o-dgal-HEX-0:0|1:aldi\n" +
                "2s:n-acetyl\n" +
                "3b:b-dglc-HEX-1:5\n" +
                "4b:x-lgal-HEX-1:5|6:d\n" +
                "5b:x-dgal-HEX-1:5\n" +
                "6b:a-lgal-HEX-1:5|6:d\n" +
                "7b:a-dgal-HEX-1:5\n" +
                "8s:n-acetyl\n" +
                "LIN\n" +
                "1:1d(2+1)2n\n" +
                "2:1o(3+1)3d\n" +
                "3:3o(-1+1)4d\n" +
                "4:3o(-1+1)5d\n" +
                "5:5o(-1+2)6d\n" +
                "6:5o(3+1)7d\n" +
                "7:3d(2+1)8n", "test");

        Assert.assertEquals(1042.406424,glycan.calculateMass(),delta);
        Assert.assertEquals(1041.399148,glycan.calculateMz(1),delta);
    }


    /**
     * The masses in the test are compute with GlycoWorkBench.
     * The sugars in the test have FreeEnd.
     * @throws Exception
     */
    @Test
    public void testCalculateMolecularMassWithGlycoCT2() throws Exception{

        GlycoCTReader reader = new GlycoCTReader();
        double delta = 0.0001;

        Glycan glycan2 = reader.read("RES\n" +
                "1b:x-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" +
                "2s:n-glycolyl\n" +
                "LIN\n" +
                "1:1d(5+1)2n", "test");

        Assert.assertEquals(325.1009,glycan2.calculateMass(),delta);
    }

    @Test
    public void testCalculateMz() throws Exception{

        double delta = 0.0001; //Because GlycoWorkBench round the masses to the fourth decimal place.

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar = builder.build();

        Assert.assertEquals(503.1618,sugar.calculateMz(1),delta);
        Assert.assertEquals(251.0772,sugar.calculateMz(2),delta);
        Assert.assertEquals(167.0491,sugar.calculateMz(3),delta);

        Glycan.Builder builder1 = new Glycan.Builder();
        Monosaccharide node1_0 = builder1.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_2");
        Monosaccharide node1_1 = builder1.add(monosaccharideLookup.getNew("Hex"), node1_0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node1_2 = builder1.add(monosaccharideLookup.getNew("Gal"), node1_1, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Substituent node1_3 = builder1.add(substituentLookup.getNew("NAcetyl"), node1_2, new SubstituentLinkage(Optional.<Integer>absent(), Optional.of(Composition.parseComposition("O-1H-1"))));
        Glycan sugar1 = builder1.build();

        Assert.assertEquals(544.1883,sugar1.calculateMz(1),delta);
        Assert.assertEquals(271.5905,sugar1.calculateMz(2),delta);
        Assert.assertEquals(180.7246,sugar1.calculateMz(3),delta);

        Glycan.Builder builder2 = new Glycan.Builder();
        Monosaccharide node2_0 = builder2.setRoot(monosaccharideLookup.getNew("Gal"), Optional.<Anomericity>absent(), "Id_3");
        Monosaccharide node2_1 = builder2.add(monosaccharideLookup.getNew("Hex"), node2_0, new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Substituent node2_3 = builder2.add(substituentLookup.getNew("NAcetyl"), node2_1, new SubstituentLinkage(Optional.<Integer>absent(), Optional.of(Composition.parseComposition("O-1H-1"))));
        Monosaccharide node2_2 = builder2.add(monosaccharideLookup.getNew("Fuc"), node2_1, new GlycosidicLinkage(Anomericity.alpha, 1, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar2 = builder2.build();

        Assert.assertEquals(528.1934,sugar2.calculateMz(1),delta);
        Assert.assertEquals(263.5931,sugar2.calculateMz(2),delta);
        Assert.assertEquals(175.3929,sugar2.calculateMz(3),delta);

    }

    @Test
    public void testCanFragmentCrossRing() throws Exception{

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar = builder.build();
        Assert.assertTrue(sugar.canFragmentCrossRing(node0));
        Assert.assertTrue(sugar.canFragmentCrossRing(node1));
        Assert.assertTrue(sugar.canFragmentCrossRing(node2));

        Glycan.Builder builder2 = new Glycan.Builder();
        Monosaccharide node2_0 = builder2.setRoot(monosaccharideLookup.getNew("Gal"), Optional.<Anomericity>absent(), "Id_3");
        Monosaccharide node2_1 = builder2.add(monosaccharideLookup.getNew("Hex"), node2_0, new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Substituent node2_3 = builder2.add(substituentLookup.getNew("NAcetyl"), node2_1, new SubstituentLinkage(Optional.<Integer>absent(), Optional.of(Composition.parseComposition("O-1H-1"))));
        Monosaccharide node2_2 = builder2.add(monosaccharideLookup.getNew("Fuc"), node2_1, new GlycosidicLinkage(Anomericity.alpha, 1, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar2 = builder2.build();
        Assert.assertFalse(sugar2.canFragmentCrossRing(node2_1));
    }

    @Test
    public void testCanFragmentCrossRingNoCarbon() throws  Exception{

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        List<Monosaccharide> parents = new ArrayList<Monosaccharide>();
        parents.add(node0);
        parents.add(node1);
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), parents, new GlycosidicLinkage(Anomericity.alpha, 4, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar = builder.build();
        Assert.assertFalse(sugar.canFragmentCrossRing(node0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testZeroCharge() throws  Exception{

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar = builder.build();
        sugar.calculateMz(0);

    }


    @Test
    public void testEquals() throws  Exception{
        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 1, 6, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar = builder.build();

        Glycan.Builder builder1 = new Glycan.Builder();
        Monosaccharide node1_0 = builder1.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1_1 = builder1.add(monosaccharideLookup.getNew("Man"), node1_0, new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node1_2 = builder1.add(monosaccharideLookup.getNew("Gal"), node1_0, new GlycosidicLinkage(Anomericity.alpha, 1, 6, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar1 = builder1.build();

        Assert.assertTrue(sugar.equals(sugar1));
        Assert.assertTrue(sugar1.equals(sugar));

        Glycan.Builder builder2 = new Glycan.Builder();
        Monosaccharide node2_0 = builder2.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node2_1 = builder2.add(monosaccharideLookup.getNew("Man"), node2_0, new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2_2 = builder2.add(monosaccharideLookup.getNew("Glc"), node2_0, new GlycosidicLinkage(Anomericity.alpha, 1, 6, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar2 = builder2.build();

        Assert.assertFalse(sugar.equals(sugar2));
        Assert.assertFalse(sugar2.equals(sugar));

    }

    @Test
    public void testisEmpty() throws  Exception{
        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Glycan sugar = builder.build();
        Assert.assertFalse(sugar.isEmpty());
    }


    @Test
    public void testHashCode() throws  Exception{

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Man"), node0, new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.alpha, 1, 6, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar = builder.build();

        Glycan.Builder builder1 = new Glycan.Builder();
        Monosaccharide node1_0 = builder1.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "Id_1");
        Monosaccharide node1_1 = builder1.add(monosaccharideLookup.getNew("Man"), node1_0, new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node1_2 = builder1.add(monosaccharideLookup.getNew("Gal"), node1_0, new GlycosidicLinkage(Anomericity.alpha, 1, 6, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar1 = builder1.build();

        Assert.assertEquals(sugar.hashCode(),sugar1.hashCode());


    }



}
