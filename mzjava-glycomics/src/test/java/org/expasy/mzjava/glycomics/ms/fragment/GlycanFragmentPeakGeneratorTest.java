package org.expasy.mzjava.glycomics.ms.fragment;

import org.expasy.mzjava.core.ms.peaklist.Peak;
import org.expasy.mzjava.core.ms.spectrum.AnnotatedPeak;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.glycomics.mol.Glycan;
import org.expasy.mzjava.glycomics.mol.GlycanFragment;
import org.expasy.mzjava.glycomics.ms.spectrum.GlycanFragAnnotation;
import org.junit.Assert;
import org.junit.Test;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */

public class GlycanFragmentPeakGeneratorTest {


    @Test
    public void testGetFragmentTypes() throws Exception {

        Set<FragmentType> fragmentTypes = new HashSet<FragmentType>();
        fragmentTypes.add(FragmentType.FORWARD);
        fragmentTypes.add(FragmentType.REVERSE);
        fragmentTypes.add(FragmentType.INTACT);
        fragmentTypes.add(FragmentType.INTERNAL);
        Set<IonType> ionTypes = EnumSet.of(IonType.a,IonType.b,IonType.c,IonType.i,IonType.x,IonType.y,IonType.z);
        GlycanFragmentPeakGenerator peakGenerator = new GlycanFragmentPeakGenerator(fragmentTypes,ionTypes,1);
        Assert.assertEquals(fragmentTypes,peakGenerator.getFragmentTypes());
    }



    @Test
    public void testGeneratePeaks() throws Exception {

        GlycanFragment mockFragment = mock(GlycanFragment.class);
        Glycan mockGlycan = mock(Glycan.class);
        when(mockFragment.calculateMolecularMass()).thenReturn(211.456);
        when(mockFragment.getFragmentType()).thenReturn(FragmentType.FORWARD);
        when(mockFragment.isEmpty()).thenReturn(false);

        List<AnnotatedPeak<GlycanFragAnnotation>> annotatedPeaks = null;
        Set<FragmentType> fragmentTypes = new HashSet<FragmentType>();
        fragmentTypes.add(FragmentType.FORWARD);
        fragmentTypes.add(FragmentType.REVERSE);
        fragmentTypes.add(FragmentType.INTACT);
        fragmentTypes.add(FragmentType.INTERNAL);
        Set<IonType> ionTypes = EnumSet.of(IonType.a,IonType.b,IonType.c,IonType.i,IonType.x,IonType.y,IonType.z);
        GlycanFragmentPeakGenerator peakGenerator = new GlycanFragmentPeakGenerator(fragmentTypes,ionTypes,1);
        int[] charge = new int[3];
        charge[0] = 1;
        charge[1] = 2;
        charge[2] = 3;
        annotatedPeaks =peakGenerator.generatePeaks(mockGlycan,mockFragment,charge,annotatedPeaks);
        for(Peak peak : annotatedPeaks){
            if (peak.getCharge() == 1){
                Assert.assertEquals(211.456,peak.getMz(),0.0000001);
            }
            if (peak.getCharge() == 2){
                Assert.assertEquals(105.22436194615,peak.getMz(),0.0000001);
            }
            if (peak.getCharge() == 3){
                Assert.assertEquals(69.8138159282,peak.getMz(),0.0000001);
            }
        }
        Assert.assertEquals(3,annotatedPeaks.size());

    }

    @Test

    public void testGeneratePeaksOnlyaIon() throws Exception {

        GlycanFragment mockFragment = mock(GlycanFragment.class);
        Glycan mockGlycan = mock(Glycan.class);
        when(mockFragment.calculateMolecularMass()).thenReturn(211.456);
        when(mockFragment.getFragmentType()).thenReturn(FragmentType.FORWARD);
        when(mockFragment.getIonTypes()).thenReturn(EnumSet.of(IonType.c));
        when(mockFragment.isEmpty()).thenReturn(false);

        List<AnnotatedPeak<GlycanFragAnnotation>> annotatedPeaks = null;
        Set<FragmentType> fragmentTypes = new HashSet<FragmentType>();
        fragmentTypes.add(FragmentType.FORWARD);
        fragmentTypes.add(FragmentType.REVERSE);
        fragmentTypes.add(FragmentType.INTACT);
        fragmentTypes.add(FragmentType.INTERNAL);
        Set<IonType> ionTypes = EnumSet.of(IonType.a);

        GlycanFragmentPeakGenerator peakGenerator = new GlycanFragmentPeakGenerator(fragmentTypes,ionTypes,1);

        int[] charge = new int[3];
        charge[0] = 1;
        charge[1] = 2;
        charge[2] = 3;
        annotatedPeaks = peakGenerator.generatePeaks(mockGlycan,mockFragment,charge,annotatedPeaks);

        Assert.assertEquals(0, annotatedPeaks.size());
        ionTypes.add(IonType.c);
        GlycanFragmentPeakGenerator peakGenerator2 = new GlycanFragmentPeakGenerator(fragmentTypes,ionTypes,1);
        annotatedPeaks.clear();
        annotatedPeaks =peakGenerator.generatePeaks(mockGlycan,mockFragment,charge,annotatedPeaks);


        Assert.assertEquals(3,annotatedPeaks.size());



    }

}
