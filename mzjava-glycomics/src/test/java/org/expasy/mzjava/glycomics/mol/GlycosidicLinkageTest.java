/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.glycomics.mol;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.mol.Composition;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class GlycosidicLinkageTest {

    @Test
    public void testEqualsAndHash() throws Exception {

        GlycosidicLinkage linkage1 = new GlycosidicLinkage(Anomericity.alpha, 2, 3, Composition.parseComposition("(OH)-1"), Composition.parseComposition("H-1"));
        GlycosidicLinkage linkage2 = new GlycosidicLinkage(Anomericity.alpha, 2, 3, Composition.parseComposition("(OH)-1"), Composition.parseComposition("H-1"));
        GlycosidicLinkage linkage3 = new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("(OH)-1"), Composition.parseComposition("H-1"));
        GlycosidicLinkage linkage4 = new GlycosidicLinkage(Anomericity.alpha, 3, 2, Composition.parseComposition("(OH)-1"), Composition.parseComposition("(OH)-1"));
        GlycosidicLinkage unknown = new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent());

        assertThat(linkage1.equals(linkage2), is(true));
        assertThat(linkage2.equals(linkage1), is(true));
        assertThat(linkage1.hashCode(), is(linkage2.hashCode()));

        assertThat(linkage1.equals(linkage3), is(false));
        assertThat(linkage1.hashCode(), not(linkage3.hashCode()));
        assertThat(linkage2.equals(linkage3), is(false));
        assertThat(linkage2.hashCode(), not(linkage3.hashCode()));
        assertThat(linkage3.equals(linkage4), is(false));
        assertThat(linkage3.hashCode(), not(linkage4.hashCode()));



        assertThat(linkage1.equals(unknown), is(false));
        assertThat(linkage1.hashCode(), not(unknown.hashCode()));
        assertThat(unknown.equals(linkage1), is(false));
    }

    @Test
    public void testToString() throws Exception {

        assertEquals("GlycosidicLinkage{anomericity=alpha, anomericCarbon=2, linkedCarbon=3, anomericComposition=(OH)-1, linkedComposition=H-1}", new GlycosidicLinkage(Anomericity.alpha, 2, 3, Composition.parseComposition("(OH)-1"), Composition.parseComposition("H-1")).toString());
        assertEquals("GlycosidicLinkage{anomericity=unknown, anomericCarbon=-1, linkedCarbon=-1, anomericComposition=unknown, linkedComposition=unknown}", GlycosidicLinkage.UNKNOWN.toString());
    }

    @Test
    public void testHasSetCarbon() throws Exception {

        Integer linkedCarbon = null;
        Integer anomericCarbon = null;
        Composition linkageComposition = null;
        Composition anomericComposition = null;

        GlycosidicLinkage linkageNoCarb = new GlycosidicLinkage(Anomericity.alpha,anomericCarbon,linkedCarbon,anomericComposition,linkageComposition);
        assertFalse(linkageNoCarb.hasSetCarbon());

        GlycosidicLinkage linkageNoCarb2 = new GlycosidicLinkage(Anomericity.alpha,anomericCarbon,4,anomericComposition,linkageComposition);
        assertFalse(linkageNoCarb2.hasSetCarbon());

        GlycosidicLinkage linkageWithCarb = new GlycosidicLinkage(Anomericity.alpha,2,4,anomericComposition,linkageComposition);
        assertTrue(linkageWithCarb.hasSetCarbon());
    }
}
