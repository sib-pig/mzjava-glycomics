package org.expasy.mzjava.glycomics.mol;

import org.junit.Assert;
import org.junit.Test;

public class CleavedMonosaccharideTest {

    @Test
    public void testGetMonosaccharide() throws Exception {

        DefaultMonosaccharideLookup monosaccharideLookup = new DefaultMonosaccharideLookup();
        Monosaccharide monosaccharide = monosaccharideLookup.getNew("Gal");
        CleavedMonosaccharide cleavedMonosaccharide = new CleavedMonosaccharide(monosaccharide, new CutIndexes(1,5));
        Assert.assertEquals(monosaccharide, cleavedMonosaccharide.getMonosaccharide());

    }

    @Test
    public void testGetCutIndexes() throws Exception {

        DefaultMonosaccharideLookup monosaccharideLookup = new DefaultMonosaccharideLookup();
        Monosaccharide monosaccharide = monosaccharideLookup.getNew("Gal");
        CleavedMonosaccharide cleavedMonosaccharide = new CleavedMonosaccharide(monosaccharide, new CutIndexes(1,5));
        Assert.assertEquals(new CutIndexes(1,5), cleavedMonosaccharide.getCutIndexes());

    }

    @Test
    public void testEquals() throws Exception {

        DefaultMonosaccharideLookup monosaccharideLookup = new DefaultMonosaccharideLookup();
        Monosaccharide monosaccharide = monosaccharideLookup.getNew("Gal");
        CleavedMonosaccharide cleavedMonosaccharide = new CleavedMonosaccharide(monosaccharide, new CutIndexes(1,5));
        CleavedMonosaccharide cleavedMonosaccharide2 = new CleavedMonosaccharide(monosaccharide, new CutIndexes(1,5));
        CleavedMonosaccharide cleavedMonosaccharide3 = new CleavedMonosaccharide(monosaccharideLookup.getNew("Gal"), new CutIndexes(1,5));
        CleavedMonosaccharide cleavedMonosaccharide4 = new CleavedMonosaccharide(monosaccharide, new CutIndexes(2,5));


        Assert.assertTrue(cleavedMonosaccharide.equals(cleavedMonosaccharide2));
        Assert.assertTrue(cleavedMonosaccharide2.equals(cleavedMonosaccharide));
        Assert.assertFalse(cleavedMonosaccharide.equals(cleavedMonosaccharide3));
        Assert.assertFalse(cleavedMonosaccharide3.equals(cleavedMonosaccharide));
        Assert.assertFalse(cleavedMonosaccharide4.equals(cleavedMonosaccharide));
        Assert.assertFalse(cleavedMonosaccharide.equals(cleavedMonosaccharide4));

    }
}