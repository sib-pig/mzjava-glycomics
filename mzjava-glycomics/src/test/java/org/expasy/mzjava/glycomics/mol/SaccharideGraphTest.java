package org.expasy.mzjava.glycomics.mol;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.mol.Composition;
import org.junit.Test;
import org.mockito.InOrder;
import org.openide.util.Lookup;

import static org.mockito.Mockito.*;

public class SaccharideGraphTest {

    MonosaccharideLookup monosaccharideLookup = Lookup.getDefault().lookup(MonosaccharideLookup.class);
    SubstituentLookup substituentLookup = Lookup.getDefault().lookup(SubstituentLookup.class);
    
    /**
     * glc
     * @throws Exception
     */
    @Test
    public void testForEachLinkageBFS1Monosaccharide() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide glc = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.fromNullable(Anomericity.alpha), "A");
        GlycosidicLinkage linkage = new GlycosidicLinkage(Optional.fromNullable(Anomericity.alpha), Optional.fromNullable(1), Optional.of(1), Optional.<Composition>absent(), Optional.<Composition>absent());
        Monosaccharide gal = builder.add(monosaccharideLookup.getNew("Gal"), glc, linkage);
        Glycan glycan = builder.build();

        LinkageAcceptor linkageAcceptor = mock(LinkageAcceptor.class);
        glycan.forEachLinkage(SaccharideGraph.Traversal.BFS, linkageAcceptor);

        verify(linkageAcceptor).accept(glc, gal, linkage);
        verifyNoMoreInteractions(linkageAcceptor);
    }

    /**
     *       man - fuc - ara
     *     /
     * glc               galA
     *     \           /
     *       gal - bac
     *                 \
     *                   dHex
     *
     * @throws Exception
     */
    @Test
    public void testForEachLinkageBFS() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        GlycosidicLinkage linkage = new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent());

        Monosaccharide glc = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "A");

        Monosaccharide man = builder.add(monosaccharideLookup.getNew("Man"), glc, linkage);
        Monosaccharide gal = builder.add(monosaccharideLookup.getNew("Gal"), glc, linkage);

        Monosaccharide fuc = builder.add(monosaccharideLookup.getNew("Fuc"), man, linkage);
        Monosaccharide bac = builder.add(monosaccharideLookup.getNew("Bac"), gal, linkage);

        Monosaccharide ara = builder.add(monosaccharideLookup.getNew("Ara"), fuc, linkage);
        Monosaccharide galA = builder.add(monosaccharideLookup.getNew("GalA"), bac, linkage);
        Monosaccharide dHex = builder.add(monosaccharideLookup.getNew("DeoxyHex"), bac, linkage);

        Glycan glycan = builder.build();

        LinkageAcceptor linkageAcceptor = mock(LinkageAcceptor.class);
        InOrder inOrder = inOrder(linkageAcceptor);

        glycan.forEachLinkage(SaccharideGraph.Traversal.BFS, linkageAcceptor);

        inOrder.verify(linkageAcceptor).accept(glc, gal, linkage);
        inOrder.verify(linkageAcceptor).accept(glc, man, linkage);

        inOrder.verify(linkageAcceptor).accept(gal, bac, linkage);
        inOrder.verify(linkageAcceptor).accept(man, fuc, linkage);

        inOrder.verify(linkageAcceptor).accept(bac, dHex, linkage);
        inOrder.verify(linkageAcceptor).accept(bac, galA, linkage);
        inOrder.verify(linkageAcceptor).accept(fuc, ara, linkage);

        verifyNoMoreInteractions(linkageAcceptor);
    }

    /**
     *       man - fuc - ara
     *     /
     * glc               galA
     *     \           /
     *       gal - bac
     *                 \
     *                   dHex
     *
     * @throws Exception
     */
    @Test
    public void testForEachLinkageBFSWithComparator() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        GlycosidicLinkage linkage = new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent());

        Monosaccharide glc = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "A");

        Monosaccharide man = builder.add(monosaccharideLookup.getNew("Man"), glc, linkage);
        Monosaccharide gal = builder.add(monosaccharideLookup.getNew("Gal"), glc, linkage);

        Monosaccharide fuc = builder.add(monosaccharideLookup.getNew("Fuc"), man, linkage);
        Monosaccharide bac = builder.add(monosaccharideLookup.getNew("Bac"), gal, linkage);

        Monosaccharide ara = builder.add(monosaccharideLookup.getNew("Ara"), fuc, linkage);
        Monosaccharide galA = builder.add(monosaccharideLookup.getNew("GalA"), bac, linkage);
        Monosaccharide dHex = builder.add(monosaccharideLookup.getNew("DeoxyHex"), bac, linkage);

        Glycan glycan = builder.build();

        LinkageAcceptor linkageAcceptor = mock(LinkageAcceptor.class);
        InOrder inOrder = inOrder(linkageAcceptor);

        TraversalComparator traversalComparator = new TraversalComparator() {
            @Override
            public int compare(Monosaccharide child1, GlycosidicLinkage linkage1, Monosaccharide child2, GlycosidicLinkage linkage2) {

                return child2.getName().compareTo(child1.getName());
            }
        };

        glycan.forEachLinkage(SaccharideGraph.Traversal.BFS, traversalComparator, linkageAcceptor);

        inOrder.verify(linkageAcceptor).accept(glc, man, linkage);
        inOrder.verify(linkageAcceptor).accept(glc, gal, linkage);

        inOrder.verify(linkageAcceptor).accept(man, fuc, linkage);
        inOrder.verify(linkageAcceptor).accept(gal, bac, linkage);

        inOrder.verify(linkageAcceptor).accept(fuc, ara, linkage);
        inOrder.verify(linkageAcceptor).accept(bac, galA, linkage);
        inOrder.verify(linkageAcceptor).accept(bac, dHex, linkage);

        verifyNoMoreInteractions(linkageAcceptor);
    }

    /**
     *       ara - fuc
     *     /
     * glc
     *     \
     *       gal - man
     * @throws Exception
     */
    @Test
    public void testForEachLinkageDFS() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        GlycosidicLinkage linkage = new GlycosidicLinkage(Optional.<Anomericity>absent(), Optional.<Integer>absent(), Optional.<Integer>absent(), Optional.<Composition>absent(), Optional.<Composition>absent());
        Monosaccharide glc = builder.setRoot(monosaccharideLookup.getNew("Glc"), Optional.<Anomericity>absent(), "A");
        Monosaccharide gal = builder.add(monosaccharideLookup.getNew("Gal"), glc, linkage);
        Monosaccharide man = builder.add(monosaccharideLookup.getNew("Man"), gal, linkage);
        Monosaccharide ara = builder.add(monosaccharideLookup.getNew("Ara"), glc, linkage);
        Monosaccharide fuc = builder.add(monosaccharideLookup.getNew("Fuc"), ara, linkage);

        Glycan glycan = builder.build();

        LinkageAcceptor linkageAcceptor = mock(LinkageAcceptor.class);
        InOrder inOrder = inOrder(linkageAcceptor);

        glycan.forEachLinkage(SaccharideGraph.Traversal.DFS, linkageAcceptor);

        inOrder.verify(linkageAcceptor).accept(glc, gal, linkage);
        inOrder.verify(linkageAcceptor).accept(gal, man, linkage);
        inOrder.verify(linkageAcceptor).accept(glc, ara, linkage);
        inOrder.verify(linkageAcceptor).accept(ara, fuc, linkage);
        verifyNoMoreInteractions(linkageAcceptor);
    }

}