/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.glycomics.mol;

import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class NodeSetTest {

    @SuppressWarnings("RedundantStringConstructorCall")
    @Test
    public void testEqualsToHash() throws Exception {

        Set<String> nodeSet = NodeSet.of("A", "B", "C");
        Set<String> hasSet = Sets.newHashSet("A", "B", "C");

        Assert.assertEquals(true, nodeSet.equals(hasSet));
        Assert.assertEquals(true, hasSet.equals(nodeSet));

        Set<String> nodeSet2 = NodeSet.of("A", "B", "C", new String("A"));
        Assert.assertEquals(false, nodeSet2.equals(hasSet));
        Assert.assertEquals(false, hasSet.equals(nodeSet2));
    }

    @SuppressWarnings("RedundantStringConstructorCall")
    @Test
    public void testEquals() throws Exception {

        Set<String> set1 = NodeSet.of("A", "B", "C");
        Set<String> set2 = NodeSet.of("A", "B", "C");
        Set<String> set3 = NodeSet.of("A", "B", "C", new String("A"));

        Assert.assertEquals(true, set1.equals(set2));
        Assert.assertEquals(true, set2.equals(set1));

        Assert.assertEquals(false, set1.equals(set3));
        Assert.assertEquals(false, set2.equals(set3));
    }
}
