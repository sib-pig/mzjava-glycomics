package org.expasy.mzjava.glycomics.mol;

import org.expasy.mzjava.core.mol.Composition;
import org.junit.Assert;
import org.junit.Test;

import java.io.StringReader;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class MonosaccharideJsonParserTest {
    private final Composition compositionAlt = Composition.parseComposition("C6H12O6");
    private final Monosaccharide Alt = new Monosaccharide("Alt",compositionAlt, MonosaccharideSuperclass.Hexose, MonosaccharideClass.Hex, RingType.pyranose,1,5);

    @Test
    public void testParse() throws IllegalStateException {
        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Alt\",\n" +
                "\t\t\"superclass\": \"Hexose\",\n" +
                "\t\t\"class\": \"Hex\",\n" +
                "\t\t\"ring\": \"p\",\n" +
                "\t\t\"ringstart\": 1,\n" +
                "\t\t\"ringend\": 5,\n" +
                "\t\t\"composition\": \"C6H12O6\"\n" +
                "\t}" +
                "]");
        final DefaultMonosaccharideLookup monosaccharideLookup = new DefaultMonosaccharideLookup();
        final Map<String, Monosaccharide> msMap = new LinkedHashMap<String, Monosaccharide>();
        monosaccharideLookup.parse(sr,msMap);

        Assert.assertEquals(msMap.size(),1);
        Assert.assertEquals(msMap.get("Alt"),Alt);


    }

    @Test(expected = NullPointerException.class)
    public void testNullReader() throws IllegalStateException {

        final DefaultMonosaccharideLookup monosaccharideLookup = new DefaultMonosaccharideLookup();
        final Map<String, Monosaccharide> msMap = new LinkedHashMap<String, Monosaccharide>();
        monosaccharideLookup.parse(null,msMap);
        Assert.assertEquals(msMap.size(),1);
        Assert.assertEquals(msMap.get("Alt"),Alt);
    }


    @Test(expected = NullPointerException.class)
    public void testNullMap() throws IllegalStateException {
        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Alt\",\n" +
                "\t\t\"superclass\": \"Hexose\",\n" +
                "\t\t\"class\": \"Hex\",\n" +
                "\t\t\"ring\": \"p\",\n" +
                "\t\t\"ringstart\": 1,\n" +
                "\t\t\"ringend\": 5,\n" +
                "\t\t\"composition\": \"C6H12O6\"\n" +
                "\t}" +
                "]");

        final DefaultMonosaccharideLookup monosaccharideLookup = new DefaultMonosaccharideLookup();
        monosaccharideLookup.parse(sr,null);

    }

    @Test
    public void testParseGoodEntry() throws IllegalStateException {
    StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Alt\",\n" +
                "\t\t\"superclass\": \"Hexose\",\n" +
                "\t\t\"ring\": \"p\",\n" +
                "\t\t\"ringstart\": 1,\n" +
                "\t\t\"ringend\": 5,\n" +
                "\t\t\"class\": \"Hex\",\n" +
                "\t\t\"composition\": \"C6H12O6\"\n" +
                "\t}" +
                "]");

        final DefaultMonosaccharideLookup monosaccharideLookup = new DefaultMonosaccharideLookup();
        final Map<String, Monosaccharide> msMap = new LinkedHashMap<String, Monosaccharide>();
        monosaccharideLookup.parse(sr,msMap);
        Assert.assertEquals(msMap.size(),1);
        Assert.assertEquals(msMap.get("Alt"),Alt);
    }

    @Test(expected = IllegalStateException.class)
    public void testParseNoname() throws IllegalStateException {
        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"composition\": \"C6H12O6\"\n" +
                "\t}" +
                "]");
        final DefaultMonosaccharideLookup monosaccharideLookup = new DefaultMonosaccharideLookup();
        final Map<String, Monosaccharide> msMap = new LinkedHashMap<String, Monosaccharide>();
        monosaccharideLookup.parse(sr,msMap);
        Assert.assertEquals(msMap.size(),1);
        Assert.assertEquals(msMap.get("Alt"),Alt);

    }
    @Test(expected = IllegalStateException.class)
    public void testParseNocomp() throws IllegalStateException {
        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"C6H12O6\"\n" +
                "\t}" +
                "]");

        final DefaultMonosaccharideLookup monosaccharideLookup = new DefaultMonosaccharideLookup();
        final Map<String, Monosaccharide> msMap = new LinkedHashMap<String, Monosaccharide>();
        monosaccharideLookup.parse(sr,msMap);
        Assert.assertEquals(msMap.size(),1);
        Assert.assertEquals(msMap.get("Alt"),Alt);

    }

    @Test(expected = IllegalStateException.class)
    public void testParseBadNameField() throws IllegalStateException {
        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"naasme\": \"Alt\",\n" +
                "\t\t\"composrtition\": \"C6H12O6\"\n" +
                "\t}" +
                "]");

        final DefaultMonosaccharideLookup monosaccharideLookup = new DefaultMonosaccharideLookup();
        final Map<String, Monosaccharide> msMap = new LinkedHashMap<String, Monosaccharide>();
        monosaccharideLookup.parse(sr,msMap);
        Assert.assertEquals(msMap.size(),1);
        Assert.assertEquals(msMap.get("Alt"),Alt);
    }

    @Test(expected = IllegalStateException.class)
    public void testParseMissing() throws IllegalStateException {
        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Alt\",\n" +
                "\t\t\"composition\": \"C6H12O6\"\n" +
                "\t}" +
                "]");

        final DefaultMonosaccharideLookup monosaccharideLookup = new DefaultMonosaccharideLookup();
        final Map<String, Monosaccharide> msMap = new LinkedHashMap<String, Monosaccharide>();
        monosaccharideLookup.parse(sr,msMap);
    }


    @Test
    public void testParseGoodEntry2() throws IllegalStateException {
        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Alt\",\n" +
                "\t\t\"name2\": \"Alt\",\n" +
                "\t\t\"superclass\": \"Hexose\",\n" +
                "\t\t\"ring\": \"p\",\n" +
                "\t\t\"ringstart\": 1,\n" +
                "\t\t\"ringend\": 5,\n" +
                "\t\t\"class\": \"Hex\",\n" +
                "\t\t\"composition\": \"C6H12O6\"\n" +
                "\t}" +
                "]");

        final DefaultMonosaccharideLookup monosaccharideLookup = new DefaultMonosaccharideLookup();
        final Map<String, Monosaccharide> msMap = new LinkedHashMap<String, Monosaccharide>();
        monosaccharideLookup.parse(sr,msMap);
        Assert.assertEquals(msMap.size(),1);
        Assert.assertEquals(msMap.get("Alt"),Alt);
    }

    @Test(expected = IllegalStateException.class)
    public void testParseNoClass() throws IllegalStateException {
        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Alt\",\n" +
                "\t\t\"name2\": \"Alt\",\n" +
                "\t\t\"superclass\": \"Hexosedsada\",\n" +
                "\t\t\"composition\": \"C6H12O6\"\n" +
                "\t}" +
                "]");

        final DefaultMonosaccharideLookup monosaccharideLookup = new DefaultMonosaccharideLookup();
        final Map<String, Monosaccharide> msMap = new LinkedHashMap<String, Monosaccharide>();
        monosaccharideLookup.parse(sr,msMap);
        Assert.assertEquals(msMap.size(),1);
    }

    @Test(expected = IllegalStateException.class)
    public void testParseBadClass() throws IllegalStateException {
        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Alt\",\n" +
                "\t\t\"name2\": \"Alt\",\n" +
                "\t\t\"class\": \"Altdsda\",\n" +
                "\t\t\"superclass\": \"Hexose\",\n" +
                "\t\t\"composition\": \"C6H12O6\"\n" +
                "\t}" +
                "]");

        final DefaultMonosaccharideLookup monosaccharideLookup = new DefaultMonosaccharideLookup();
        final Map<String, Monosaccharide> msMap = new LinkedHashMap<String, Monosaccharide>();
        monosaccharideLookup.parse(sr,msMap);
        Assert.assertEquals(msMap.size(),1);
    }

    @Test(expected = IllegalStateException.class)
    public void testParseBadSuperclass() throws IllegalStateException {
        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Alt\",\n" +
                "\t\t\"name2\": \"Alt\",\n" +
                "\t\t\"class\": \"Hex\",\n" +
                "\t\t\"superclass\": \"asdfada\",\n" +
                "\t\t\"composition\": \"C6H12O6\"\n" +
                "\t}" +
                "]");

        final DefaultMonosaccharideLookup monosaccharideLookup = new DefaultMonosaccharideLookup();
        final Map<String, Monosaccharide> msMap = new LinkedHashMap<String, Monosaccharide>();
        monosaccharideLookup.parse(sr,msMap);
        Assert.assertEquals(msMap.size(),1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParseBadRing() throws IllegalStateException {
        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Alt\",\n" +
                "\t\t\"name2\": \"Alt\",\n" +
                "\t\t\"class\": \"Hex\",\n" +
                "\t\t\"ring\": \"t\",\n" +
                "\t\t\"ringstart\": 1,\n" +
                "\t\t\"ringend\": 5,\n" +
                "\t\t\"superclass\": \"Hexose\",\n" +
                "\t\t\"composition\": \"C6H12O6\"\n" +
                "\t}" +
                "]");

        final DefaultMonosaccharideLookup monosaccharideLookup = new DefaultMonosaccharideLookup();
        final Map<String, Monosaccharide> msMap = new LinkedHashMap<String, Monosaccharide>();
        monosaccharideLookup.parse(sr,msMap);
        Assert.assertEquals(msMap.size(),1);
    }


    @Test(expected = IllegalStateException.class)
    public void testParseEmptyClass() throws IllegalStateException {
        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Alt\",\n" +
                "\t\t\"name2\": \"Alt\",\n" +
                "\t\t\"class\": \"\",\n" +
                "\t\t\"ring\": \"t\",\n" +
                "\t\t\"ringstart\": 1,\n" +
                "\t\t\"ringend\": 5,\n" +
                "\t\t\"superclass\": \"Hexose\",\n" +
                "\t\t\"composition\": \"C6H12O6\"\n" +
                "\t}" +
                "]");

        final DefaultMonosaccharideLookup monosaccharideLookup = new DefaultMonosaccharideLookup();
        final Map<String, Monosaccharide> msMap = new LinkedHashMap<String, Monosaccharide>();
        monosaccharideLookup.parse(sr,msMap);
        Assert.assertEquals(msMap.size(),1);
    }



}
