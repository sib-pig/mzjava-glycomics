/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.glycomics.io.mol.glycoct;

import com.google.common.base.Optional;
import org.expasy.mzjava.glycomics.mol.*;
import org.junit.Test;
import org.openide.util.Lookup;

import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: jmarieth
 * Date: 10/29/13
 * Time: 4:34 PM
 */
public class GlycoCTReaderTest {


    private final SubstituentLookup substituentLookup = Lookup.getDefault().lookup(SubstituentLookup.class);
    private final MonosaccharideLookup monosaccharideLookup = Lookup.getDefault().lookup(MonosaccharideLookup.class);


    @Test
    public void testRead() throws Exception {

        GlycoCTReader reader = new GlycoCTReader();

       /* String glycoct1 = "RES\n" +
                "1b:o-dgal-HEX-1:5\n" +
                "2b:b-dgal-HEX-1:5\n" +
                "3s:n-acetyl\n" +
                "LIN\n" +
                "1:1o(4+1)2d\n" +
                "2:1d(2+1)3n";   */


        String glycoct2 = "RES\n" +
                "1b:x-dgal-HEX-1:5\n" +
                "2b:b-dgal-HEX-1:5\n" +
                "3s:n-acetyl\n" +
                "LIN\n" +
                "1:1o(-1+1)2d\n" +
                "2:1d(2+1)3n";

      /*  String glycoct3 = "RES\n" +
                "1b:o-dgal-HEX-0:0|1:aldi\n" +
                "2s:n-acetyl\n" +
                "3b:b-dgal-HEX-1:5\n" +
                "4b:x-HEX-1:5|6:a\n" +
                "5b:x-HEX-1:5\n" +
                "6b:x-HEX-1:5\n" +
                "7s:n-acetyl\n" +
                "LIN\n" +
                "1:1d(2+1)2n\n" +
                "2:1o(3+1)3d\n" +
                "3:3o(-1+1)4d\n" +
                "4:4o(-1+1)5d\n" +
                "5:5o(-1+1)6d\n" +
                "6:5d(2+1)7n";  */


        /*
       "RES\n" +
       "1b:o-dgal-HEX-0:0|1:aldi\n" +
       "2s:n-acetyl\n" +
       "3b:b-dgal-HEX-1:5\n" +  //"3b:b-xgal-HEX-1:5\n" +
       "4b:x-HEX-1:5\n" +
       "5s:n-acetyl\n" +
       "LIN\n" +
       "1:1d(2+1)2n\n" +
       "2:1o(3+1)3d\n" +
       "3:3o(4+1)4d\n" +
       "4:4d(2+1)5n";
        */
//        assertThat(reader.read(glycoct1, "Id_1"),notNullValue());
        assertThat(reader.read(glycoct2, "Id_2"),notNullValue());
//        assertThat(reader.read(glycoct3, "Id_3"),notNullValue());

        //FIXME : write assertion
    }



    @Test
    public void testGlycoctListsToSugar() throws Exception {

        GlycoCTReader reader = new GlycoCTReader();

        String glycoct1 = "RES\n" +
                "1b:x-dgal-HEX-1:5\n" +
                "2b:b-dgal-HEX-1:5\n" +
                "3s:n-acetyl\n" +
                "LIN\n" +
                "1:1o(4+1)2d\n" +
                "2:1d(2+1)3n";

        List<String> glycoctArray1 = reader.glycoctToList(glycoct1);

        Glycan sugar1 = reader.glycoctListsToSugar("Id_1",reader.readRES(glycoctArray1), reader.readLIN(glycoctArray1));

        assertThat(sugar1.order(),is(3));
        assertThat(sugar1.getChildren((Monosaccharide)sugar1.getNode(0)).size(), is(2));
        assertTrue(sugar1.getNode(0).equals(sugar1.getRoot()));
        assertThat(sugar1.copyEdgeList().size(), is(2));
        assertThat(sugar1.getNode(1), instanceOf(Monosaccharide.class));
        assertThat(sugar1.getNode(2), instanceOf(Substituent.class));

       /* String glycoct2 = "RES\n" +
                "1b:o-dgal-HEX-0:0|1:aldi\n" +
                "2s:n-acetyl\n" +
                "3b:b-dgal-HEX-1:5\n" +
                "4b:x-HEX-1:5\n" +
                "5s:n-acetyl\n" +
                "LIN\n" +
                "1:1d(2+1)2n\n" +
                "2:1o(3+1)3d\n" +
                "3:3o(4+1)4d\n" +
                "4:4d(2+1)5n";

        List<String> glycoctArray2 = reader.glycoctToList(glycoct2);

        Glycan sugar2 = reader.glycoctListsToSugar("Id_2",reader.readRES(glycoctArray2), reader.readLIN(glycoctArray2));

        assertThat(sugar2.order(),is(5));
        assertThat(sugar2.getChildren(sugar1.getNode(0)).order(), is(2));
        assertTrue(sugar2.getNode(0).equals(sugar2.getRoot()));
        assertThat(sugar2.getEdgeList().order(), is(4));
        assertThat(sugar2.getNode(3).isMonosaccharide(), is(true));
        assertThat(sugar2.getNode(1).isMonosaccharide(), is(false));
       */
    }

    @Test
    public void testGetSubstituent() throws Exception {
        GlycoCTReader reader = new GlycoCTReader();

        assertThat(reader.getSubstituent("13s:n-acetyl"), is(substituentLookup.getNew("NAcetyl")));
        assertThat(reader.getSubstituent("13s:fluoro"), is(substituentLookup.getNew("Fluoro")));   // flouro -> fluoro?

        assertFalse(reader.getSubstituent("13s:n-acetyl").equals(substituentLookup.getNew("Acetyl")));
    }

    @Test
    public void testGetMonosaccharide() throws Exception {
        GlycoCTReader reader = new GlycoCTReader();

        assertThat(reader.getMonosaccharide("1b:x-dgal-HEX-1:5"), is(monosaccharideLookup.getNew("Gal")));
        assertThat(reader.getMonosaccharide("1b:x-lgal-HEX-1:5|6:d"), is(monosaccharideLookup.getNew("Fuc")));

        assertFalse(reader.getMonosaccharide("1b:x-dgal-HEX-1:5").equals(monosaccharideLookup.getNew("Fuc")));
    }



    @Test
    public void testGetNumericValue() throws Exception {
        GlycoCTReader reader = new GlycoCTReader();
        assertThat(reader.getNumericValue("1b:x-dgal-HEX-1:5",0), is(1));
        assertThat(reader.getNumericValue("1:1o(4+1)2d",0), is(1));
        assertThat(reader.getNumericValue("1:1o(4+1)2d",5), is(4));
        assertThat(reader.getNumericValue("1:1o(4+1)2d",9), is(2));
        assertThat(reader.getNumericValue("3s:n-acetyl", 0), is(3));

        assertThat(reader.getNumericValue("1:1o(-1+1)2d", 5), is(-1));
        assertThat(reader.getNumericValue("13s:n-acetyl", 0), is(13));
    }

    @Test
    public void testGetOptionalInteger() throws Exception {
        GlycoCTReader reader = new GlycoCTReader();
        assertThat(reader.getOptionalInteger(1), is(Optional.of(1)));
        assertTrue(reader.getOptionalInteger(-1).equals(Optional.absent()));
    }

    @Test
    public void testGetOptionalCharacter() throws Exception {
        GlycoCTReader reader = new GlycoCTReader();
        assertThat(reader.getOptionalCharacter('a'), is(Optional.of('a')));
        assertTrue(reader.getOptionalCharacter('x').equals(Optional.absent()));
    }

    @Test
    public void testGetGlycoctIndex() throws Exception {
        GlycoCTReader reader = new GlycoCTReader();
        assertThat(reader.getGlycoctIndex("1b:x-dgal-HEX-1:5"), is(1));
        assertThat(reader.getGlycoctIndex("1:1o(4+1)2d"), is(1));
        assertThat(reader.getGlycoctIndex("3s:n-acetyl"), is(3));

        assertThat(reader.getGlycoctIndex("13s:n-acetyl"), is(13));
    }

    @Test
    public void testGetAnomericResidueId() throws Exception {
        GlycoCTReader reader = new GlycoCTReader();

        assertThat(reader.getAnomericResidueId("1:1o(4+1)2d"), is(2));
        assertThat(reader.getAnomericResidueId("1:4o(-1+1)5d"), is(5));
    }

    @Test
    public void testGetAnomericCarbon() throws Exception {
        GlycoCTReader reader = new GlycoCTReader();

        assertThat(reader.getAnomericCarbon("1:1o(4+1)2d"), is(1));
        assertThat(reader.getAnomericCarbon("1:4o(-1+1)5d"), is(1));
    }

    @Test
    public void testGetAnomericCarbonModification() throws Exception {
        GlycoCTReader reader = new GlycoCTReader();

        assertThat(reader.getAnomericCompsition("1:1o(4+1)2d"), is('d'));
    }

    @Test
    public void testGetLinkedResidueId() throws Exception {
        GlycoCTReader reader = new GlycoCTReader();

        assertThat(reader.getLinkedResidueId("1:1o(4+1)2d"), is(1));
        assertThat(reader.getLinkedResidueId("1:4o(-1+1)5d"), is(4));
    }

    @Test
    public void testGetLinkedCarbon() throws Exception {
        GlycoCTReader reader = new GlycoCTReader();

        assertThat(reader.getLinkedCarbon("1:1o(4+1)2d"), is(4));
        assertThat(reader.getLinkedCarbon("1:4o(-1+-1)5d"), is(-1));
    }

    @Test
    public void testGetLinkedCarbonModification() throws Exception {
        GlycoCTReader reader = new GlycoCTReader();

        assertThat(reader.getLinkedCompsition("1:1o(4+1)2d"), is('o'));
    }

    @Test
    public void testFindLinkages() throws Exception {

        String glycoct = "RES\n" +
                "1b:x-dgal-HEX-1:5\n" +
                "2b:b-dgal-HEX-1:5\n" +
                "3s:n-acetyl\n" +
                "LIN\n" +
                "1:1o(-1+1)2d\n" +
                "2:1d(2+1)3n";

        GlycoCTReader reader = new GlycoCTReader();
        List<String> glycoctList = reader.glycoctToList(glycoct);

        //Map residues = reader.glycoctListToMap(reader.readRES(glycoctList));
        Map<Integer, String> linkages = reader.glycoctListToMap(reader.readLIN(glycoctList));

        Map<Integer,String> foundLinkages1 = reader.findLinkages(linkages,2);
        assertThat(foundLinkages1.size(), is(1));
        assertThat(foundLinkages1.get(1), is("1:1o(-1+1)2d"));

        Map<Integer,String> foundLinkages2 = reader.findLinkages(linkages,3);
        assertThat(foundLinkages2.size(), is(1));
        assertThat(foundLinkages2.get(2), is("2:1d(2+1)3n"));
        assertFalse(foundLinkages2.get(2).equals("1:1o(-1+1)2d"));

    }

    @Test
    public void testGetGlycoctResidueType() throws Exception {
        GlycoCTReader reader = new GlycoCTReader();
        assertThat(reader.getGlycoctResidueType("1b:x-dgal-HEX-1:5"), is('b'));
        assertThat(reader.getGlycoctResidueType("3s:n-acetyl"), is('s'));

        assertThat(reader.getGlycoctResidueType("13s:n-acetyl"), is('s'));
    }

    @Test
    public void testGetGlycoctResidueAnomer() throws Exception {
        GlycoCTReader reader = new GlycoCTReader();
        assertThat(reader.getGlycoctResidueAnomer("1b:x-dgal-HEX-1:5"), is('x'));
        assertThat(reader.getGlycoctResidueAnomer("1b:b-dgal-HEX-1:5"), is('b'));
        //no anomericity on substituent
        assertThat(reader.getGlycoctResidueAnomer("3s:n-acetyl"), is(nullValue()));
    }



    @Test
    public void testReadRES() throws Exception {
        GlycoCTReader reader = new GlycoCTReader();

        List<String> glycoctAl1 = reader.glycoctToList("RES\n" + "1b:x-dgal-HEX-1:5\n" + "2b:x-dgal-HEX-1:5\n" + "LIN\n" + "1:1o(4+1)2d");
        List<String> residues1 = reader.readRES(glycoctAl1);
        assertThat(residues1.size(), is(2));
        assertThat(residues1.get(0),is("1b:x-dgal-HEX-1:5") );
        assertThat(residues1.get(1),is("2b:x-dgal-HEX-1:5") );

        List<String> glycoctAl2 = reader.glycoctToList("RES\n" + "1b:x-dgal-HEX-1:5\n");
        List<String> residues2 = reader.readRES(glycoctAl2);
        assertThat(residues2.size(), is(1));
        assertThat(residues2.get(0),is("1b:x-dgal-HEX-1:5") );


        String glycoct3 = "RES\n" +
                "1b:o-dgal-HEX-0:0|1:aldi\n" +
                "2s:n-acetyl\n" +
                "3b:b-xgal-HEX-1:5\n" +
                "4b:x-HEX-1:5\n" +
                "5s:n-acetyl\n" +
                "LIN\n" +
                "1:1d(2+1)2n\n" +
                "2:1o(3+1)3d\n" +
                "3:3o(4+1)4d\n" +
                "4:4d(2+1)5n";
        List<String> glycoctAl3 = reader.glycoctToList(glycoct3);
        List<String> residues3 = reader.readRES(glycoctAl3);
        assertThat(residues3.size(), is(5));
        assertThat(residues3.get(0),is("1b:o-dgal-HEX-0:0|1:aldi") );


    }

    @Test
    public void testReadLIN() throws Exception {
        GlycoCTReader reader = new GlycoCTReader();

        List<String> glycoctAl1 = reader.glycoctToList("RES\n" + "1b:x-dgal-HEX-1:5\n" + "2b:x-dgal-HEX-1:5\n" + "LIN\n" + "1:1o(4+1)2d");
        List<String> linkages1 = reader.readLIN(glycoctAl1);
        assertThat(linkages1.size(), is(1));
        assertThat(linkages1.get(0),is("1:1o(4+1)2d") );

        List<String> glycoctAl2 = reader.glycoctToList("RES\n" + "1b:x-dgal-HEX-1:5\n");
        List<String> linkages2 = reader.readLIN(glycoctAl2);
        assertThat(linkages2.size(), is(0));

        String glycoct3 = "RES\n" +
                "1b:o-dgal-HEX-0:0|1:aldi\n" +
                "2s:n-acetyl\n" +
                "3b:b-xgal-HEX-1:5\n" +
                "4b:x-HEX-1:5\n" +
                "5s:n-acetyl\n" +
                "LIN\n" +
                "1:1d(2+1)2n\n" +
                "2:1o(3+1)3d\n" +
                "3:3o(4+1)4d\n" +
                "4:4d(2+1)5n";
        List<String> glycoctAl3 = reader.glycoctToList(glycoct3);
        List<String> linkages3 = reader.readLIN(glycoctAl3);
        assertThat(linkages3.size(), is(4));
    }

    @Test
    public void testGlycoctToArrayList() throws Exception {
        GlycoCTReader reader = new GlycoCTReader();
        List glycoctAl1 = reader.glycoctToList("RES\n" + "1b:x-dgal-HEX-1:5");
        assertThat(glycoctAl1.size(),is(2) );
        assertThat(glycoctAl1.get(0).toString(),is("RES") );
        assertThat(glycoctAl1.get(1).toString(),is("1b:x-dgal-HEX-1:5") );
        assertNotEquals(glycoctAl1.get(1).toString(), "1b:x-dgal-HEX-1:6");
    }

    @Test
    public void testGetStartIndex() throws Exception {
        GlycoCTReader reader = new GlycoCTReader();
        List<String> glycoctAl1 = reader.glycoctToList("RES\n" + "1b:x-dgal-HEX-1:5");
        assertThat(reader.getStartIndex(glycoctAl1, "RES"), is(1));
    }

    @Test
    public void testGetStopIndex() throws Exception {
        GlycoCTReader reader = new GlycoCTReader();

        // stopTag NOT found
        List<String> glycoctAl2 = reader.glycoctToList("RES\n" + "1b:x-dgal-HEX-1:5");
        assertThat(reader.getStopIndex(glycoctAl2,"LIN"),is(1) );

        // stopTag found
        List<String> glycoctAl1 = reader.glycoctToList("RES\n" + "1b:x-dgal-HEX-1:5\n" + "2b:x-dgal-HEX-1:5\n" + "LIN\n" + "1:1o(4+1)2d");
        assertThat(reader.getStopIndex(glycoctAl1, "LIN"),is(2) );
    }


    @Test
    public void testFucoseReader() throws Exception{

        GlycoCTReader reader = new GlycoCTReader();
        Glycan glycan = reader.read("RES\n" +
                "1b:x-lgal-HEX-1:5|6:d\n", "");

        assertEquals(1,glycan.size());
        GlycanNode node =  glycan.getNode(0);
        assertEquals("Fuc",node.getName());
    }
}
