package org.expasy.mzjava.glycomics.mol;

import org.expasy.mzjava.core.mol.Composition;
import org.junit.Assert;
import org.junit.Test;

import java.io.StringReader;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class SubstituentJsonParserTest {



    @Test
    public void testParse() throws Exception {
        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Acetyl\",\n" +
                "\t\t\"composition\": \"COCH3\"\n" +
                "\t}" +
                "]");

        final DefaultSubstituentLookup substituentLookup = new DefaultSubstituentLookup();
        final Map<String, Substituent> substMap =new LinkedHashMap<String, Substituent>();
        substituentLookup.parse(sr, substMap);
        Assert.assertEquals(substMap.size(), 1);
        Assert.assertEquals(substMap.get("Acetyl"),new Substituent("Acetyl", Composition.parseComposition("COCH3")));
    }

    @Test (expected = NullPointerException.class)
    public void testNoInput() throws Exception {
        final DefaultSubstituentLookup substituentLookup = new DefaultSubstituentLookup();
        final Map<String, Substituent> substMap =new LinkedHashMap<String, Substituent>();
        substituentLookup.parse(null, substMap);
        Assert.assertEquals(substMap.size(), 1);
        Assert.assertEquals(substMap.get("Acetyl"),new Substituent("Acetyl", Composition.parseComposition("COCH3")));
    }

    @Test (expected = NullPointerException.class)
    public void testNoInput2() throws Exception {
        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Acetyl\",\n" +
                "\t\t\"composition\": \"COCH3\"\n" +
                "\t}" +
                "]");

        final DefaultSubstituentLookup substituentLookup = new DefaultSubstituentLookup();
        final Map<String, Substituent> substMap =new LinkedHashMap<String, Substituent>();
        substituentLookup.parse(sr, null);
        Assert.assertEquals(substMap.size(), 1);
        Assert.assertEquals(substMap.get("Acetyl"),new Substituent("Acetyl", Composition.parseComposition("COCH3")));
    }


    @Test(expected = IllegalStateException.class)
    public void testEmptyCompostion() throws Exception {
        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Seragio\",\n" +
                "\t\t\"composition\": \"\"\n" +
                "\t}" +
                "]");
        final DefaultSubstituentLookup substituentLookup = new DefaultSubstituentLookup();
        final Map<String, Substituent> substMap =new LinkedHashMap<String, Substituent>();
        substituentLookup.parse(sr, substMap);
        Assert.assertEquals(substMap.size(), 1);
        Assert.assertEquals(substMap.get("Acetyl"),new Substituent("Acetyl", Composition.parseComposition("COCH3")));
    }


    @Test (expected = IllegalStateException.class)
    public void testNoComp() throws Exception {
        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Acetyl\"\n" +
                "\t}" +
                "]");
        final DefaultSubstituentLookup substituentLookup = new DefaultSubstituentLookup();
        final Map<String, Substituent> substMap =new LinkedHashMap<String, Substituent>();
        substituentLookup.parse(sr, substMap);
        Assert.assertEquals(substMap.size(), 1);
        Assert.assertEquals(substMap.get("Acetyl"),new Substituent("Acetyl", Composition.parseComposition("COCH3")));
    }

    @Test(expected = IllegalStateException.class)
    public void testNoName() throws Exception {
        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"composition\": \"COCH3\"\n" +
                "\t}" +
                "]");
        final DefaultSubstituentLookup substituentLookup = new DefaultSubstituentLookup();
        final Map<String, Substituent> substMap =new LinkedHashMap<String, Substituent>();
        substituentLookup.parse(sr, substMap);
        Assert.assertEquals(substMap.size(), 1);
        Assert.assertEquals(substMap.get("Acetyl"),new Substituent("Acetyl", Composition.parseComposition("COCH3")));
    }


    @Test(expected = IllegalStateException.class)
    public void testWrongField() throws Exception {
        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"namdse\": \"Acetyl\",\n" +
                "\t\t\"test\": \"COCH3\",\n" +
                "\t\t\"compos43ition\": \"COCH3\"\n" +
                "\t}" +
                "]");
        final DefaultSubstituentLookup substituentLookup = new DefaultSubstituentLookup();
        final Map<String, Substituent> substMap =new LinkedHashMap<String, Substituent>();
        substituentLookup.parse(sr, substMap);
        Assert.assertEquals(substMap.size(), 1);
        Assert.assertEquals(substMap.get("Acetyl"),new Substituent("Acetyl", Composition.parseComposition("COCH3")));
    }

    @Test
    public void testMultiplefield() throws Exception {
        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Acetyl\",\n" +
                "\t\t\"test\": \"COCH3\",\n" +
                "\t\t\"composition\": \"COCH3\"\n" +
                "\t}" +
                "]");
        final DefaultSubstituentLookup substituentLookup = new DefaultSubstituentLookup();
        final Map<String, Substituent> substMap =new LinkedHashMap<String, Substituent>();
        substituentLookup.parse(sr, substMap);
        Assert.assertEquals(substMap.size(), 1);
        Assert.assertEquals(substMap.get("Acetyl"),new Substituent("Acetyl", Composition.parseComposition("COCH3")));
    }
}
