/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.glycomics.mol;

import org.junit.Test;
import org.openide.util.Lookup;

import java.util.List;
import java.util.Random;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class SNodeTest {
    private final SubstituentLookup substituentLookup = Lookup.getDefault().lookup(SubstituentLookup.class);
    private final MonosaccharideLookup monosaccharideLookup = Lookup.getDefault().lookup(MonosaccharideLookup.class);



    @Test
    public void testIsMonosaccharide() throws Exception {

        SNode monosaccharide = new SNode<Monosaccharide>(12, monosaccharideLookup.getNew("Glc"));
        assertThat(monosaccharide.isMonosaccharide(), is(true));

        SNode substituent = new SNode<Substituent>(13, substituentLookup.getNew("NAcetyl"));
        assertThat(substituent.isMonosaccharide(), is(false));
    }


    @Test
    public void testIsSubstituent() throws Exception {

        SNode monosaccharide = new SNode<Monosaccharide>(12, monosaccharideLookup.getNew("Glc"));
        assertThat(monosaccharide.isSubstituent(), is(false));

        SNode substituent = new SNode<Substituent>(13, substituentLookup.getNew("NAcetyl"));
        assertThat(substituent.isSubstituent(), is(true));
    }

    @Test
    public void testEqualsAndHash() throws Exception {

        SNode<Monosaccharide> node1 = new SNode<Monosaccharide>(12, monosaccharideLookup.getNew("GalA"));
        SNode<Monosaccharide> node2 = new SNode<Monosaccharide>(12, monosaccharideLookup.getNew("ManA"));
        SNode<Substituent> node3 = new SNode<Substituent>(12, substituentLookup.getNew("Ethanolamine"));

        SNode<Monosaccharide> node11 = new SNode<Monosaccharide>(11, monosaccharideLookup.getNew("GalA"));
        SNode<Substituent> node31 = new SNode<Substituent>(11, substituentLookup.getNew("Ethanolamine"));

        assertThat(node1.equals(node2), is(true));
        assertThat(node2.equals(node1), is(true));
        assertEquals(node1.hashCode(), node2.hashCode());

        assertThat(node1.equals(node3), is(true));
        assertThat(node3.equals(node1), is(true));
        assertEquals(node1.hashCode(), node3.hashCode());

        assertThat(node1.equals(node11), is(false));
        assertThat(node11.equals(node1), is(false));
        assertNotEquals(node1.hashCode(), node11.hashCode());

        assertThat(node3.equals(node31), is(false));
        assertThat(node31.equals(node3), is(false));
        assertNotEquals(node3.hashCode(), node31.hashCode());
    }

    @Test
    public void testCompareTo() throws Exception {

        Random random = new Random();
        List<Monosaccharide> monosaccharides = monosaccharideLookup.values();
        for(int i = 0; i < 10; i++) {

            int id1 = random.nextInt(100);
            int id2 = random.nextInt(100);

            SNode<Monosaccharide> node1 = new SNode<Monosaccharide>(id1, monosaccharides.get(random.nextInt(monosaccharides.size())));
            SNode<Monosaccharide> node2 = new SNode<Monosaccharide>(id2, monosaccharides.get(random.nextInt(monosaccharides.size())));

            assertEquals(Integer.compare(id1, id2), node1.compareTo(node2));
        }
    }

    @Test
    public void testToString() throws Exception {

        SNode<Substituent> node = new SNode<Substituent>(23, substituentLookup.getNew("Chloro"));

        assertEquals("SNode{id=23, element=Substituent{name='Chloro', composition=Cl}}", node.toString());
    }
}