package org.expasy.mzjava.glycomics.mol;


import org.expasy.mzjava.core.mol.Composition;
import org.junit.Assert;
import org.junit.Test;
import org.openide.util.Lookup;

import java.io.StringReader;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class InternaCleavageJsonParserTest {

    private final MonosaccharideLookup monosaccharideLookup = Lookup.getDefault().lookup(MonosaccharideLookup.class);


    @Test
    public void testGoodEntry() throws Exception{

        final Map<Monosaccharide, Map<CutIndexes,Composition>> compMap = new LinkedHashMap<Monosaccharide, Map<CutIndexes, Composition>>();

        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Ara\",\n" +
                "\t\t\"composition\": \"C5H10O5\",\n" +
                "\t\t\"ring\": \"f\",\n" +
                "\t\t\"superclass\": \"Pentose\",\n" +
                "\t\t\"class\": \"Pen\",\n" +
                "\t\t\"deltaMass\": [\n" +
                "                    {\n" +
                "                        \"ionType\" : \"x\",\n" +
                "                        \"cleavage\" : [0, 2],\n" +
                "                        \"composition\" : \"C-3H-6O-3\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"a\",\n" +
                "                        \"cleavage\" : [0, 2],\n" +
                "                        \"composition\" : \"C-2H-4O-2\"\n" +
                "                    }\n" +
                "]}\n" +
                "]");

        DefaultGlycanMassCalculator.InternalCleavageJsonParser parser = new DefaultGlycanMassCalculator.InternalCleavageJsonParser();
        parser.parse(sr,compMap);
        Assert.assertTrue(compMap.containsKey(monosaccharideLookup.getNew("Ara")));
        Map<CutIndexes,Composition> aCleavedCompMap = compMap.get(monosaccharideLookup.getNew("Ara"));
        Assert.assertEquals(1,aCleavedCompMap.size());
        Assert.assertTrue(aCleavedCompMap.containsValue(Composition.parseComposition("C-2H-4O-2")));


    }



    @Test (expected = IllegalStateException.class)
    public void testNoName() throws Exception{
        final Map<Monosaccharide, Map<CutIndexes,Composition>> compMap = new LinkedHashMap<Monosaccharide, Map<CutIndexes, Composition>>();

        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"composition\": \"C5H10O5\",\n" +
                "\t\t\"ring\": \"f\",\n" +
                "\t\t\"superclass\": \"Pentose\",\n" +
                "\t\t\"class\": \"Pen\",\n" +
                "\t\t\"deltaMass\": [\n" +
                "                    {\n" +
                "                        \"cleavage\" : [0, 2],\n" +
                "                        \"composition\" : \"C-3H-6O-3\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"cleavage\" : [0, 2],\n" +
                "                        \"composition\" : \"C-2H-4O-2\"\n" +
                "                    }\n" +
                "]}\n" +
                "]");


        DefaultGlycanMassCalculator.InternalCleavageJsonParser parser = new DefaultGlycanMassCalculator.InternalCleavageJsonParser();
        parser.parse(sr,compMap);

    }

    @Test (expected = IllegalStateException.class)
    public void testNoCleavage() throws Exception{
        final Map<Monosaccharide, Map<CutIndexes,Composition>> compMap = new LinkedHashMap<Monosaccharide, Map<CutIndexes, Composition>>();

        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Ara\",\n" +
                "\t\t\"composition\": \"C5H10O5\",\n" +
                "\t\t\"ring\": \"f\",\n" +
                "\t\t\"superclass\": \"Pentose\",\n" +
                "\t\t\"class\": \"Pen\",\n" +
                "\t\t\"deltaMass\": [\n" +
                "                    {\n" +
                "                        \"cleavage\" : \n" +
                "                        \"composition\" : \"C-3H-6O-3\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"cleavage\" : [0, 2],\n" +
                "                        \"composition\" : \"C-2H-4O-2\"\n" +
                "                    }\n" +
                "]}\n" +
                "]");

        DefaultGlycanMassCalculator.InternalCleavageJsonParser parser = new DefaultGlycanMassCalculator.InternalCleavageJsonParser();
        parser.parse(sr,compMap);

    }


    @Test (expected = IllegalStateException.class)
    public void testNoComp() throws Exception{
        final Map<Monosaccharide, Map<CutIndexes,Composition>> compMap = new LinkedHashMap<Monosaccharide, Map<CutIndexes, Composition>>();
        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Ara\",\n" +
                "\t\t\"composition\": \"C5H10O5\",\n" +
                "\t\t\"ring\": \"f\",\n" +
                "\t\t\"superclass\": \"Pentose\",\n" +
                "\t\t\"class\": \"Pen\",\n" +
                "\t\t\"deltaMass\": [\n" +
                "                    {\n" +
                "                        \"cleavage\" : [0, 2],\n" +
                "                        \"composition\" : \n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"cleavage\" : [0, 2],\n" +
                "                        \"composition\" : \"C-2H-4O-2\"\n" +
                "                    }\n" +
                "]}\n" +
                "]");

        DefaultGlycanMassCalculator.InternalCleavageJsonParser parser = new DefaultGlycanMassCalculator.InternalCleavageJsonParser();
        parser.parse(sr,compMap);

    }

    @Test (expected = IllegalStateException.class)
    public void testWrongCleavage() throws Exception{
        final Map<Monosaccharide, Map<CutIndexes,Composition>> compMap = new LinkedHashMap<Monosaccharide, Map<CutIndexes, Composition>>();

        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Ara\",\n" +
                "\t\t\"composition\": \"C5H10O5\",\n" +
                "\t\t\"ring\": \"f\",\n" +
                "\t\t\"superclass\": \"Pentose\",\n" +
                "\t\t\"class\": \"Pen\",\n" +
                "\t\t\"deltaMass\": [\n" +
                "                    {\n" +
                "                        \"cleavage\" : [0, 2, 5],\n" +
                "                        \"composition\" : \"C-3H-6O-3\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"cleavage\" : [0, 2],\n" +
                "                        \"composition\" : \"C-2H-4O-2\"\n" +
                "                    }\n" +
                "]}\n" +
                "]");

        DefaultGlycanMassCalculator.InternalCleavageJsonParser parser = new DefaultGlycanMassCalculator.InternalCleavageJsonParser();
        parser.parse(sr,compMap);

    }

    @Test (expected = IllegalStateException.class)
    public void testWrongCleavage2() throws Exception{
        final Map<Monosaccharide, Map<CutIndexes,Composition>> compMap = new LinkedHashMap<Monosaccharide, Map<CutIndexes, Composition>>();

        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Ara\",\n" +
                "\t\t\"composition\": \"C5H10O5\",\n" +
                "\t\t\"ring\": \"f\",\n" +
                "\t\t\"superclass\": \"Pentose\",\n" +
                "\t\t\"class\": \"Pen\",\n" +
                "\t\t\"deltaMass\": [\n" +
                "                    {\n" +
                "                        \"cleavage\" : [0],\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"cleavage\" : [0, 2],\n" +
                "                        \"composition\" : \"C-2H-4O-2\"\n" +
                "                    }\n" +
                "]}\n" +
                "]");

        DefaultGlycanMassCalculator.InternalCleavageJsonParser parser = new DefaultGlycanMassCalculator.InternalCleavageJsonParser();
        parser.parse(sr,compMap);
    }

    @Test (expected = IllegalStateException.class)
    public void testEmpyCleavage() throws Exception{
        final Map<Monosaccharide, Map<CutIndexes,Composition>> compMap = new LinkedHashMap<Monosaccharide, Map<CutIndexes, Composition>>();

        StringReader sr = new StringReader("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Ara\",\n" +
                "\t\t\"composition\": \"C5H10O5\",\n" +
                "\t\t\"ring\": \"f\",\n" +
                "\t\t\"superclass\": \"Pentose\",\n" +
                "\t\t\"class\": \"Pen\",\n" +
                "\t\t\"deltaMass\": [\n" +
                "                    {\n" +
                "                        \"cleavage\" : ,\n" +
                "                        \"composition\" : \"C-2H-4O-2\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"cleavage\" : [0, 2],\n" +
                "                        \"composition\" : \"C-2H-4O-2\"\n" +
                "                    }\n" +
                "]}\n" +
                "]");

        DefaultGlycanMassCalculator.InternalCleavageJsonParser parser = new DefaultGlycanMassCalculator.InternalCleavageJsonParser();
        parser.parse(sr,compMap);
    }
}
