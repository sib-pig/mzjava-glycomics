package org.expasy.mzjava.glycomics.ms.fragment;

import com.google.common.base.Optional;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.expasy.mzjava.glycomics.io.mol.glycoct.GlycoCTReader;
import org.expasy.mzjava.glycomics.mol.*;
import org.expasy.mzjava.glycomics.ms.spectrum.GlycanSpectrum;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.openide.util.Lookup;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class GlycanFragmenterTest {

    private final SubstituentLookup substituentLookup = Lookup.getDefault().lookup(SubstituentLookup.class);
    private final MonosaccharideLookup monosaccharideLookup = Lookup.getDefault().lookup(MonosaccharideLookup.class);
    private final CutDescriptorGenerator cutDescriptorGenerator = new CutDescriptorGenerator();




    @Test @Ignore
    public void testFragment() throws Exception {
        Set<IonType> ionTypes = new HashSet<IonType>();
        ionTypes.add(IonType.p);
        ionTypes.add(IonType.b);
        ionTypes.add(IonType.c);
        ionTypes.add(IonType.y);
        ionTypes.add(IonType.z);
        ionTypes.add(IonType.a);
        ionTypes.add(IonType.x);

        GlycanFragmenter glycanFragmenter = new GlycanFragmenter(ionTypes, true, true, PeakList.Precision.DOUBLE, 2,2);

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Gal"), Optional.<Anomericity>absent(), "Id_1");
        Substituent sub0 = builder.add(substituentLookup.getNew("NAcetyl"), node0, new SubstituentLinkage(2, Composition.parseComposition("O-1H-1")));
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.beta, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Fuc"), node1, new GlycosidicLinkage(Anomericity.alpha, 1, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar = builder.build();

        GlycanSpectrum spectrum = glycanFragmenter.fragment(sugar, 1);

        List<GlycanFragment> fragmentList = new ArrayList<GlycanFragment>();
        List<CutDescriptor> descriptors = cutDescriptorGenerator.generateCutDescriptor(sugar,2, 2);
        Map<Double, List<GlycanFragment>> testMap = new TreeMap<Double, List<GlycanFragment>>();

        for(CutDescriptor descriptor : descriptors){
            fragmentList.addAll(sugar.getFragment(descriptor));
        }

        DefaultGlycanMassCalculator glycanMassCalculator = new DefaultGlycanMassCalculator();

        for(GlycanFragment fragment : fragmentList){
            double mass = glycanMassCalculator.calculateMz(fragment.calculateMolecularMass(), 1, fragment.getFragmentType());

            if( testMap.containsKey(mass)){
                testMap.get(mass).add(fragment);
            } else {
                List<GlycanFragment> tempList = new ArrayList<GlycanFragment>();
                tempList.add(fragment);
                testMap.put(mass,tempList);
            }
        }



        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(145.05063237599998)),145.05063237599998,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(163.061197062)),163.061197062,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(205.071761748)),205.071761748,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(220.082660785)),220.082660785,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(235.082326434)),235.082326434,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(248.07757540699998)),248.07757540699998,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(279.10854118399993)),279.10854118399993,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(292.10379015699993)),292.10379015699993,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(307.10345580599994)),307.10345580599994,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(322.11435484299994)),322.11435484299994,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(325.11402049199995)),325.11402049199995,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(364.12491952899995)),364.12491952899995,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(367.12458517799996)),367.12458517799996,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(382.13548421499996)),382.13548421499996,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(408.15113427899996)),408.15113427899996,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(410.1303988369999)),410.1303988369999,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(411.15079992799997)),411.15079992799997,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(424.146048901)),424.146048901,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(427.14571455)),427.14571455,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(438.1616989649999)),438.1616989649999,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(440.140963523)),440.140963523,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(454.1566135869999)),454.1566135869999,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(468.172263651)),468.172263651,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(470.15152820899993)),470.15152820899993,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(482.1879137149999)),482.1879137149999,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(484.16717827299993)),484.16717827299993,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(528.193393023)),528.193393023,0.0001);
    }

    @Test @Ignore
    public void testWithGWB() throws Exception {

        Set<IonType> ionTypes = new HashSet<IonType>();
        ionTypes.add(IonType.p);
        ionTypes.add(IonType.b);
        ionTypes.add(IonType.c);
        ionTypes.add(IonType.y);
        ionTypes.add(IonType.z);
        ionTypes.add(IonType.a);
        ionTypes.add(IonType.x);


        GlycanFragmenter glycanFragmenter = new GlycanFragmenter(ionTypes, true,true, PeakList.Precision.DOUBLE,2 ,2);
        GlycoCTReader glycoCTReader = new GlycoCTReader();
        InputStream streamFile = this.getClass().getResourceAsStream("fragmentTest.json");

        ObjectMapper mapper = new ObjectMapper();
        JsonNode glycanArray;

        try {
            glycanArray = mapper.readTree(streamFile);
        } catch (IOException e) {
            throw new IllegalStateException("Monosaccharide Lookup file error", e);
        }
        String glycoCtCode = "glycoCtCode";
        String spectrumString = "spectrum";
        int count = 0;

        for (final JsonNode glycanNode : glycanArray) {

            JsonNode glycoCTNode = glycanNode.path(glycoCtCode);
            JsonNode spectrumNode = glycanNode.path(spectrumString);
            GlycanSpectrum spectrum = glycanFragmenter.fragment(glycoCTReader.read(glycoCTNode.getTextValue(),""), 2);
            System.out.println(count);
            count++;
            if (spectrumNode.isArray()) {
                for (final JsonNode mzNode : spectrumNode) {
                    Double peakMass = spectrum.getMz(spectrum.getClosestIndex(mzNode.getDoubleValue()));
                    Assert.assertEquals(peakMass,mzNode.getDoubleValue(),0.000001);
                }
            }


        }
    }


    @Test @Ignore
    public void testFragment2Charge() throws Exception {
        Set<IonType> ionTypes = new HashSet<IonType>();
        ionTypes.add(IonType.p);
        ionTypes.add(IonType.b);
        ionTypes.add(IonType.c);
        ionTypes.add(IonType.y);
        ionTypes.add(IonType.z);
        ionTypes.add(IonType.a);
        ionTypes.add(IonType.x);

        GlycanFragmenter glycanFragmenter = new GlycanFragmenter(ionTypes, true,true,PeakList.Precision.DOUBLE,1 ,1);

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Gal"), Optional.<Anomericity>absent(), "Id_1");
        Substituent sub0 = builder.add(substituentLookup.getNew("NAcetyl"), node0, new SubstituentLinkage(2, Composition.parseComposition("O-1H-1")));
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.beta, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Fuc"), node1, new GlycosidicLinkage(Anomericity.alpha, 1, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar = builder.build();

        GlycanSpectrum spectrum = glycanFragmenter.fragment(sugar, 2);


        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(72.0217)),72.0217,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(81.0270)),81.0270,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(100.5324)),100.53241015779999,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(102.0322)),102.0322,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(109.5377)),109.5377,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(117.0375)),117.0375,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(123.5351)),123.5351,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(139.0506)),139.0506,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(145.05063237599998)),145.05063237599998,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(145.5483)),145.5483,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(153.0481)),153.0481,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(160.5535)),160.5535,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(162.0534)),162.0534,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(163.061197062)),163.061197062,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(181.5588)),181.5588,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(183.0587)),183.0587,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(190.5641)),190.5641,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(202.072096099)),202.072096099,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(203.5719)),203.5719,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(204.5616)),204.5616,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(205.071761748)),205.071761748,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(211.5694)),211.5694,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(213.0692)),213.0692,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(218.5772)),218.5772,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(219.5668)),219.5668,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(220.082660785)),220.082660785,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(226.5747)),226.5747,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(233.5825)),233.5825,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(234.5721)),234.5721,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(235.082326434)),235.082326434,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(240.5903)),240.5903,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(241.5800)),241.5800,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(248.07757540699998)),248.07757540699998,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(263.5931)),263.5931,0.0001);

        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(279.10854118399993)),279.10854118399993,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(292.10379015699993)),292.10379015699993,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(307.10345580599994)),307.10345580599994,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(322.11435484299994)),322.11435484299994,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(325.11402049199995)),325.11402049199995,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(364.12491952899995)),364.12491952899995,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(367.12458517799996)),367.12458517799996,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(382.13548421499996)),382.13548421499996,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(408.15113427899996)),408.15113427899996,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(410.1303988369999)),410.1303988369999,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(411.15079992799997)),411.15079992799997,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(424.146048901)),424.146048901,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(427.14571455)),427.14571455,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(438.1616989649999)),438.1616989649999,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(440.140963523)),440.140963523,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(454.1566135869999)),454.1566135869999,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(468.172263651)),468.172263651,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(470.15152820899993)),470.15152820899993,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(482.1879137149999)),482.1879137149999,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(484.16717827299993)),484.16717827299993,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(528.193393023)),528.193393023,0.0001);

    }


    @Test
    public void testFragmentTripleGlycosidic() throws Exception {

        Set<IonType> ionTypes = new HashSet<IonType>();
        ionTypes.add(IonType.p);
        ionTypes.add(IonType.b);
        ionTypes.add(IonType.c);
        ionTypes.add(IonType.y);
        ionTypes.add(IonType.z);
        ionTypes.add(IonType.a);
        ionTypes.add(IonType.x);


        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Gal"), Optional.<Anomericity>absent(), "Id_1");
        Substituent sub0 = builder.add(substituentLookup.getNew("NAcetyl"), node0, new SubstituentLinkage(2, Composition.parseComposition("O-1H-1")));
        GlycosidicLinkage linkage0 = new GlycosidicLinkage(Anomericity.beta, 1, 4, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1"));
        Monosaccharide node3 = builder.add(monosaccharideLookup.getNew("Gal"), node0, linkage0);
        GlycosidicLinkage linkage1 = new GlycosidicLinkage(Anomericity.alpha, 1, 6, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1"));
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Gal"), node3, linkage1);
        GlycosidicLinkage linkage2 = new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1"));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Fuc"), node3, linkage2);
        Glycan sugar = builder.build();

        GlycanFragmenter glycanFragmenter = new GlycanFragmenter(ionTypes, true, true, PeakList.Precision.DOUBLE,3 ,0);
        GlycanSpectrum spectrum = glycanFragmenter.fragment(sugar, 1);

        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(125.0244)),125.0244,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(143.03498231199998)),143.03498231199998,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(145.05063237599998)),145.05063237599998,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(161.045546998)),161.045546998,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(163.061197062)),163.061197062,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(179.056111684)),179.056111684,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(202.072096099)),202.072096099,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(220.082660785)),220.082660785,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(289.09289111999993)),289.09289111999993,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(305.08780574199994)),305.08780574199994,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(307.10345580599994)),307.10345580599994,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(323.09837042799995)),323.09837042799995,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(325.11402049199995)),325.11402049199995,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(341.10893511399996)),341.10893511399996,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(346.11435484299994)),346.11435484299994,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(364.12491952899995)),364.12491952899995,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(382.13548421499996)),382.13548421499996,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(469.15627923599993)),469.15627923599993,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(487.16684392199994)),487.16684392199994,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(510.18282833699993)),510.18282833699993,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(526.1777429589999)),526.1777429589999,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(528.193393023)),528.193393023,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(544.1883076449999)),544.1883076449999,0.0001);
        Assert.assertEquals(spectrum.getMz(spectrum.getClosestIndex(690.246216453)),690.246216453,0.0001);

        /*List<GlycanFragment> fragmentList = new ArrayList<GlycanFragment>();
        List<CutDescriptor> descriptors = sugar.generateCutDescriptor(3, 3);
        Map<Double, List<GlycanFragment>> testMap = new TreeMap<Double, List<GlycanFragment>>();

        for(CutDescriptor descriptor : descriptors){
                fragmentList.addAll(sugar.getFragment(descriptor));
        }
        DefaultGlycanMassCalculator glycanMassCalculator = new DefaultGlycanMassCalculator();
        List<GlycanFragment> test = new ArrayList<GlycanFragment>();
        for(GlycanFragment fragment : fragmentList) {
            double mass = glycanMassCalculator.calculateMz(fragment.calculateMolecularMass(), 1, fragment.getFragmentType());
            if (mass > 161 && mass < 161.01) {
                test.add(fragment);
            }
                if (testMap.containsKey(mass)) {
                    testMap.get(mass).add(fragment);
                } else {
                    List<GlycanFragment> tempList = new ArrayList<GlycanFragment>();
                    tempList.add(fragment);
                    testMap.put(mass, tempList);
                }

        }
        System.out.println();*/

    }



    @Test
    public void testFragmentTripleGlycosidicCrossring() throws Exception {

        Set<IonType> ionTypes = new HashSet<IonType>();
        ionTypes.add(IonType.p);
        ionTypes.add(IonType.b);
        ionTypes.add(IonType.c);
        ionTypes.add(IonType.y);
        ionTypes.add(IonType.z);
        ionTypes.add(IonType.a);
        ionTypes.add(IonType.x);

        Set<FragmentType> fragmentTypes = new HashSet<FragmentType>();
        fragmentTypes.add(FragmentType.FORWARD);
        fragmentTypes.add(FragmentType.REVERSE);
        fragmentTypes.add(FragmentType.INTACT);
        fragmentTypes.add(FragmentType.INTERNAL);



        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Gal"), Optional.<Anomericity>absent(), "Id_1");
        Substituent sub0 = builder.add(substituentLookup.getNew("NAcetyl"), node0, new SubstituentLinkage(2, Composition.parseComposition("O-1H-1")));
        Monosaccharide node3 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.beta, 1, 4, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Gal"), node3, new GlycosidicLinkage(Anomericity.alpha, 1, 6, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Fuc"), node3, new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar = builder.build();


        List<GlycanFragment> fragmentList = new ArrayList<GlycanFragment>();
        List<CutDescriptor> descriptors = cutDescriptorGenerator.generateCutDescriptor(sugar, 2, 1);
        Map<Double, List<GlycanFragment>> testMap = new TreeMap<Double, List<GlycanFragment>>();

        for(CutDescriptor descriptor : descriptors){
            fragmentList.addAll(sugar.getFragment(descriptor));
        }

        DefaultGlycanMassCalculator glycanMassCalculator = new DefaultGlycanMassCalculator();

        for(GlycanFragment fragment : fragmentList){
            double mass = glycanMassCalculator.calculateMz(fragment.calculateMolecularMass(), 1, fragment.getFragmentType());

            if( testMap.containsKey(mass)){
                testMap.get(mass).add(fragment);
            } else {
                List<GlycanFragment> tempList = new ArrayList<GlycanFragment>();
                tempList.add(fragment);
                testMap.put(mass,tempList);
            }
        }
    }



    @Test
    public void testDifferentMass() throws Exception {

        GlycanFragment.Builder builder = new GlycanFragment.Builder();
        Monosaccharide root = monosaccharideLookup.getNew("Gal");
        builder.setCleavedRoot(root,FragmentType.INTERNAL,new CutIndexes(2,5));
        builder.addCleavedMonosaccharide(monosaccharideLookup.getNew("Gal"),root,new GlycosidicLinkage(Anomericity.alpha, 1, 6, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")),new CutIndexes(2,5),IonType.x);
        builder.addCleavedMonosaccharide(monosaccharideLookup.getNew("Fuc"),root,new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")),new CutIndexes(1,5),IonType.x);
        GlycanFragment fragment1 = builder.build();

        GlycanFragment.Builder builder2 = new GlycanFragment.Builder();
        Monosaccharide root2 = monosaccharideLookup.getNew("Gal");
        builder2.setCleavedRoot(root2,FragmentType.INTERNAL,new CutIndexes(2,5));
        builder2.addCleavedMonosaccharide(monosaccharideLookup.getNew("Gal"),root2,new GlycosidicLinkage(Anomericity.alpha, 1, 6, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")),new CutIndexes(2,5),IonType.x);
        builder2.addCleavedMonosaccharide(monosaccharideLookup.getNew("Fuc"),root2,new GlycosidicLinkage(Anomericity.alpha, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")),new CutIndexes(1,4),IonType.x);
        GlycanFragment fragment2 = builder2.build();


        GlycanFragment.Builder builder3 = new GlycanFragment.Builder();
        Monosaccharide root3 = monosaccharideLookup.getNew("Gal");
        builder3.setCleavedRoot(root3,FragmentType.INTERNAL,new CutIndexes(0,4));
        builder3.addCleavedMonosaccharide(monosaccharideLookup.getNew("Kdn"),root3,new GlycosidicLinkage(Anomericity.alpha, 2, 6, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")),new CutIndexes(1,5),IonType.x);
        GlycanFragment fragment3 = builder3.build();
        DefaultGlycanMassCalculator glycanMassCalculator = new DefaultGlycanMassCalculator();
        double massfragment3 = glycanMassCalculator.calculateMz(fragment3.calculateMolecularMass(), 2, fragment3.getFragmentType());

        Assert.assertNotEquals(massfragment3, fragment2.calculateMolecularMass(), 0.0000001);
        Assert.assertNotEquals(fragment1.calculateMolecularMass(), fragment2.calculateMolecularMass(), 0.0000001);
    }

    @Test
    public void test675() throws Exception {

        final Glycan glycan = new GlycoCTReader().read("RES\n" +
                "1b:o-HEX-0:0|1:aldi\n" +
                "2b:x-HEX-1:5\n" +
                "3b:x-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n" +
                "4s:n-acetyl\n" +
                "5s:n-acetyl\n" +
                "LIN\n" +
                "1:1o(-1+1)2d\n" +
                "2:1o(-1+2)3d\n" +
                "3:3d(5+1)4n\n" +
                "4:1d(2+1)5n", "test");

        final GlycanFragmenter fragmenter = new GlycanFragmenter(EnumSet.of(IonType.b, IonType.y), true, false, PeakList.Precision.FLOAT, 1, 0);

        GlycanSpectrum spectrum = fragmenter.fragment(glycan, 1);

        Assert.assertEquals(5, spectrum.size());
        Assert.assertEquals(161.05, spectrum.getMz(0), 0.01);
        Assert.assertEquals(290.09, spectrum.getMz(1), 0.01);
        Assert.assertEquals(384.15, spectrum.getMz(2), 0.01);
        Assert.assertEquals(513.19, spectrum.getMz(3), 0.01);
        Assert.assertEquals(675.25, spectrum.getMz(4), 0.01);
    }

    @Test
    public void test384_minus2() throws Exception {

        //Hex-HexNAc~
        final Glycan glycan = new GlycoCTReader().read("RES\n" +
                "1b:o-HEX-0:0|1:aldi\n" +
                "2b:x-HEX-1:5\n" +
                "3s:n-acetyl\n" +
                "LIN\n" +
                "1:1o(-1+1)2d\n" +
                "2:1d(2+1)3n", "test");

        final GlycanFragmenter fragmenter = new GlycanFragmenter(EnumSet.of(IonType.b, IonType.y), true, false, PeakList.Precision.FLOAT, 1, 0);

        GlycanSpectrum spectrum = fragmenter.fragment(glycan, 2);

        Assert.assertEquals(6, spectrum.size());
        final double delta = 0.0001;
        Assert.assertEquals(80.0191, spectrum.getMz(0), delta);     // Hex-] -2
        Assert.assertEquals(110.5455, spectrum.getMz(1), delta);    //   [-HexNac~ -2
        Assert.assertEquals(161.0455, spectrum.getMz(2), delta);    // Hex-] -1
        Assert.assertEquals(191.5719, spectrum.getMz(3), delta);    // Hex-HexNAc~ -2
        Assert.assertEquals(222.0983, spectrum.getMz(4), delta);    //   [-HexNac~ -1
        Assert.assertEquals(384.1511, spectrum.getMz(5), delta);    // Hex-HexNAc~ -1
    }
}
