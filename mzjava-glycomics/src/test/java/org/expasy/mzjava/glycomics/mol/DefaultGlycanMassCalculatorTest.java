package org.expasy.mzjava.glycomics.mol;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.mol.PeriodicTable;
import org.expasy.mzjava.core.ms.spectrum.FragmentType;
import org.expasy.mzjava.core.ms.spectrum.IonType;
import org.junit.Assert;
import org.junit.Test;
import org.openide.util.Lookup;

import java.util.Arrays;
import java.util.List;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class DefaultGlycanMassCalculatorTest {

    private final MonosaccharideLookup monosaccharideLookup = Lookup.getDefault().lookup(MonosaccharideLookup.class);
    private final GlycanMassCalculator glycanMassCalculator = Lookup.getDefault().lookup(GlycanMassCalculator.class);


    @Test
    public void testGetGlycosidicDeltaMass() throws Exception {

        Assert.assertEquals(glycanMassCalculator.getGlycosidicDeltaMass(IonType.b),-19.0178407937,0.0000000001);
        Assert.assertEquals(glycanMassCalculator.getGlycosidicDeltaMass(IonType.c),-1.0072761077,0.0000000001);
        Assert.assertEquals(glycanMassCalculator.getGlycosidicDeltaMass(IonType.z),-19.0178407937,0.0000000001);
        Assert.assertEquals(glycanMassCalculator.getGlycosidicDeltaMass(IonType.y),-1.0072761077,0.0000000001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void wrongIonType() throws Exception {
        glycanMassCalculator.getGlycosidicDeltaMass(IonType.i);
    }

    @Test(expected = NullPointerException.class)
    public void nullIonType() throws Exception {
        glycanMassCalculator.getGlycosidicDeltaMass(null);
    }

    @Test
    public void testCalculateMzGlcNac() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        builder.setRoot(monosaccharideLookup.getNew("Gal"), Optional.fromNullable(Anomericity.alpha),"");
        Glycan glycan = builder.build();

        Assert.assertEquals(179.0561,glycan.calculateMz(1),0.0001);

    }


    @Test
    public void testCalculateMz() throws Exception {

        Assert.assertEquals(161.0455, glycanMassCalculator.calculateMz(161.0455, 1, FragmentType.FORWARD),0.00000000001);
        Assert.assertEquals(179.0561, glycanMassCalculator.calculateMz(179.0561, 1, FragmentType.FORWARD),0.00000000001);
        Assert.assertEquals(80.0191, glycanMassCalculator.calculateMz(80.0191 * 2 + (PeriodicTable.H_MASS - PeriodicTable.ELECTRON_MASS), 2, FragmentType.FORWARD),0.00000000001);
        Assert.assertEquals(89.0244, glycanMassCalculator.calculateMz(89.0244 * 2 + (PeriodicTable.H_MASS - PeriodicTable.ELECTRON_MASS), 2, FragmentType.FORWARD),0.00000000001);
        Assert.assertEquals(271.5905, glycanMassCalculator.calculateMz(271.5905 * 2 + (PeriodicTable.H_MASS - PeriodicTable.ELECTRON_MASS), 2, FragmentType.REVERSE),0.00000000001);
        Assert.assertEquals(262.5852, glycanMassCalculator.calculateMz(262.5852 * 2 + (PeriodicTable.H_MASS - PeriodicTable.ELECTRON_MASS), 2, FragmentType.REVERSE),0.00000000001);
        Assert.assertEquals(544.1883, glycanMassCalculator.calculateMz(544.1883, 1, FragmentType.REVERSE),0.00000000001);
        Assert.assertEquals(526.1777, glycanMassCalculator.calculateMz(526.1777, 1, FragmentType.REVERSE),0.00000000001);
        Assert.assertEquals(706.2411, glycanMassCalculator.calculateMz(707.2483, 1, FragmentType.INTACT),0.0001);
        Assert.assertEquals(352.6169, glycanMassCalculator.calculateMz(707.2483, 2, FragmentType.INTACT),0.0001);

        //Add test for a  and x Ion.

    }

    @Test
    public void testGetAllInternalTypeCleavage() throws Exception {
        CutIndexes[] firstInternalTypeCleavage = glycanMassCalculator.getAllInternalTypeCleavage(monosaccharideLookup.getNew("Hex"));
        CutIndexes[] secondInternalTypeCleavage = glycanMassCalculator.getAllInternalTypeCleavage(monosaccharideLookup.getNew("Fuc"));

        Assert.assertTrue(firstInternalTypeCleavage.length == 9);
        Assert.assertTrue(secondInternalTypeCleavage.length == 9);
        CutIndexes test1 = new CutIndexes(0,2);
        CutIndexes test2 = new CutIndexes(3,5);

        List aInternalTypeCleavageList = Arrays.asList(firstInternalTypeCleavage);
        List xInternalTypeCleavageList = Arrays.asList(secondInternalTypeCleavage);
        Assert.assertTrue(aInternalTypeCleavageList.contains(test1));
        Assert.assertTrue(xInternalTypeCleavageList.contains(test1));
        Assert.assertTrue(aInternalTypeCleavageList.contains(test2));
        Assert.assertTrue(xInternalTypeCleavageList.contains(test2));
    }
}
