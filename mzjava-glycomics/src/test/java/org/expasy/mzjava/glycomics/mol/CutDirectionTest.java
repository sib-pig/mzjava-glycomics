package org.expasy.mzjava.glycomics.mol;


import org.junit.Assert;
import org.junit.Test;

public class CutDirectionTest {

    @Test
    public void testGetComplement() throws Exception {
        Assert.assertEquals(CutDirection.ANTI_CLOCK_WISE, CutDirection.CLOCK_WISE.getComplement());
        Assert.assertEquals(CutDirection.CLOCK_WISE , CutDirection.ANTI_CLOCK_WISE.getComplement());
    }
}