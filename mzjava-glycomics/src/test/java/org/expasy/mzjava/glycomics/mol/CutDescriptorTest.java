package org.expasy.mzjava.glycomics.mol;

import org.junit.Assert;
import org.junit.Test;
import org.openide.util.Lookup;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.mock;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class CutDescriptorTest {

    private final MonosaccharideLookup monosaccharideLookup = Lookup.getDefault().lookup(MonosaccharideLookup.class);

    @Test
    public void testAddCleavedEdge() throws Exception {
        CutDescriptor cutDescriptor = new CutDescriptor();
        SEdge edge = mock(SEdge.class);
        SEdge edge2 = mock(SEdge.class);
        SEdge edge3 = mock(SEdge.class);
        cutDescriptor.addCleavedEdge(edge);
        cutDescriptor.addCleavedEdge(edge2);
        cutDescriptor.addCleavedEdge(edge3);
        Assert.assertTrue(cutDescriptor.contains(edge));
        Assert.assertTrue(cutDescriptor.contains(edge2));
        Assert.assertTrue(cutDescriptor.contains(edge3));
        Assert.assertFalse(cutDescriptor.contains(mock(SEdge.class)));
    }

    @Test
    public void testAddCleavedEdges() throws Exception {
        CutDescriptor cutDescriptor = new CutDescriptor();
        SEdge edge = mock(SEdge.class);
        SEdge edge2 = mock(SEdge.class);
        SEdge edge3 = mock(SEdge.class);
        Set<SEdge> edges = new HashSet<SEdge>();
        edges.add(edge);
        edges.add(edge2);
        edges.add(edge3);
        cutDescriptor.addCleavedEdges(edges);
        Assert.assertTrue(cutDescriptor.contains(edge));
        Assert.assertTrue(cutDescriptor.contains(edge2));
        Assert.assertTrue(cutDescriptor.contains(edge3));
        Assert.assertFalse(cutDescriptor.contains(mock(SEdge.class)));
    }


    @Test
    public void testAddCleavedMonosaccharide() throws Exception {

        CutDescriptor cutDescriptor = new CutDescriptor();
        Monosaccharide gal = monosaccharideLookup.getNew("Gal");
        Monosaccharide glc = monosaccharideLookup.getNew("Glc");
        Monosaccharide fuc = monosaccharideLookup.getNew("Fuc");
        cutDescriptor.addCleavedMonosaccharide(gal, new CutIndexes(1,4));
        cutDescriptor.addCleavedMonosaccharide(glc, new CutIndexes(1,3));
        cutDescriptor.addCleavedMonosaccharide(fuc, new CutIndexes(2,4));
        Assert.assertTrue(cutDescriptor.contains(gal));
        Assert.assertEquals(new CutIndexes(1,4),cutDescriptor.getCutIndexesForMonosaccharide(gal));
        Assert.assertEquals(new CutIndexes(1,3),cutDescriptor.getCutIndexesForMonosaccharide(glc));
        Assert.assertEquals(new CutIndexes(2,4),cutDescriptor.getCutIndexesForMonosaccharide(fuc));
        Assert.assertTrue(cutDescriptor.contains(glc));
        Assert.assertTrue(cutDescriptor.contains(fuc));
        Assert.assertFalse(cutDescriptor.contains(mock(SEdge.class)));
    }

    @Test
    public void testAddCleavedMonosaccharides() throws Exception {
        CutDescriptor cutDescriptor = new CutDescriptor();
        Monosaccharide gal = monosaccharideLookup.getNew("Gal");
        Monosaccharide glc = monosaccharideLookup.getNew("Glc");
        Monosaccharide fuc = monosaccharideLookup.getNew("Fuc");
        Set<CleavedMonosaccharide> cleavedMonosaccharides = new HashSet<CleavedMonosaccharide>();
        cleavedMonosaccharides.add(new CleavedMonosaccharide(gal, new CutIndexes(1, 4)));
        cleavedMonosaccharides.add(new CleavedMonosaccharide(glc, new CutIndexes(1, 3)));
        cleavedMonosaccharides.add(new CleavedMonosaccharide(fuc, new CutIndexes(2, 4)));
        cutDescriptor.addCleavedMonosaccharides(cleavedMonosaccharides);
        Assert.assertTrue(cutDescriptor.contains(gal));
        Assert.assertEquals(new CutIndexes(1,4),cutDescriptor.getCutIndexesForMonosaccharide(gal));
        Assert.assertEquals(new CutIndexes(1,3),cutDescriptor.getCutIndexesForMonosaccharide(glc));
        Assert.assertEquals(new CutIndexes(2,4),cutDescriptor.getCutIndexesForMonosaccharide(fuc));
        Assert.assertTrue(cutDescriptor.contains(glc));
        Assert.assertTrue(cutDescriptor.contains(fuc));
        Assert.assertFalse(cutDescriptor.contains(mock(SEdge.class)));

    }

    @Test
    public void testContains() throws Exception {

        CutDescriptor cutDescriptor = new CutDescriptor();
        SEdge edge = mock(SEdge.class);
        SEdge edge2 = mock(SEdge.class);
        cutDescriptor.addCleavedEdge(edge);
        Assert.assertTrue(cutDescriptor.contains(edge));
        Assert.assertFalse(cutDescriptor.contains(edge2));

    }

    @Test
    public void testContains1() throws Exception {
        CutDescriptor cutDescriptor = new CutDescriptor();
        Monosaccharide monosaccharide = monosaccharideLookup.getNew("Gal");
        cutDescriptor.addCleavedMonosaccharide(monosaccharide, new CutIndexes(1,3));
        Assert.assertTrue(cutDescriptor.contains(monosaccharide));
        Assert.assertFalse(cutDescriptor.contains(monosaccharideLookup.getNew("Gal")));

    }

    @Test
    public void testGetCutIndexesForMonosaccharide() throws Exception {
        CutDescriptor cutDescriptor = new CutDescriptor();
        Monosaccharide monosaccharide = monosaccharideLookup.getNew("Gal");
        cutDescriptor.addCleavedMonosaccharide(monosaccharide, new CutIndexes(1,3));
        Assert.assertEquals(1, cutDescriptor.getNumberOfCut());
        Assert.assertNotEquals(new CutIndexes(1, 2), cutDescriptor.getCutIndexesForMonosaccharide(monosaccharide));
        Assert.assertEquals(new CutIndexes(1,3),cutDescriptor.getCutIndexesForMonosaccharide(monosaccharide));
    }

    @Test
    public void testIsEmpty() throws Exception {

        CutDescriptor cutDescriptor = new CutDescriptor();
        Monosaccharide monosaccharide = monosaccharideLookup.getNew("Gal");
        cutDescriptor.addCleavedMonosaccharide(monosaccharide,new CutIndexes(1,4));
        Assert.assertFalse(cutDescriptor.isEmpty());
        Assert.assertEquals(1,cutDescriptor.getNumberOfCut());
        CutDescriptor cutDescriptorEmpty = new CutDescriptor();
        Assert.assertTrue(cutDescriptorEmpty.isEmpty());
        Assert.assertEquals(0, cutDescriptorEmpty.getNumberOfCut());
        cutDescriptorEmpty.addCleavedEdge(mock(SEdge.class));
        Assert.assertFalse(cutDescriptorEmpty.isEmpty());
        Assert.assertEquals(1,cutDescriptorEmpty.getNumberOfCut());
    }

    @Test
    public void testGetNumberOfCut() throws Exception {

        CutDescriptor cutDescriptor = new CutDescriptor();
        Monosaccharide monosaccharide = monosaccharideLookup.getNew("Gal");
        cutDescriptor.addCleavedMonosaccharide(monosaccharide,new CutIndexes(1,4));

        Assert.assertEquals(1,cutDescriptor.getNumberOfCut());
        Assert.assertNotEquals(3, cutDescriptor.getNumberOfCut());
        CutDescriptor cutDescriptorEmpty = new CutDescriptor();
        Assert.assertEquals(0, cutDescriptorEmpty.getNumberOfCut());
        Assert.assertNotEquals(3,cutDescriptorEmpty.getNumberOfCut());
    }

    @Test
    public void testGetCleavedNodes() throws Exception {

        CutDescriptor cutDescriptor = new CutDescriptor();
        Monosaccharide monosaccharide = monosaccharideLookup.getNew("Gal");
        cutDescriptor.addCleavedMonosaccharide(monosaccharide,new CutIndexes(1,4));

        Set<Monosaccharide> cleavedNodes = cutDescriptor.getCleavedNodes();
        Assert.assertFalse(cleavedNodes.isEmpty());
        Assert.assertEquals(1, cleavedNodes.size());
        Assert.assertNotEquals(2, cleavedNodes.size());
        Assert.assertTrue(cleavedNodes.contains(monosaccharide));
    }

    @Test
    public void testGetCleavedEdges() throws Exception {

        CutDescriptor cutDescriptor = new CutDescriptor();
        SEdge edge = mock(SEdge.class);
        cutDescriptor.addCleavedEdge(edge);

        Set<SEdge> cleavedEdges = cutDescriptor.getCleavedEdges();
        Assert.assertFalse(cutDescriptor.isEmpty());
        Assert.assertFalse(cleavedEdges.isEmpty());
        Assert.assertEquals(1,cleavedEdges.size());
        Assert.assertNotEquals(2,cleavedEdges.size());
        Assert.assertTrue(cleavedEdges.contains(edge));
    }

    @Test
    public void testEquals() throws Exception {
        CutDescriptor cutDescriptor  = new CutDescriptor();
        Monosaccharide gal = monosaccharideLookup.getNew("Gal");
        Monosaccharide glc = monosaccharideLookup.getNew("Glc");
        CutIndexes cut1 = new CutIndexes(1,4);
        cutDescriptor.addCleavedMonosaccharide(gal, cut1);
        CutIndexes cut2 = new CutIndexes(1,3);
        cutDescriptor.addCleavedMonosaccharide(glc, cut2);

        CutDescriptor cutDescriptor2  = new CutDescriptor();
        cutDescriptor2.addCleavedMonosaccharide(gal, new CutIndexes(1,4));
        cutDescriptor2.addCleavedMonosaccharide(glc, cut2);

        Assert.assertTrue(cutDescriptor.equals(cutDescriptor2));

        CutDescriptor cutDescriptor3  = new CutDescriptor();
        Monosaccharide fuc = monosaccharideLookup.getNew("Fuc");
        cutDescriptor2.addCleavedMonosaccharide(gal, new CutIndexes(1,4));
        cutDescriptor2.addCleavedMonosaccharide(fuc, new CutIndexes(1,3));

        Assert.assertFalse(cutDescriptor.equals(cutDescriptor3));
        Assert.assertFalse(cutDescriptor3.equals(cutDescriptor2));

        CutDescriptor cutDescriptor4  = new CutDescriptor();
        Linkage linkage = mock(Linkage.class);
        SEdge edge = new SEdge(gal,glc,linkage);
        cutDescriptor4.addCleavedEdge(edge);

        CutDescriptor cutDescriptor5  = new CutDescriptor();
        cutDescriptor5.addCleavedEdge(edge);

        Assert.assertTrue(cutDescriptor5.equals(cutDescriptor4));
        Assert.assertTrue(cutDescriptor4.equals(cutDescriptor5));

        CutDescriptor cutDescriptor6 = new CutDescriptor();
        cutDescriptor6.addCleavedEdge(new SEdge(glc,gal,linkage));

        Assert.assertFalse(cutDescriptor5.equals(cutDescriptor6));
        Assert.assertFalse(cutDescriptor6.equals(cutDescriptor5));
        Assert.assertFalse(cutDescriptor6.equals(cutDescriptor4));
        Assert.assertFalse(cutDescriptor4.equals(cutDescriptor6));


    }


}
