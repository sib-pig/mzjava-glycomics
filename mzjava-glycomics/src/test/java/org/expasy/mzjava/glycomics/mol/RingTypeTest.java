package org.expasy.mzjava.glycomics.mol;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */

public class RingTypeTest {


    @Test
    public void testGetRingType() throws Exception {
        Assert.assertEquals(RingType.pyranose, RingType.getRingType('p'));
        Assert.assertEquals(RingType.furanose, RingType.getRingType('f'));
        Assert.assertEquals(RingType.open, RingType.getRingType('o'));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testGetRingTypeWrong() throws Exception {
        Assert.assertEquals(RingType.pyranose, RingType.getRingType('s'));
        Assert.assertEquals(RingType.open, RingType.getRingType('k'));
    }


}