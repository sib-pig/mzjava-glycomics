package org.expasy.mzjava.glycomics.mol;

import org.expasy.mzjava.core.mol.Composition;
import org.junit.After;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.openide.util.Lookup;

import java.io.File;
import java.io.FileWriter;
import java.util.Collection;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class DefaultMonosaccharideLookupTest {

    private final Composition compositionGal = Composition.parseComposition("C6H12O6");
    private final Monosaccharide Gal = new Monosaccharide("Gal",compositionGal, MonosaccharideSuperclass.Hexose, MonosaccharideClass.Hex, RingType.pyranose,1,5);
    private final Composition compositionHex = Composition.parseComposition("C6H12O5");
    private final Monosaccharide DeoxyHex = new Monosaccharide("DeoxyHex",compositionHex, MonosaccharideSuperclass.Hexose,MonosaccharideClass.dHex, RingType.pyranose,1,5);
    private final MonosaccharideLookup monosaccharideLookup = Lookup.getDefault().lookup(MonosaccharideLookup.class);
    private final Composition compositionAra = Composition.parseComposition("C5H10O5");
    private final Monosaccharide Ara = new Monosaccharide("Ara",compositionAra,MonosaccharideSuperclass.Pentose,MonosaccharideClass.Pen,RingType.furanose,1,5);
    

    @Test
    public void testGetNew() throws Exception {
        Assert.assertEquals(DeoxyHex, monosaccharideLookup.getNew("DeoxyHex"));
        Assert.assertEquals(Gal, monosaccharideLookup.getNew("Gal"));
        Assert.assertNotEquals(Gal, monosaccharideLookup.getNew("Hex"));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testGetNull() throws Exception {
        Assert.assertNotEquals(Gal, monosaccharideLookup.getNew("Ciao"));
    }


    @Test
    public void testValues() throws Exception {
        Collection<Monosaccharide> monosaccharideCollection = monosaccharideLookup.values();
        Assert.assertTrue(monosaccharideCollection.contains(monosaccharideLookup.getNew("Gal")));
        Assert.assertTrue(monosaccharideCollection.contains(monosaccharideLookup.getNew("Hex")));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValuesNull() throws Exception{
        Collection<Monosaccharide> monosaccharideCollection = monosaccharideLookup.values();
        Assert.assertFalse(monosaccharideCollection.contains(monosaccharideLookup.getNew("CIAO")));
    }


    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void testSetPropertyFile() throws Exception {

        File temporaryfile = folder.newFile("testSubstituentData.json");

        FileWriter fileWriter = new FileWriter(temporaryfile);

        fileWriter.write("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Ara\",\n" +
                "\t\t\"composition\": \"C5H10O5\",\n" +
                "\t\t\"ring\": \"f\",\n" +
                "\t\t\"ringstart\": 1,\n" +
                "\t\t\"ringend\": 5,\n" +
                "\t\t\"superclass\": \"Pentose\",\n" +
                "\t\t\"class\": \"Pen\",\n" +
                "\t\t\"deltaMass\": [\n" +
                "                    {\n" +
                "                        \"ionType\" : \"x\",\n" +
                "                        \"cleavage\" : [0, 2],\n" +
                "                        \"composition\" : \"C-3H-7O-3(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"a\",\n" +
                "                        \"cleavage\" : [0, 2],\n" +
                "                        \"composition\" : \"C-2H-5O-2(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"x\",\n" +
                "                        \"cleavage\" : [0, 3],\n" +
                "                        \"composition\" : \"C-2H-5O-2(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"a\",\n" +
                "                        \"cleavage\" : [0, 3],\n" +
                "                        \"composition\" : \"C-3H-7O-3(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"x\",\n" +
                "                        \"cleavage\" : [0, 4],\n" +
                "                        \"composition\" : \"C-1H-3O-1(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"a\",\n" +
                "                        \"cleavage\" : [0, 4],\n" +
                "                        \"composition\" : \"C-4H-9O-4(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"x\",\n" +
                "                        \"cleavage\" : [1, 3],\n" +
                "                        \"composition\" : \"C-2H-5O-2(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"a\",\n" +
                "                        \"cleavage\" : [1, 3],\n" +
                "                        \"composition\" : \"C-3H-7O-3(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"x\",\n" +
                "                        \"cleavage\" : [1, 4],\n" +
                "                        \"composition\" : \"C-3H-7O-3(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"a\",\n" +
                "                        \"cleavage\" : [1, 4],\n" +
                "                        \"composition\" : \"C-2H-5O-2(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"x\",\n" +
                "                        \"cleavage\" : [1, 5],\n" +
                "                        \"composition\" : \"C-4H-9O-3(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"a\",\n" +
                "                        \"cleavage\" : [1, 5],\n" +
                "                        \"composition\" : \"C-1H-3O-2(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"x\",\n" +
                "                        \"cleavage\" : [2, 4],\n" +
                "                        \"composition\" : \"C-2H-5O-2(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"a\",\n" +
                "                        \"cleavage\" : [2, 4],\n" +
                "                        \"composition\" : \"C-3H-7O-3(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"x\",\n" +
                "                        \"cleavage\" : [2, 5],\n" +
                "                        \"composition\" : \"C-3H-7O-2(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"a\",\n" +
                "                        \"cleavage\" : [2, 5],\n" +
                "                        \"composition\" : \"C-2H-5O-3(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"x\",\n" +
                "                        \"cleavage\" : [3, 5],\n" +
                "                        \"composition\" : \"C-2H-5O-1(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"a\",\n" +
                "                        \"cleavage\" : [3, 5],\n" +
                "                        \"composition\" : \"C-3H-7O-4(-)\"\n" +
                "                    }\n" +
                "                    ]\n" +
                "\t}]");

        fileWriter.close();
        System.setProperty(DefaultMonosaccharideLookup.PROPERTY_MONOSACCHARIDE_LOOKUP,temporaryfile.getPath());
        MonosaccharideLookup lookup = new DefaultMonosaccharideLookup();
        Assert.assertEquals(Ara, lookup.getNew("Ara"));
        System.clearProperty(DefaultMonosaccharideLookup.PROPERTY_MONOSACCHARIDE_LOOKUP);
        folder.delete();
    }


    @Test (expected = IllegalArgumentException.class)
    public void testSetPropertyFileException() throws Exception {

        File temporaryfile = folder.newFile("testSubstituentData.json");

        FileWriter fileWriter = new FileWriter(temporaryfile);

        fileWriter.write("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Ara\",\n" +
                "\t\t\"composition\": \"C5H10O5\",\n" +
                "\t\t\"ring\": \"p\",\n" +
                "\t\t\"ringstart\": 1,\n" +
                "\t\t\"ringend\": 5,\n" +
                "\t\t\"superclass\": \"Pentose\",\n" +
                "\t\t\"class\": \"Pen\",\n" +
                "\t\t\"deltaMass\": [\n" +
                "                    {\n" +
                "                        \"ionType\" : \"x\",\n" +
                "                        \"cleavage\" : [0, 2],\n" +
                "                        \"composition\" : \"C-3H-7O-3(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"a\",\n" +
                "                        \"cleavage\" : [0, 2],\n" +
                "                        \"composition\" : \"C-2H-5O-2(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"x\",\n" +
                "                        \"cleavage\" : [0, 3],\n" +
                "                        \"composition\" : \"C-2H-5O-2(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"a\",\n" +
                "                        \"cleavage\" : [0, 3],\n" +
                "                        \"composition\" : \"C-3H-7O-3(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"x\",\n" +
                "                        \"cleavage\" : [0, 4],\n" +
                "                        \"composition\" : \"C-1H-3O-1(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"a\",\n" +
                "                        \"cleavage\" : [0, 4],\n" +
                "                        \"composition\" : \"C-4H-9O-4(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"x\",\n" +
                "                        \"cleavage\" : [1, 3],\n" +
                "                        \"composition\" : \"C-2H-5O-2(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"a\",\n" +
                "                        \"cleavage\" : [1, 3],\n" +
                "                        \"composition\" : \"C-3H-7O-3(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"x\",\n" +
                "                        \"cleavage\" : [1, 4],\n" +
                "                        \"composition\" : \"C-3H-7O-3(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"a\",\n" +
                "                        \"cleavage\" : [1, 4],\n" +
                "                        \"composition\" : \"C-2H-5O-2(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"x\",\n" +
                "                        \"cleavage\" : [1, 5],\n" +
                "                        \"composition\" : \"C-4H-9O-3(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"a\",\n" +
                "                        \"cleavage\" : [1, 5],\n" +
                "                        \"composition\" : \"C-1H-3O-2(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"x\",\n" +
                "                        \"cleavage\" : [2, 4],\n" +
                "                        \"composition\" : \"C-2H-5O-2(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"a\",\n" +
                "                        \"cleavage\" : [2, 4],\n" +
                "                        \"composition\" : \"C-3H-7O-3(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"x\",\n" +
                "                        \"cleavage\" : [2, 5],\n" +
                "                        \"composition\" : \"C-3H-7O-2(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"a\",\n" +
                "                        \"cleavage\" : [2, 5],\n" +
                "                        \"composition\" : \"C-2H-5O-3(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"x\",\n" +
                "                        \"cleavage\" : [3, 5],\n" +
                "                        \"composition\" : \"C-2H-5O-1(-)\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                        \"ionType\" : \"a\",\n" +
                "                        \"cleavage\" : [3, 5],\n" +
                "                        \"composition\" : \"C-3H-7O-4(-)\"\n" +
                "                    }\n" +
                "                    ]\n" +
                "\t}]");

        fileWriter.close();
        System.setProperty(DefaultMonosaccharideLookup.PROPERTY_MONOSACCHARIDE_LOOKUP,temporaryfile.getPath());
        MonosaccharideLookup lookup = new DefaultMonosaccharideLookup();
        Assert.assertEquals(Ara, lookup.getNew("Ciao"));
        System.clearProperty(DefaultMonosaccharideLookup.PROPERTY_MONOSACCHARIDE_LOOKUP);
        folder.delete();
    }

    @After
    public void closeResource() {
        System.clearProperty(DefaultMonosaccharideLookup.PROPERTY_MONOSACCHARIDE_LOOKUP);
        folder.delete();
    }

}
