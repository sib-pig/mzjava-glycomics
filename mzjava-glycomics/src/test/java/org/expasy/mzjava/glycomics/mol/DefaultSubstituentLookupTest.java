package org.expasy.mzjava.glycomics.mol;

import org.expasy.mzjava.core.mol.Composition;
import org.junit.After;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileWriter;
import java.util.Collection;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class DefaultSubstituentLookupTest {
    private final Substituent nitrat = new Substituent("Nitrat", Composition.parseComposition("NO2"));
    private final Substituent thio = new Substituent("Thio", Composition.parseComposition("SH"));
    private final Substituent rLactate2 = new Substituent("RLactate2", Composition.parseComposition("CH3CHCO"));


    @Test (expected = IllegalArgumentException.class)
    public void testGetNull() throws Exception {
        final SubstituentLookup lookup = new DefaultSubstituentLookup();
        lookup.getNew("Ciao");
    }

    @Test
    public void testGetNew() throws Exception {
        final SubstituentLookup lookup = new DefaultSubstituentLookup();
        Assert.assertEquals(nitrat, lookup.getNew("Nitrat"));
        Assert.assertEquals(thio, lookup.getNew("Thio"));
        Assert.assertNotEquals(rLactate2, lookup.getNew("Bromo"));
    }

    @Test
    public void testValues() throws Exception {
        final SubstituentLookup lookup = new DefaultSubstituentLookup();
        Collection<Substituent> substituentsCollection = lookup.values();
        Assert.assertTrue(substituentsCollection.contains(lookup.getNew("Nitrat")));
        Assert.assertTrue(substituentsCollection.contains(lookup.getNew("RLactate2")));
    }

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void testSetPropertyFile() throws Exception {

        File temporaryfile = folder.newFile("testSubstituentData.json");

        FileWriter fileWriter = new FileWriter(temporaryfile);

        fileWriter.write("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Nitrat\",\n" +
                "\t\t\"composition\": \"NO2\"\n" +
                "\t}]");

        fileWriter.close();
        System.setProperty(DefaultSubstituentLookup.PROPERTY_SUBSTITUENT_LOOKUP,temporaryfile.getPath());
        SubstituentLookup lookup = new DefaultSubstituentLookup();
        Assert.assertEquals(nitrat, lookup.getNew("Nitrat"));
        System.clearProperty(DefaultSubstituentLookup.PROPERTY_SUBSTITUENT_LOOKUP);
        folder.delete();
    }


    @Test (expected = IllegalArgumentException.class)
    public void testSetPropertyFileException() throws Exception {

        File temporaryfile = folder.newFile("testSubstituentData.json");

        FileWriter fileWriter = new FileWriter(temporaryfile);

        fileWriter.write("[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Nitrat\",\n" +
                "\t\t\"composition\": \"NO2\"\n" +
                "\t}]");

        fileWriter.close();
        System.setProperty(DefaultSubstituentLookup.PROPERTY_SUBSTITUENT_LOOKUP,temporaryfile.getPath());
        SubstituentLookup lookup = new DefaultSubstituentLookup();
        Assert.assertEquals(nitrat, lookup.getNew("Ciao"));
        System.clearProperty(DefaultSubstituentLookup.PROPERTY_SUBSTITUENT_LOOKUP);
        folder.delete();
    }

    @After
    public void closeResource() {
        System.clearProperty(DefaultSubstituentLookup.PROPERTY_SUBSTITUENT_LOOKUP);
        folder.delete();
    }

}
