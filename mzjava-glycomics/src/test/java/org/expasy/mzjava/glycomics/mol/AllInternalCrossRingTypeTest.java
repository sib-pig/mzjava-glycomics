package org.expasy.mzjava.glycomics.mol;

import org.junit.Assert;
import org.junit.Test;
import org.openide.util.Lookup;

import java.util.Set;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class AllInternalCrossRingTypeTest {

    private final MonosaccharideLookup monosaccharideLookup = Lookup.getDefault().lookup(MonosaccharideLookup.class);

    @Test
    public void testGet() throws Exception {
     AllCrossRingCutType allInternalCleavageType = new AllCrossRingCutType();
        Set<CutIndexes> aVectorIndexes = allInternalCleavageType.get(monosaccharideLookup.getNew("Ara"));
        Set<CutIndexes> aVectorIndexes2 = allInternalCleavageType.get(monosaccharideLookup.getNew("Ara"));
        Assert.assertEquals(aVectorIndexes,aVectorIndexes2);
    }



    @Test (expected = NullPointerException.class)
    public void testNullMonosaccharide() throws Exception {
        AllCrossRingCutType allInternalCleavageType = new AllCrossRingCutType();
        allInternalCleavageType.get(null);
    }
}
