package org.expasy.mzjava.glycomics.mol;

import com.google.common.base.Optional;
import org.expasy.mzjava.core.mol.Composition;
import org.junit.Assert;
import org.junit.Test;
import org.openide.util.Lookup;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */

public class CutDescriptorGeneratorTest {

    private final SubstituentLookup substituentLookup = Lookup.getDefault().lookup(SubstituentLookup.class);
    private final MonosaccharideLookup monosaccharideLookup = Lookup.getDefault().lookup(MonosaccharideLookup.class);

    @Test
    public void testCutDescriptorGenerator() throws Exception {

        Glycan.Builder builder = new Glycan.Builder();
        Monosaccharide node0 = builder.setRoot(monosaccharideLookup.getNew("Gal"), Optional.<Anomericity>absent(), "Id_1");
        Substituent sub0 = builder.add(substituentLookup.getNew("NAcetyl"), node0, new SubstituentLinkage(2, Composition.parseComposition("O-1H-1")));
        Monosaccharide node1 = builder.add(monosaccharideLookup.getNew("Gal"), node0, new GlycosidicLinkage(Anomericity.beta, 1, 3, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Monosaccharide node2 = builder.add(monosaccharideLookup.getNew("Fuc"), node1, new GlycosidicLinkage(Anomericity.alpha, 1, 2, Composition.parseComposition("O-1H-1"), Composition.parseComposition("H-1")));
        Glycan sugar = builder.build();

        CutDescriptorGenerator cutDescriptorGenerator = new CutDescriptorGenerator();
        List<CutDescriptor> cutDescriptors = cutDescriptorGenerator.generateCutDescriptor(sugar,1,1);
        Assert.assertEquals(84, cutDescriptors.size());
        Assert.assertTrue(cutDescriptors.contains(new CutDescriptor(new HashSet<CleavedMonosaccharide>(),new HashSet<SEdge>())));
        Set<CleavedMonosaccharide> cleavedMonosaccharideSet = new HashSet<CleavedMonosaccharide>();
        cleavedMonosaccharideSet.add(new CleavedMonosaccharide(node0,new CutIndexes(0,2)));
        Assert.assertTrue(cutDescriptors.contains(new CutDescriptor(cleavedMonosaccharideSet,new HashSet<SEdge>())));

        List<CutDescriptor> cutDescriptorsDoubleCrossring = cutDescriptorGenerator.generateCutDescriptor(sugar,0,2);
        int countNoCut = 0;
        int countSingleCut = 0;
        int countDoubleCut = 0;
        for (CutDescriptor cuts : cutDescriptorsDoubleCrossring){
            if ( cuts.getNumberOfCut() == 0){
                countNoCut++;
            }
            if ( cuts.getNumberOfCut() == 1){
                countSingleCut++;
            }
            if ( cuts.getNumberOfCut() == 2){
                countDoubleCut++;
            }
        }
        Assert.assertEquals(1,countNoCut);
        Assert.assertEquals(27,countSingleCut);
        Assert.assertEquals(243,countDoubleCut);
        cleavedMonosaccharideSet.add(new CleavedMonosaccharide(node1,new CutIndexes(0,2)));
        Assert.assertTrue(cutDescriptorsDoubleCrossring.contains(new CutDescriptor(cleavedMonosaccharideSet,new HashSet<SEdge>())));

    }

}