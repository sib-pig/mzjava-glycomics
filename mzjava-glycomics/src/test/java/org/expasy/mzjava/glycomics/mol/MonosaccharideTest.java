package org.expasy.mzjava.glycomics.mol;


import org.expasy.mzjava.core.mol.Composition;
import org.junit.Assert;
import org.junit.Test;
import org.openide.util.Lookup;

/**
 * @author Davide Alocci
 * @version sqrt -1.
 */
public class MonosaccharideTest {

    private final Composition compositionAlt = Composition.parseComposition("C6H12O6");
    private final Monosaccharide Alt = new Monosaccharide("Alt",compositionAlt, MonosaccharideSuperclass.Hexose, MonosaccharideClass.Hex, RingType.pyranose,1,5);
    private final Composition compositionDeoxyHex = Composition.parseComposition("C6H12O5");
    private final Monosaccharide DeoxyHex = new Monosaccharide("DeoxyHex", compositionDeoxyHex, MonosaccharideSuperclass.Hexose, MonosaccharideClass.dHex,RingType.pyranose,1,5);
    private final MonosaccharideLookup monosaccharideLookup = Lookup.getDefault().lookup(MonosaccharideLookup.class);


    @Test(expected = NullPointerException.class)
    public void testNullName() throws Exception{
        new Monosaccharide(null,Composition.parseComposition("H"),MonosaccharideSuperclass.Hexose, MonosaccharideClass.dHex, RingType.pyranose,1,5);
    }

    @Test(expected = NullPointerException.class)
    public void testNullComp() throws Exception{
        new Monosaccharide("Ciao",null,MonosaccharideSuperclass.Hexose, MonosaccharideClass.dHex, RingType.pyranose,1,5);
    }

    @Test(expected = NullPointerException.class)
    public void testNullClass() throws Exception{
        new Monosaccharide("Ciao",Composition.parseComposition("H"),MonosaccharideSuperclass.Hexose, null,RingType.pyranose,1,5);
    }

    @Test(expected = NullPointerException.class)
    public void testNullSupClass() throws Exception{
        new Monosaccharide("Ciao",Composition.parseComposition("H"),null, MonosaccharideClass.dHex,RingType.pyranose,1,5 );
    }


    @Test(expected = NullPointerException.class)
    public void testNullCopy() throws Exception{
        new Monosaccharide(null);
    }



    @Test
    public void testGetName() throws Exception {

        Assert.assertEquals("DeoxyHex",DeoxyHex.getName());
        Assert.assertEquals("Alt",Alt.getName());
        Assert.assertNotEquals("Hex",DeoxyHex.getName());
    }

    @Test
    public void testGetMonosaccharideClass() throws Exception {

        Assert.assertEquals("dHex",DeoxyHex.getMonosaccharideClass().toString());
        Assert.assertEquals("Hex",Alt.getMonosaccharideClass().toString());
        Assert.assertNotEquals("dsadada",DeoxyHex.getMonosaccharideClass().toString());
    }

    @Test
    public void testGetComposition() throws Exception {
        Assert.assertEquals(Composition.parseComposition("C6H12O6"),Alt.getComposition());
        Assert.assertEquals(Composition.parseComposition("C6H12O5"),DeoxyHex.getComposition());
        Assert.assertNotEquals(Composition.parseComposition("C6O4H8"),DeoxyHex.getComposition());
    }

    @Test
    public void testGetMolecularMass() throws Exception {
        Monosaccharide Hex = monosaccharideLookup.getNew("Hex");
        Assert.assertEquals(164.068473494,DeoxyHex.getMolecularMass(),0.000000001);
        Assert.assertEquals(180.063388116,Hex.getMolecularMass(),0.000000001);

    }

    @Test
    public void testGetMonosaccharideSuperclass() throws Exception {
        Monosaccharide Hex = monosaccharideLookup.getNew("Hex");
        Assert.assertEquals(Hex.getMonosaccharideSuperclass(), MonosaccharideSuperclass.Hexose);
        Assert.assertEquals(DeoxyHex.getMonosaccharideSuperclass(), MonosaccharideSuperclass.Hexose);
        Assert.assertEquals(Alt.getMonosaccharideSuperclass(), MonosaccharideSuperclass.Hexose);
        Assert.assertEquals(Alt.getMonosaccharideSuperclass().toString(),"Hexose");
    }

    @Test
    public void testEquals_Symmetric() {
        //Inside Lookup there is a copy constructor so the object are different
        Monosaccharide Hex1 = monosaccharideLookup.getNew("Hex");
        Monosaccharide Hex2 = monosaccharideLookup.getNew("Hex");
        Assert.assertTrue(Hex1.equals(Hex2) && Hex2.equals(Hex1));
        Assert.assertTrue(Hex1.hashCode() == Hex2.hashCode());
    }

    @Test
    public void testEquals() throws Exception{
        Assert.assertEquals(DeoxyHex,DeoxyHex);
        Assert.assertNotEquals(DeoxyHex,Alt);
        final Composition wrongComp = Composition.parseComposition("C6H12O4");
        final Monosaccharide DeoxyHexWrong = new Monosaccharide("DeoxyHex",wrongComp, MonosaccharideSuperclass.Hexose,MonosaccharideClass.HexNAc,RingType.pyranose,1,5 );
        final Monosaccharide DeoxyHexGood = new Monosaccharide("DeoxyHex", compositionDeoxyHex, MonosaccharideSuperclass.Hexose,MonosaccharideClass.dHex, RingType.pyranose,1,5 );
        Assert.assertEquals(DeoxyHex,DeoxyHexGood);
        Assert.assertNotEquals(DeoxyHexWrong,DeoxyHexGood);
        Assert.assertEquals(monosaccharideLookup.getNew("Hex"), monosaccharideLookup.getNew("Hex"));
        Assert.assertNotEquals(monosaccharideLookup.getNew("Hex"), monosaccharideLookup.getNew("DeoxyHex"));
    }

    @Test
    public void testToString() throws Exception{
        Monosaccharide Hex = monosaccharideLookup.getNew("Hex");
        Assert.assertEquals("Monosaccharide{name='Hex', composition=C6H12O6, monosaccharideSuperclass=Hexose, monosaccharideClass=Hex, ring=pyranose, firstRingCarbon=1, lastRingCarbon=5}",Hex.toString());
        Assert.assertEquals("Monosaccharide{name='DeoxyHex', composition=C6H12O5, monosaccharideSuperclass=Hexose, monosaccharideClass=dHex, ring=pyranose, firstRingCarbon=1, lastRingCarbon=5}",DeoxyHex.toString());
        Assert.assertEquals("Monosaccharide{name='Alt', composition=C6H12O6, monosaccharideSuperclass=Hexose, monosaccharideClass=Hex, ring=pyranose, firstRingCarbon=1, lastRingCarbon=5}", Alt.toString());
    }

    @Test
    public  void testGetFirstRingCarbon() throws Exception{

        Assert.assertEquals(Alt.getFirstCarbonRing(),1);
        Assert.assertEquals(DeoxyHex.getFirstCarbonRing(),1);

        Assert.assertNotEquals(Alt.getFirstCarbonRing(),5);
        Assert.assertNotEquals(DeoxyHex.getFirstCarbonRing(),5);

    }

    @Test
    public void testGetLastRingCarbon() throws Exception{

        Assert.assertEquals(Alt.getLastCarbonRing(),5);
        Assert.assertEquals(DeoxyHex.getLastCarbonRing(),5);
        Assert.assertNotEquals(Alt.getLastCarbonRing(),1);
        Assert.assertNotEquals(DeoxyHex.getLastCarbonRing(),1);

    }

}
