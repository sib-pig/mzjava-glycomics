/**
 * Copyright (c) 2010, SIB. All rights reserved.
 *
 * SIB (Swiss Institute of Bioinformatics) - http://www.isb-sib.ch Host -
 * http://mzjava.expasy.org
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer. Redistributions in binary
 * form must reproduce the above copyright notice, this list of conditions and
 * the following disclaimer in the documentation and/or other materials provided
 * with the distribution. Neither the name of the SIB/GENEBIO nor the names of
 * its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL SIB/GENEBIO BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.expasy.mzjava.glycomics.mol;

import org.expasy.mzjava.core.mol.Composition;
import org.junit.Test;
import org.openide.util.Lookup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Oliver Horlacher
 * @version sqrt -1
 */
public class SEdgeTest {

    private final SubstituentLookup substituentLookup = Lookup.getDefault().lookup(SubstituentLookup.class);
    private final MonosaccharideLookup monosaccharideLookup = Lookup.getDefault().lookup(MonosaccharideLookup.class);



    @Test(expected = IllegalArgumentException.class)
    public void testIllegalArguments1() throws Exception {

        new SEdge<Monosaccharide, GlycosidicLinkage>(Collections.<Monosaccharide>emptyList(),
                monosaccharideLookup.getNew("Hex"),
                new GlycosidicLinkage(Anomericity.alpha, null, null, null, null));
    }

    @Test(expected = NullPointerException.class)
    public void testIllegalArguments2() throws Exception {

        new SEdge<Monosaccharide, GlycosidicLinkage>(monosaccharideLookup.getNew("Ara"),
                null,
                new GlycosidicLinkage(Anomericity.alpha, null, null, null, null));
    }

    @Test(expected = NullPointerException.class)
    public void testIllegalArguments3() throws Exception {

        new SEdge<Monosaccharide, GlycosidicLinkage>(monosaccharideLookup.getNew("Ara"),
                monosaccharideLookup.getNew("Man"),
                null);
    }

    @Test
    public void testGetParent() throws Exception {

        Monosaccharide node1 = monosaccharideLookup.getNew("DeoxyHex");
        Monosaccharide node2 = monosaccharideLookup.getNew("Man");
        Monosaccharide node3 = monosaccharideLookup.getNew("Glc");
        Monosaccharide node4 = monosaccharideLookup.getNew("Bac");

        List<Monosaccharide> parents = Arrays.asList(node2, node1, node3, node4);

        Monosaccharide child = monosaccharideLookup.getNew("Pen");
        GlycosidicLinkage linkage = new GlycosidicLinkage(Anomericity.alpha, 3, 4, Composition.parseComposition("(OH)-1"), Composition.parseComposition("H-1"));

        SEdge edge = new SEdge<Monosaccharide, GlycosidicLinkage>(parents, child, linkage);

        assertEquals(child, edge.getChild());
        assertEquals(linkage, edge.getLink());
        assertEquals(node2, edge.getParent());

        assertEquals(true, edge.isHyperEdge());
        assertEquals(parents, edge.getParents());
        assertNotSame(parents, edge.getParents());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testImmutableParentCollection() throws Exception {

        Monosaccharide node1 = monosaccharideLookup.getNew("DeoxyHex");
        Monosaccharide node2 = monosaccharideLookup.getNew("Man");
        Monosaccharide node3 = monosaccharideLookup.getNew("Glc");
        Monosaccharide node4 = monosaccharideLookup.getNew("Bac");

        List<Monosaccharide> parents = Arrays.asList(node2, node1, node3, node4);

        Monosaccharide child = monosaccharideLookup.getNew("Pen");

        GlycosidicLinkage linkage = new GlycosidicLinkage(Anomericity.alpha, 3, 4, Composition.parseComposition("(OH)-1"), Composition.parseComposition("H-1"));

        SEdge edge= new SEdge<Monosaccharide, GlycosidicLinkage>(parents, child, linkage);
        edge.getParents().clear();
    }

    @Test
    public void testDefensiveCopyOfParent() throws Exception {

        Monosaccharide node1 = monosaccharideLookup.getNew("DeoxyHex");
        Monosaccharide node2 = monosaccharideLookup.getNew("Man");
        Monosaccharide node3 = monosaccharideLookup.getNew("Glc");
        Monosaccharide node4 = monosaccharideLookup.getNew("Bac");

        List<Monosaccharide> parents = new ArrayList<Monosaccharide>(Arrays.asList(node2, node1, node3, node4));

        Monosaccharide child = monosaccharideLookup.getNew("Pen");

        GlycosidicLinkage linkage = new GlycosidicLinkage(Anomericity.alpha, 3, 4, Composition.parseComposition("(OH)-1"), Composition.parseComposition("H-1"));

        SEdge edge= new SEdge<Monosaccharide, GlycosidicLinkage>(parents, child, linkage);

        parents.clear();
        assertEquals(4, edge.getParents().size());
    }

    @Test
    public void testEqualsAndHashCode1() throws Exception {

        SEdge edge1 = new SEdge<Monosaccharide, GlycosidicLinkage>(Arrays.asList(newNode(0)), newNode(1), GlycosidicLinkage.UNKNOWN);
        SEdge edge2 = new SEdge<Monosaccharide, GlycosidicLinkage>(Arrays.asList(newNode(0)), newNode(1), GlycosidicLinkage.UNKNOWN);

        assertEquals(true, edge1.equals(edge2));
        assertEquals(true, edge2.equals(edge1));
        assertEquals(edge1.hashCode(), edge2.hashCode());
    }

    @Test
    public void testEqualsAndHashCode2() throws Exception {

        SEdge edge1 = new SEdge<Monosaccharide, GlycosidicLinkage>(Arrays.asList(newNode(0), newNode(1), newNode(2)), newNode(3), GlycosidicLinkage.UNKNOWN);
        SEdge edge2 = new SEdge<Monosaccharide, GlycosidicLinkage>(Arrays.asList(newNode(0), newNode(1), newNode(2)), newNode(3), GlycosidicLinkage.UNKNOWN);

        assertEquals(true, edge1.equals(edge2));
        assertEquals(true, edge2.equals(edge1));
        assertEquals(edge1.hashCode(), edge2.hashCode());
    }

    @Test
    public void testEqualsAndHashCode3() throws Exception {

        SEdge edge1 = new SEdge<Monosaccharide, GlycosidicLinkage>(Arrays.asList(newNode(1)), newNode(2), GlycosidicLinkage.UNKNOWN);
        SEdge edge2 = new SEdge<Monosaccharide, GlycosidicLinkage>(Arrays.asList(newNode(0)), newNode(1), GlycosidicLinkage.UNKNOWN);

        assertEquals(false, edge1.equals(edge2));
        assertEquals(false, edge2.equals(edge1));
        assertNotEquals(edge1.hashCode(), edge2.hashCode());
    }

    @Test
    public void testEqualsAndHashCode4() throws Exception {

        SEdge edge1 = new SEdge<Monosaccharide, GlycosidicLinkage>(Arrays.asList(newNode(1)), newNode(2), GlycosidicLinkage.UNKNOWN);
        SEdge edge2 = new SEdge<Monosaccharide, GlycosidicLinkage>(Arrays.asList(newNode(0), newNode(1)), newNode(2), GlycosidicLinkage.UNKNOWN);

        assertEquals(false, edge1.equals(edge2));
        assertEquals(false, edge2.equals(edge1));
        assertNotEquals(edge1.hashCode(), edge2.hashCode());
    }

    @Test
    public void testToString() throws Exception {

        SEdge edge = new SEdge<Substituent, SubstituentLinkage>(
                Arrays.asList(monosaccharideLookup.getNew("Pen"), monosaccharideLookup.getNew("Hex")),
                substituentLookup.getNew("Glycolyl"),
                SubstituentLinkage.UNKNOWN);

        assertEquals("SEdge{parents=[Monosaccharide{name='Pen', composition=C5H10O5, monosaccharideSuperclass=Pentose, monosaccharideClass=Pen, ring=pyranose, firstRingCarbon=1, lastRingCarbon=5}, Monosaccharide{name='Hex', composition=C6H12O6, monosaccharideSuperclass=Hexose, monosaccharideClass=Hex, ring=pyranose, firstRingCarbon=1, lastRingCarbon=5}], child=Substituent{name='Glycolyl', composition=COCH2OH}, link=SubstituentLinkage{linkedCarbon=-1,linkedComposition=unknown}}", edge.toString());
    }

    private Monosaccharide newNode(int index) {

        List<Monosaccharide> monosaccharides = monosaccharideLookup.values();
        return monosaccharides.get(index);

    }
}
